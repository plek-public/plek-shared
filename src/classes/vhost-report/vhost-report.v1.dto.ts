export class VhostReportV1Dto {
	/**
	 * Hostname of the Vhost
	 */
	vhost: string;
	/**
	 * Month and year of the report formatted
	 * as MM-YYYY
	 *
	 * @example
	 * "01-2023"
	 */
	monthYear: string;

	members: number;
	externals: number;
	admins: number;

	activatedMembers: number;
	activatedExternals: number;
	activeMembers: number;
	activeExternals: number;
	/**
	 * @deprecated
	 */
	activeLastMonthMembers: number;
	/**
	 * @deprecated
	 */
	activeLastMonthExternals: number;

	appUsers: number;
	activeGroups: number;
	/**
	 * Number of daily visitors in the last month
	 *
	 * Daily is defined as more than 14 visits in a
	 * given month
	 */
	dailyVisitors: number;
	/**
	 * Number of weekly visitors in the last month
	 *
	 * Weekly is defined as more than 7 visits in a
	 * given month
	 */
	weeklyVisitors: number;
	/**
	 * Number of monthly visitors in the last month
	 *
	 * Monthly is defined as more than 1 visit in a
	 * given month
	 */
	monthlyVisitors: number;

	uniqueViews: number;
	uniquePostViews: number;
	uniquePageViews: number;
	uniqueDocumentViews: number;
	uniqueReads: number;

	'read%': number;
	'visit%': number;
	'visitFrequency%': number;
	'reach%': number;
	'contribution%': number;
	'interaction%': number;

	'readNorm%': number;
	'visitNorm%': number;
	'reachNorm%': number;
	'contributionNorm%': number;
	'interactionNorm%': number;

	readQuartileRank: number;
	reachQuartileRank: number;
	visitQuartileRank: number;
	contributionQuartileRank: number;
	interactionQuartileRank: number;

	clientScore: number;
	maxScore: number;
	relativeScore: number;
	healthScore: number;

	channels: number;
	// Comma separated list
	channelReadsPerChannel: string | string[] | any;
	channelReads: number;
	groups: number;
	programs: number;
	pages: number;
	documents: number;

	postsLastMonth: number;
	commentsLastMonth: number;
	chatsLastMonth: number;
	activeChatRoomsLastMonth: number;

	// Comma separated list
	groupCategories: string | string[];

	// string acting like a bool
	chat: string | boolean;
	/**
	 * string acting like a bool
	 * @deprecated
	 */
	newChat: string | boolean;
	userFilters: number;
	// Comma separated list
	programManager: string | string[];
	// Comma separated list
	pageCategories: string | string[];
	sso: string;
	userProvisioning: string;
	createdAt: Date;
	updatedAt: Date;
	programsCompleted: number;
	activePrograms: number;
	tasksEnabled: boolean;
	academyEnabled: boolean;
	innovationEnabled: boolean;
	hasOwnAppIOS: boolean;
	hasOwnAppAndroid: boolean;
	isMother: boolean;
	motherURL: string;
	twoFactorEnabled: boolean;
	twoFactorMethod?: string;
	smartConversationsEnabled: boolean;
	smartConversationLastDate?: Date;
	smartConversationLastSubject?: string;
	subscriptionPlan: string;
	/**
	 * Client ID of the Vhost
	 * Part of Vhost Metadata
	 */
	clientId: string;

	likesLastMonth: number;
	agendaItemsCreated: number;
	contentPagesCreated: number;
	documentsCreated: number;
	averageVisitors: number;

	/// undocumented fields
	contribution: number;
	/**
	 * @deprecated
	 */
	interaction: number;
	healthScoreQuartile: {
		min: number;
		max: number;
		q1: number;
		q2: number;
		q3: number;
		upper: number;
		lower: number;
	};
	healthScoreQuartileRank: number;
	id: string;
}
