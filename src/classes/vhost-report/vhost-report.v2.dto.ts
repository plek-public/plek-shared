import { Quartile, QuartileRank } from '../../types/quartile';
import { VhostReportV1Dto } from './vhost-report.v1.dto';

export class VhostReportV2Dto extends VhostReportV1Dto {
	public channelReadsPerChannel: Record<string, number>;
	public groupCategories: string[];
	public chat: boolean;
	/**
	 * @deprecated
	 */
	public newChat: boolean;
	public pageCategories: string[];
	public programManager: string[];

	public healthScoreQuartile: Quartile;
	public healthScoreQuartileRank: QuartileRank;
	public readQuartileRank: QuartileRank;
	public reachQuartileRank: QuartileRank;
	public visitQuartileRank: QuartileRank;
	public contributionQuartileRank: QuartileRank;
	public interactionQuartileRank: QuartileRank;
}
