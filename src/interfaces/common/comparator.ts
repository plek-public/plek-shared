/**
 * Defines the relationship between 2 values.
 *
 * Any new comparators that may be needed should be added to this union type.
 * It's encouraged to use TS's Extract utility type in order to protect your code from such changes.
 * @example type NonEqualComparator = Extract<Comparator, 'eq' | 'lt'>
 */
export type Comparator = 'eq' | 'gt' | 'lt';
