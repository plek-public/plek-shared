import { IPulse } from './IPulse';

/**
 * @deprecated
 */
export interface IInitiative {
	id: number;
	title: string;
	pulses: IPulse[];
}
