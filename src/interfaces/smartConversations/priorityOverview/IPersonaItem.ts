/**
 * @deprecated
 */
export interface IPersonaItem {
	id: number;
	category_id: number;
	category_icon: string;
	category: string;
	categorylabel: { en?: string; nl?: string };
	name: string;
	namelabel: { en?: string; nl?: string };
	count: number;
}
