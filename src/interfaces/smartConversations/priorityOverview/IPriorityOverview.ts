import { IPriorityOverviewStakeholder } from './IPriorityOverviewStakeholder';
import { IPersonaItem } from './IPersonaItem';

/**
 * @deprecated
 */
export interface IPriorityOverview {
	personaItems: IPersonaItem[];
	teamItems: IPersonaItem[];
	// Sentensor returns number indexed object instead of more obvious array
	stakeholders: { [index: number]: IPriorityOverviewStakeholder };
}
