import { IPersonaItem } from './IPersonaItem';

/**
 * @deprecated
 */
export interface IPriorityOverviewStakeholder {
	stakeholder_id: number;
	level: number;
	name: string;
	personaItems: Array<
		IPersonaItem & {
			distance: number;
			effort: string;
			ranking: number;
			teamscore: number;
			teamscoreSort: number;
		}
	>;
	teamItems: Array<
		IPersonaItem & {
			distance: number;
			effort: string;
			teamscore: number;
		}
	>;
}
