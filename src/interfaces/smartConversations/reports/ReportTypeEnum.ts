export enum ReportTypeEnum {
	PERSONAS = 'personas',
	PRIORITY = 'priority',
	COMMENTS = 'comments',
	HEATMAPS = 'persona-heatmaps',
	DKAR = 'dkar',
}
