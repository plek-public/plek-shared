import { IPriority } from './IPriority';

export interface IPriorities {
	personaItems: IPriority[];
	teamItems: IPriority[];
}
