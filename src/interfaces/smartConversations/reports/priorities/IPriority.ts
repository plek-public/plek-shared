import { EffortEnum } from './EffortEnum';
import { ITranslation } from '../../ITranslation';

export interface IPriority {
	id: number;
	effort: EffortEnum | string;
	distance: number;
	category: string;
	category_icon: string;
	category_id: number;
	categorylabel: ITranslation;
	name: string;
	namelabel: ITranslation;
	teamscore: number;
	teamscoreSort?: number;
	ranking?: number;
}
