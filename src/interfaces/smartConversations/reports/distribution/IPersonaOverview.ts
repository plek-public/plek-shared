export interface IPersonaOverview {
	name: string;
	fullname: string;
	gap: number;
	changerealisation: number;
	effectiveness: number;
	organisatiion_percentage: number;
	distribution: number[];
	color: string;
}
