import { IPersonaOverview } from './IPersonaOverview';

export interface IDistribution {
	stakeholders: string[];
	persona_overview: IPersonaOverview[];
}
