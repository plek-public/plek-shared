import { IResponse } from './IResponse';
import { ITranslation } from '../../ITranslation';

export interface IQuestion {
	flowelement_id: number;
	responses: IResponse[];
	text: ITranslation;
}
