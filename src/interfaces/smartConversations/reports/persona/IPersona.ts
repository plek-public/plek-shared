import { Item } from './Item';

export interface IPersona {
	id: number;
	color: string;
	name: string;
	avatar: string;
	percentage: number;
	stakeholderPercentage: number;
	descriptionTitle: string;
	description: string;
	itemsTitle: string;
	items: Item[];
	footer: string;
}
