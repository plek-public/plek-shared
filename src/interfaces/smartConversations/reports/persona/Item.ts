export interface Item {
	id: number;
	rating: number;
	organizationItem: boolean;
	name: string;
}
