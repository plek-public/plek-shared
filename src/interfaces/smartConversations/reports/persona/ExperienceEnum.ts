export enum ExperienceEnum {
	UNDECIDED = 'undecided',
	POSITIVE = 'positive',
	VERY_POSITIVE = 'very positive',
	NEGATIVE = 'negative',
	VERY_NEGATIVE = 'very negative',
}
