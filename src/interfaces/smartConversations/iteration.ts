export type Iteration = {
	id: string;
	index: number;
	createdAt: string;
	updatedAt?: string;
	formId?: string;
};
