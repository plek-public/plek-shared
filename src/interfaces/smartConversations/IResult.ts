import { ITranslation } from './ITranslation';

/**
 * @deprecated
 */
export interface IResult {
	id: number;
	name: string;
	namelabel: ITranslation;
	score: number;
	category: {
		id: number;
		name: string;
		namelabel: ITranslation;
	};
}
