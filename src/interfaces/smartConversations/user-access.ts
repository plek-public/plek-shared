import { Iteration } from './iteration';
import { SmartConversation } from './smart-conversation';
import { Team } from './team';

export const ReportType = {
	/**
	 * Team-specific success factor information.
	 */
	SUCCESS_FACTORS: 'SUCCESS_FACTORS',
	/**
	 * Everything there is to know specifically about personas with no users or teams. This permission is not tied to any team, so if a user has it they in any share they will have it everywhere.
	 */
	PERSONA_INFO: 'PERSONA_INFO',
	/**
	 * The distribution of personas within the designated teams.
	 */
	PERSONA_DISTRIBUTION: 'PERSONA_DISTRIBUTION',
	/**
	 * Responses to the regular questions within the designated iterations and teams
	 */
	RESPONSES: 'RESPONSES',
} as const;

export type ReportType = (typeof ReportType)[keyof typeof ReportType];

export type UserAccessTeam = Team & {
	/**
	 * List of report types that the user has been given permission to access for the given team.
	 */
	reportTypes?: ReportType[];
	/**
	 * Like `reportTypes`, but filtered to not include report types that must be hidden for privacy reasons.
	 */
	privacyFilteredReportTypes?: ReportType[];
};
export type UserAccessIteration = Pick<Iteration, 'id' | 'index'> & { teams: UserAccessTeam[] };
export type UserAccessSmartConversation = Pick<SmartConversation, 'id' | 'name'> & {
	iterations: UserAccessIteration[];
};
