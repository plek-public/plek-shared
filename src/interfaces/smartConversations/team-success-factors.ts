import { Team } from './team';

export type SuccessFactorRating = {
	successFactorId: string;
	/**
	 * Normalized to 0-1
	 */
	averageRating: number;
};

export type TeamSuccessFactorRatings = {
	personaLowRated: SuccessFactorRating[];
	teamLowRated: SuccessFactorRating[];
	teamHighRated: SuccessFactorRating[];
	/*
	 * Record<successFactorId, focus percentage 0-1>
	 */
	focus?: Record<string, number>;
};

export type TeamSuccessFactorsResponse = {
	successFactorCategories: Record<
		string,
		{
			name: string;
		}
	>;
	successFactors: Record<
		string,
		{
			categoryId: string;
			name: string;
		}
	>;
	teams: (Team & TeamSuccessFactorRatings)[];
};
