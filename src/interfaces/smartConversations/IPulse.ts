import { ICycle } from './ICycle';

/**
 * @deprecated
 */
export interface IPulse {
	id: number;
	status: string;
	ordering: number;
	cycles: ICycle[];
}
