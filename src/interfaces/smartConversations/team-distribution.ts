import { Persona } from './persona';
import { Team } from './team';

export type TeamDistribution = {
	distribution: number[];
} & Team;

export type TeamDistributionResponse = {
	personas: Pick<Persona, 'avatar' | 'id' | 'name'>[];
	teams?: TeamDistribution[];
};
