import { ReportType } from './user-access';
import { IUserMicro } from '../user/IUserMicro';

export enum SharedTo {
	EVERYONE = 'EVERYONE',
	USER_IDS = 'USER_IDS',
}

type BaseShare = {
	// Metadata
	id: string;
	vhost: string;

	/**
	 * Type of user filtering to use. If set to `EVERYONE`, the `userIds` and `roles` fields are ignored and the share will apply to all users on the vhost
	 */
	sharedTo: SharedTo;

	/**
	 * User provided to make it easier for the user to find it later. Not to be used for anything else
	 */
	name?: string;

	/**
	 * The iteration that this share applies to
	 */
	upToIterationId: string;

	/**
	 * Which types of reports does this share target
	 */
	reportTypes: ReportType[];

	/**
	 * Same as `ownTeamIfSubteamOf`, but also includes all the subteams of the team the user is in. For example, a share that applies to all managers will use this so that they can see all the teams under them. A manager of a product team will be able to see results of backend, frontend, designers and so on.
	 */
	ownTeamWithSubteamsIfSubteamOf: string[];

	/**
	 * Should have access to these teams and all their subteams
	 */
	teamsWithSubteams: string[];
};

export type ShareWithUsers = {
	sharedTo: SharedTo.USER_IDS;
	users: IUserMicro[];
};

type SharedToEveryone = {
	sharedTo: SharedTo.EVERYONE;
};

export type SharedToUserIds = {
	sharedTo: SharedTo.USER_IDS;
	userIds: string[];
};

/**
 * Gives access to resources
 */
export type Share = BaseShare & (ShareWithUsers | SharedToEveryone);

export type ShareToUsers = BaseShare & (SharedToUserIds | SharedToEveryone);
