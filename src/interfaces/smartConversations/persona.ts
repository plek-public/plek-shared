export type PersonaSuccessFactor = {
	/**
	 * What is the average rating this persona gives to this success factor. Normalized to 0-1
	 */
	averageRating: number;

	/**
	 * The rank (arranged by importance) that this success factor would have if the current persona was a real person. Lower is more important
	 */
	selectionRank: number;

	/**
	 * Opaque value coming from Sentensor. Used in calculations.
	 */
	topChoice: boolean;

	/**
	 * Opaque value coming from Sentensor. Used in calculations.
	 */
	strongProperty: boolean;
};

export type Persona = {
	/**
	 * Stringified representation of persona id as used by Sentensor.
	 */
	id: string;

	/**
	 * The greek names. Deterministic based on persona id.
	 */
	name: string;

	/**
	 * Path to image
	 */
	avatar: string;

	/**
	 * User ids that are part of this persona
	 */
	userIds: string[];

	/**
	 * Success factor data for this persona. Uses `id` from {@link SuccessFactor} as the key
	 */
	successFactors: Record<string, PersonaSuccessFactor>;
};
