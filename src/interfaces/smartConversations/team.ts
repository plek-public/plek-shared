export type Team = TaxonomyTermTeam | TaxonomyTeam;

export type TeamTypes = Team['type'];

type TeamBase = {
	id: string;
	name: string;
	type: string;
};

export type TaxonomyTermTeam = {
	type: 'TAXONOMY_TERM';
	taxonomyId: string;
	/**
	 * The parent taxonomy term if it exists. undefined if the term is directly under the taxonomy.
	 **/
	parentId?: string;
} & TeamBase;

export type TaxonomyTeam = {
	type: 'TAXONOMY';
} & TeamBase;
