/**
 * @deprecated
 */
export interface ISmartConversation {
	dialogId: number;
	cycleId: number;
	initiativeId: number;
	programId: string;
	stepId: string;
	language: string;
}
