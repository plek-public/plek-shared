export type SmartConversation = {
	id: string;
	name: string;
	vhost: string;
	taxonomyIds: string[];
	/**
	 * @deprecated Needed to identify Sentensor initiative for SC
	 */
	initiative?: {
		customerId: number;
		environment: 'staging' | 'production';
		initiativeId: number;
		/**
		 * @deprecated Needed to load a combination of old and new SC result screens
		 */
		iterationCycleIdMapping: Record<string, number>;
		taxonomyStakeholderTypeMapping: Record<string, number>;
		termStakeholderMapping: Record<string, number>;
	};
};
