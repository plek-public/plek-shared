/**
 * @deprecated
 */
export interface ITranslation {
	nl?: string;
	en?: string;
}
