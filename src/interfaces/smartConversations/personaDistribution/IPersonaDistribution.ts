/**
 * @deprecated
 */
export interface IPersonaDistribution {
	data: Array<number[]>;
	stakeholders: Array<{
		name: string;
		level: number;
	}>;
}
