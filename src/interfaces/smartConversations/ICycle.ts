/**
 * @deprecated
 */
export interface ICycle {
	id: number;
	dialog_design_id: number;
	dialog_design: string;
	startdate: string;
	enddate: string;
	phase: string;
}
