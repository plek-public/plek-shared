/**
 * @deprecated
 */
export interface IStakeholder {
	id: number;
	name: string;
	children: IStakeholder[];
}
