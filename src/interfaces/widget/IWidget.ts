import { WidgetTypesEnum } from './WidgetTypes';

export interface IWidget {
	id: string;
	status: boolean;
	type: WidgetTypesEnum;
	name: string;
	isWidgetNameHidden?: boolean;
	widgetHeight?: number;
	options?: { requiredTaxonomy: boolean; startCountFrom?: number; taxonomyId: string };
	dataMappingId?: string;
	isFeatured?: boolean;
	defaultName?: string;
	widgetTwitterMode?: 'profile' | 'list';
	twitterAccount?: string;
	twitterAccountSlug?: string;
	embedCode?: string;
	image?: string;
	form?: {
		id: string;
		title: string;
	};
	link?: string;
	linkOpenNewWindow?: boolean;
}
