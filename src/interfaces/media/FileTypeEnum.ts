export enum FileTypeEnum {
	file = 'FILE',
	image = 'IMAGE',
	video = 'VIDEO',
	audio = 'AUDIO',
	html = 'HTML',
}
