import { FileTypeEnum } from './FileTypeEnum';
import { IMediaGeneric } from './IMediaGeneric';

export interface IMediaFile extends IMediaGeneric {
	fileType: FileTypeEnum.file;
}

export interface IMediaImageMeta {
	width: number;
	height: number;
	orientation: 'landscape' | 'portrait';
}

export interface IMediaImage extends IMediaGeneric {
	fileType: FileTypeEnum.image;
	imagemeta?: IMediaImageMeta;
}

export interface IMediaVideo extends IMediaGeneric {
	fileType: FileTypeEnum.video;
}

export interface IMediaAudio extends IMediaGeneric {
	fileType: FileTypeEnum.audio;
}

export interface IMediaHtml extends IMediaGeneric {
	fileType: FileTypeEnum.html;
}

export type IMedia = IMediaFile | IMediaImage | IMediaVideo | IMediaAudio | IMediaHtml;
