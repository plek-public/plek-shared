import { FileTypeEnum } from './FileTypeEnum';

export interface IMediaGeneric {
	fileType: FileTypeEnum;
	filelink: string;
	filename: string;
	id: string;
}
