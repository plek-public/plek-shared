/**
 * For getting the list of organisations.
 */
export interface IOrganisationList {
	id: string;
	name: string;
}
