/**
 * Role of the user in the organisation.
 */
export enum OrganisationUserType {
	Admin = 'Admin',
	Normal = 'Normal',
}
