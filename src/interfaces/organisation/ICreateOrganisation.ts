/**
 * Organisation creation interface.
 */
export interface ICreateOrganisation {
	name: string;
	admins: string[];
	users: string[];
}
