import { OrganisationUserType } from './OrganisationUserType';
/**
 * This data is stored on UserObj.
 * Send it with the user object as organisation.
 */
export interface IUserOrganisaion {
	/**
	 * ID of the organisation.
	 */
	id: string;
	/**
	 * Name of the organisation.
	 */
	name: string;
	/**
	 * Role of the user in the organisation.
	 */
	role: OrganisationUserType;
}
