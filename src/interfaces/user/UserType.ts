export enum UserType {
	NORMAL = 'normal',
	SAML = 'saml',
	OAUTH = 'oauth',
	LDAP = 'ldap',
	DATAMAPPING = 'datamapping',
	RECOVERED = 'recovered',
	BOT = 'bot',
}
