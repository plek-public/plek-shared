export enum PlekUserRoles {
	DEMO = 'demo',
	USER_LIGHT = 'user-light',
	USER_PREBOARDING = 'user-preboarding',
	USER = 'user',
	CONTENT_MANAGER = 'content-manager',
	ADMIN = 'admin',
	SUPER_ADMIN = 'super-admin',
	PLATFORM_EXTERNAL_ACCESS = 'external-access', // Access to the own groups on platfrom
	PLATFORM_EXTERNAL_ACCESS_LIGHT = 'external-access-light', // Same as platform-external-access without chat
	GROUP_EXTERNAL_ACCESS = 'group-external-access', // Same as platform-external-access but visible only to `members of own group
	MEMBER = 'member',
	USER_PLUS = 'user-plus', // Excludes preboarding user
	USER_PREBOARDING_PLUS = 'user-preboarding-plus', // Includes preboarding user
	SENTENSOR_ADMIN = 'sentensor-admin',
	// MEMBER = 'member',
}
