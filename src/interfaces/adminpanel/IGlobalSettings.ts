import 'reflect-metadata';

import { IAppMenu } from './globalSettings/AppMenu';
import { ContentPages } from './globalSettings/ContentPages';
import { WopiSettings } from './globalSettings/WopiSettings';
import { MicrosoftOfficeIntegration } from './globalSettings/MicrosoftOfficeIntegration';
import { Dashboard } from './globalSettings/Dashboard';
import { NativeApp } from './globalSettings/NativeApp';
import { UserImport } from './globalSettings/UserImport';
import { GroupSettings } from './globalSettings/GroupSettings';
import { DocumentSettings } from './globalSettings/DocumentSettings';
import { PlekUserRoles } from './PlekUserRoles';
import { MessagesSettings } from './globalSettings/MessageSettings';
import { AdminpanelSettings } from './globalSettings/AdminpanelSettings';
import { SearchSettings } from './globalSettings/SearchSettings';
import { UsersSettings } from './globalSettings/UsersSettings';
import { AgendaSettings } from './globalSettings/AgendaSettings';
import { Type } from '@nestjs/class-transformer';
import { IsArray, IsBoolean, IsEnum, IsOptional, ValidateNested } from '@nestjs/class-validator';
import { NotificationSettings } from './globalSettings/NotificationSettings';
import { GoogleDriveIntegrationSettings } from './globalSettings/GoogleDriveIntegrationSettings';
import { UserExportSetting } from './globalSettings/UserExportSetting';
import { SignupSettings } from './globalSettings/SignupSettings';
import { TimelineSettings } from './globalSettings/TimelineSettings';
import { DailyNewsSettings } from './globalSettings/DailyNewsSettings';
import { FaqSettings } from './globalSettings/FaqSettings';
import { IframeDashboardWidgetSettings } from './globalSettings/IframeDashboardWidgetSettings';
import { PermissionsSettings } from './globalSettings/PermissionsSettings';
import { LocalizationSettings } from './globalSettings/LocalizationSettings';
import { LogoutSettings } from './globalSettings/LogoutSettings';
import { LandingpageSettings } from './globalSettings/LandingpageSettings';
import { PlatformHelperSettings } from './globalSettings/PlatformHelperSettings';
import { HiddenGroupsSettings } from './globalSettings/HiddenGroupsSettings';
import { StatisticsSettings } from './globalSettings/StatisticsSettings';
import { TranslationSettings } from './globalSettings/TranslationSettings';
import { TranslationOverridesSettings } from './globalSettings/TranslationOverridesSettings';
import { MediaSettings } from './globalSettings/MediaSettings';
import { MultiPlekSidebarSettings } from './globalSettings/MultiPlekSidebarSettings';
import { ThirdPartySettings } from './globalSettings/ThirdPartySettings';
import { SubgroupManagerSettings } from './globalSettings/SubgroupManagerSettings';
import { ViewSettings } from './globalSettings/ViewSettings';
import { ReleaseNotesSettings } from './globalSettings/ReleaseNotesSettings';
import { MenuItemSetting } from './globalSettings/MenuItemSetting';
import { EmployeeEngagementSettings } from './globalSettings/EmployeeEngagementSettings';
import { Switch } from './annotations/Switch';
import { DigestSettings } from './globalSettings/DigestSettings';
import { ProgramManagerSettings } from './globalSettings/ProgramManagerSettings';
import { FooterInfoSettings } from './globalSettings/FooterInfoSettings';
import { RegisterFormSettings } from './globalSettings/RegisterFormSettings';
import { CustomEmailTypeTemplateSettings } from './globalSettings/CustomEmailTypeTemplateSettings';
import { UpsellSettings } from './globalSettings/UpsellSettings';

const Title =
	(value: string): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('title', value, target, propertyKey);
	};
const MenuItemDescription =
	(value: string): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('description', value, target, propertyKey);
	};

export class IGlobalSettings {
	@Title('Important news')
	@MenuItemDescription('Customize important news on the startpage')
	@Type(() => Dashboard)
	@IsOptional()
	@ValidateNested()
	dashboard?: Dashboard;
	/* ******************************************************************************** */
	@Title('Custom App')
	@MenuItemDescription('Config custom app')
	@Type(() => NativeApp)
	@IsOptional()
	@ValidateNested()
	nativeApp?: NativeApp;
	/* ******************************************************************************** */
	@Title('Edit documents on Microsoft Office Online (WOPI)')
	@MenuItemDescription('Status, User validation, WOPI url')
	@Type(() => WopiSettings)
	@IsOptional()
	@ValidateNested()
	wopi?: WopiSettings;
	/* ******************************************************************************** */
	@Title('Digest')
	@MenuItemDescription('Set digest, Featured on top, Opt in/out')
	@Type(() => DigestSettings)
	@IsOptional()
	@ValidateNested()
	digest?: DigestSettings;
	/* ******************************************************************************** */
	@Title('M365 Integration')
	@MenuItemDescription('Search M365 sharepoint document, link Plek groups to Sharepoint sites, enable teams video call ')
	@Type(() => MicrosoftOfficeIntegration)
	@IsOptional()
	@ValidateNested()
	microsoftOfficeIntegration?: MicrosoftOfficeIntegration;
	/* ******************************************************************************** */
	@Title('User import')
	@MenuItemDescription('Set user import permission')
	@Type(() => UserImport)
	@IsOptional()
	@ValidateNested()
	userImport?: UserImport;
	/* ******************************************************************************** */
	@Title('Content Pages')
	@MenuItemDescription('Customize content page categories')
	@Type(() => ContentPages)
	@IsOptional()
	@ValidateNested()
	contentpages?: ContentPages;
	/* ******************************************************************************** */
	@Title('Public access (DEPRECATED)')
	@MenuItemDescription('Make Plek accessible for public (Without login)')
	@IsBoolean()
	@IsOptional()
	@Switch({ label: 'Enable public access' })
	/**
	 * @deprecated was mainly for IWA, but used on other vhosts as well. Don't build new features with this setting
	 */
	publicAccess?: boolean;
	/* ******************************************************************************** */
	@Title('Footer')
	@MenuItemDescription('Footer configuration for html layout (ejs template files)')
	@Type(() => FooterInfoSettings)
	@IsOptional()
	@ValidateNested()
	footerInfo?: FooterInfoSettings;
	/* ******************************************************************************** */
	@Title('Register form')
	@MenuItemDescription('Footer configuration for html layout (ejs template files)')
	@Type(() => RegisterFormSettings)
	@IsOptional()
	@ValidateNested()
	registerForm?: RegisterFormSettings;
	/* ******************************************************************************** */
	@Title('React Only Plek')
	@MenuItemDescription('Disables old AngularJs code')
	@IsBoolean()
	@IsOptional()
	@Switch({ label: 'Toggle reactPlek' })
	reactPlek?: boolean;
	/* ******************************************************************************** */
	@Title('Groups')
	@MenuItemDescription('Configure pages, chat and categories in groups')
	@Type(() => GroupSettings)
	@ValidateNested()
	groups: GroupSettings;
	/* ******************************************************************************** */
	@Title('Documents')
	@MenuItemDescription('Configure document settings')
	@Type(() => DocumentSettings)
	@ValidateNested()
	documents: DocumentSettings;
	/* ******************************************************************************** */
	@Title('Message Digest')
	@MenuItemDescription('Configure message digest')
	@Type(() => MessagesSettings)
	@IsOptional()
	@ValidateNested()
	messages?: MessagesSettings;
	/* ******************************************************************************** */
	@Title('Agenda')
	@MenuItemDescription('Configure agenda settings')
	@Type(() => AgendaSettings)
	@IsOptional()
	@ValidateNested()
	agendas?: AgendaSettings;
	/* ******************************************************************************** */
	@Title('Adminpanel')
	@MenuItemDescription('Enable and disable modules in the admin panel')
	@Type(() => AdminpanelSettings)
	@IsOptional()
	@ValidateNested()
	adminpanel: AdminpanelSettings;
	/* ******************************************************************************** */
	@Title('Users')
	@MenuItemDescription('User Roles, Activated users')
	@Type(() => UsersSettings)
	@IsOptional()
	@ValidateNested()
	users?: UsersSettings;
	/* ******************************************************************************** */
	@Title('Search')
	@MenuItemDescription('Set aggregation settings')
	@Type(() => SearchSettings)
	@IsOptional()
	@ValidateNested()
	search?: SearchSettings;
	/* ******************************************************************************** */
	@Title('Notification')
	@MenuItemDescription('Set notification settings')
	@Type(() => NotificationSettings)
	@IsOptional()
	@ValidateNested()
	notification?: NotificationSettings;
	/* ******************************************************************************** */
	@Title('Google Drive')
	@MenuItemDescription('Set Google Drive configuration settings')
	@Type(() => GoogleDriveIntegrationSettings)
	@IsOptional()
	@ValidateNested()
	googleDriveIntegration?: GoogleDriveIntegrationSettings;
	/* ******************************************************************************** */
	@Title('User export')
	@MenuItemDescription('Configure user export')
	@Type(() => UserExportSetting)
	@IsOptional()
	@ValidateNested()
	userExport?: UserExportSetting;
	/* ******************************************************************************** */
	@Title('User signup')
	@MenuItemDescription('Set User signup settings')
	@Type(() => SignupSettings)
	@IsOptional()
	@ValidateNested()
	signup?: SignupSettings;
	/* ******************************************************************************** */
	@Title('Timeline')
	@MenuItemDescription('Comments, Push to top')
	@Type(() => TimelineSettings)
	@IsOptional()
	@ValidateNested()
	timeline?: TimelineSettings;
	/* ******************************************************************************** */
	@Title('Daily news')
	@MenuItemDescription('Status, ')
	@Type(() => DailyNewsSettings)
	@IsOptional()
	@ValidateNested()
	dailyNews?: DailyNewsSettings;
	/* ******************************************************************************** */
	@Title('Frequently asked questions')
	@MenuItemDescription('Status, URL')
	@Type(() => FaqSettings)
	@IsOptional()
	@ValidateNested()
	faq?: FaqSettings;
	/* ******************************************************************************** */
	@Title('Dashboard configuration for third party usage via an iframe widget')
	@MenuItemDescription('Channel selection, Editorial selection, Whitelisting')
	@Type(() => IframeDashboardWidgetSettings)
	@IsOptional()
	@ValidateNested()
	iframeDashboardWidget?: IframeDashboardWidgetSettings;
	/* ******************************************************************************** */
	@Title('M365 Teams')
	@MenuItemDescription('Allow vhost to link with Microsoft Teams')
	@Switch({ label: 'Link to teams ' })
	@IsOptional()
	@IsBoolean()
	linkToTeams?: boolean;
	/* ******************************************************************************** */
	@Title('App menu')
	@MenuItemDescription('Menu')
	@Type(() => IAppMenu)
	@IsOptional()
	@IsArray()
	@ValidateNested({ each: true })
	appMenu?: IAppMenu[];
	/* ******************************************************************************** */
	@Title('Permissions')
	@MenuItemDescription('Groups, Role specific, Whitelist email extensions')
	@Type(() => PermissionsSettings)
	@IsOptional()
	@ValidateNested()
	permissions?: PermissionsSettings;
	/* ******************************************************************************** */
	@Title('Custom e-mail template type')
	@MenuItemDescription('Status, Custom type')
	@Type(() => CustomEmailTypeTemplateSettings)
	@IsOptional()
	@ValidateNested()
	emailTemplate?: CustomEmailTypeTemplateSettings;
	/* ******************************************************************************** */
	@Title('Localization')
	@MenuItemDescription('Default timezone, Timezone permissions')
	@Type(() => LocalizationSettings)
	@ValidateNested()
	localization: LocalizationSettings;
	/* ******************************************************************************** */
	@Title('Logout')
	@MenuItemDescription('Disallow logout for selected roles')
	@Type(() => LogoutSettings)
	@IsOptional()
	@ValidateNested()
	disableLogout?: LogoutSettings;
	/* ******************************************************************************** */
	@Title('Main menu')
	@MenuItemDescription('Path, Description, Naming')
	menu: MenuItemSetting;
	/* ******************************************************************************** */
	@Title('Landing Page')
	@MenuItemDescription('Custom landing page')
	@Type(() => LandingpageSettings)
	@IsOptional()
	@ValidateNested()
	customLandingpage?: LandingpageSettings;
	/* ******************************************************************************** */
	@Title('Platform tutorial')
	@MenuItemDescription('Enable tutorial functionalities')
	@Type(() => PlatformHelperSettings)
	@IsOptional()
	@ValidateNested()
	helpLayer?: PlatformHelperSettings;
	/* ******************************************************************************** */
	@Title('Push notification permission')
	@MenuItemDescription('Select which roles are allowed to send message push notifications')
	@IsOptional()
	@IsArray()
	@IsEnum(PlekUserRoles, { each: true })
	canSendMessagePushNotifications?: PlekUserRoles[];
	/* ******************************************************************************** */
	@Title('Media permissions')
	@MenuItemDescription('Media permissions')
	@Type(() => MediaSettings)
	@IsOptional()
	@ValidateNested()
	/*
	 * @deprecated Only used in angular controller + temporary feature flag
	 * */
	media?: MediaSettings;
	/* ******************************************************************************** */
	@Title('Statistics')
	@MenuItemDescription('Export settings')
	@Type(() => StatisticsSettings)
	@IsOptional()
	@ValidateNested()
	statistics?: StatisticsSettings;
	/* ******************************************************************************** */
	@Title('Hidden groups')
	@MenuItemDescription('Group permissions')
	@Type(() => HiddenGroupsSettings)
	@IsOptional()
	@ValidateNested()
	hiddenGroups?: HiddenGroupsSettings;
	/* ******************************************************************************** */
	@Title('Multi language')
	@MenuItemDescription('Pages, Messages')
	@Type(() => TranslationSettings)
	@IsOptional()
	@ValidateNested()
	translations?: TranslationSettings;
	/* ******************************************************************************** */
	@Title('Translation overrides')
	@MenuItemDescription('Pages, Messages')
	@Type(() => TranslationOverridesSettings)
	@IsOptional()
	@ValidateNested()
	translationOverrides?: TranslationOverridesSettings;
	/* ******************************************************************************** */
	@Title('Multiplek sidebar')
	@MenuItemDescription('Plek team url')
	@Type(() => MultiPlekSidebarSettings)
	@IsOptional()
	@ValidateNested()
	multiplekSidebar: MultiPlekSidebarSettings;
	/* ******************************************************************************** */
	@Title('Third party')
	@MenuItemDescription('User profile, User search')
	@Type(() => ThirdPartySettings)
	@IsOptional()
	@ValidateNested()
	thirdParty?: ThirdPartySettings;
	/* ******************************************************************************** */
	@Title('Activation manager')
	@MenuItemDescription('Realtime programs')
	@Type(() => ProgramManagerSettings)
	@IsOptional()
	@ValidateNested()
	programManager?: ProgramManagerSettings;
	/* ******************************************************************************** */
	@Title('Subgroup manager')
	@MenuItemDescription('Enable group manager feature within a group')
	@Type(() => SubgroupManagerSettings)
	@IsOptional()
	@IsArray()
	@ValidateNested({ each: true })
	// TODO: Rename module to 'subgroupManager', or move it under 'Groups' setting
	isSubgroupManagerEnabled?: SubgroupManagerSettings;
	/* ******************************************************************************** */
	@Title('User recovery')
	@MenuItemDescription('User recovery')
	@Type(() => ViewSettings)
	@IsOptional()
	@ValidateNested()
	view?: ViewSettings;
	/* ******************************************************************************** */
	@Title('Release notes')
	@MenuItemDescription('Status, Release notes')
	@Type(() => ReleaseNotesSettings)
	@IsOptional()
	@ValidateNested()
	customReleaseNotes?: ReleaseNotesSettings;
	/* ******************************************************************************** */
	@Title('Employee engagement')
	@MenuItemDescription('Enable employee engagement')
	employeeEngagement?: EmployeeEngagementSettings;
	/* ******************************************************************************** */
	//DC: Need new place
	hideNavigationHeader?: boolean;
	hideNavigationSidebar?: boolean;
	hideTabsSidebar?: boolean;
	/* ******************************************************************************** */
	@Title('Mobile friendly')
	@MenuItemDescription('Disable mobile suggestions in a browser')
	mobileFriendly?: boolean;
	/* ******************************************************************************** */
	@Title('Upsell')
	@MenuItemDescription('Disable upsell features')
	@Type(() => UpsellSettings)
	@IsOptional()
	@ValidateNested()
	upsell?: UpsellSettings;
	/* ******************************************************************************** */
	@Title('Magic login')
	@MenuItemDescription('Enable/disable link login feature for all users in the vhost')
	@IsOptional()
	@ValidateNested()
	magicLinkLogin?: {
		enabled: boolean;
	};
}
