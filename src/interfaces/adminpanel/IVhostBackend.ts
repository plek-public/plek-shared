import 'reflect-metadata';

import { IGlobalSettings } from './IGlobalSettings';
import { IVhost } from './IVhost';
import { IServerSettings } from './IServerSettings';
import { SAMLConfigurations } from './serverSettings/SamlSettings';

export class IVhostBackend extends IVhost {
	globalSettings: IGlobalSettings;
	serverSettings: IServerSettings;
	saml: SAMLConfigurations;
}
