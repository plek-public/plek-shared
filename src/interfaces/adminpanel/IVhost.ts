import { IWidget } from '../widget/IWidget';
import { SchemaInterface } from './globalSettings/Schema';
import { IVhostTheme } from './globalSettings/Theme';
import { IModules, PlekLanguageEnum } from './IModules';
import { SubscriptionPlans } from './SubscriptionPlans';
import { IMetadata } from './IMetadata';

export class IVhost {
	id: string;
	hostname: string;
	sitename: string;
	shortname?: string;
	themename?: string;
	subscriptionPlan: SubscriptionPlans;
	description?: string;
	language: PlekLanguageEnum[];
	modules: IModules;
	emailSender?: string;
	emailFooter?: string;
	themecolor: string;
	themeColorDark?: string;
	background?: string;
	schema?: SchemaInterface[];
	theme?: IVhostTheme;
	widgets?: IWidget[];
	status: 'active' | 'deactivated';
	version?: string;
	computedSettings?: {
		isSuperadmin?: boolean;
		hasSaml?: boolean;
		userCreationAllowed?: boolean;
		permissionSettings?: any;
	};
	searchLabels: Array<{
		name: string;
		subSearchLabel: string;
	}>;
	port: number;
	uri: string;
	ESIndexCache: any;

	gitVersion?: string;
	plekIsUpdatingMessage?: string;
	parentMultiPlekVhosts?: string[];
	lastReleaseDate?: Date;

	normalLoginAllowed?: boolean;

	metadata?: IMetadata;
}
