import { PlekUserRoles } from '../PlekUserRoles';
import { ITranslations } from './Translations';
/*import {Input} from "../annotations/Input";
import { IsArray, IsEnum } from "@nestjs/class-validator";
import {Checkbox} from "../annotations/Checkbox";*/

export enum groupType {
	OPEN = 'open',
	PRIVATE = 'private',
	HIDDEN = 'hidden',
}
export class VhostGroupCategoryInterface {
	// Properties that were configurable by UI in angular settings
	/*@Input({ label: 'Category identifier (use lowercase characters only)' })*/
	category: string;
	/*@Input({ label: 'Category name' })*/
	name: string;
	/*@Input({ label: 'Category plural name' })*/
	namePlural?: string;
	/*@Input({ label: 'Select roles that have permission to join this group category' })
	@IsArray()
	@IsEnum(PlekUserRoles, {each: true})*/
	permission?: PlekUserRoles[];
	/*@Checkbox({ label: 'Set this category to be standalone (Can only be chosen on its own)' })*/
	standalone?: boolean;
	/*@Checkbox({ label: 'Enable Smart Conversations for this category' })*/
	isEnabledSentensor?: boolean;
	// ***********************************************************
	icon?: string;
	size?: string;
	description?: string;
	pageCategory?: boolean;
	onboardingAccess?: boolean;

	enabled?: boolean;

	defaultGroupTypeOnCreate?: groupType;

	memberWithLabelTranslate?: string;
	membersWithLabelTranslate?: string;

	groupMemberLabelTranslate?: string;
	groupAdminLabelTranslate?: string;
	groupCreatorLabelTranslate?: string;

	questionsModule?: boolean;
	answersModule?: boolean;

	copyGroupAdminsFromParent?: boolean;

	createTimelineEntryOfCreationFor?: string[];

	requiredParentCategory?: string[];
	parentAllowed?: boolean;

	hideOnDashboard?: boolean;

	types?: Array<{ type: string; name?: string; description?: string; allowed?: boolean }>;

	search?: {
		includeFeaturedGroupCategory?: string[];
	};

	canPost?: string[];
	joinPermission?: PlekUserRoles[];
	taxonomy?: {
		[key: string]: VhostGroupCategoryTaxonomy;
	};
	canBeFeatured?: boolean;
	features: { [key: string]: GroupDefaultFeatureInterface };
	menu?: { [key: string]: GroupDefaultMenuInterface };
	customButtonLabels?: { isNotMember?: string; isMember?: string; leaveGroup?: string };
	popups?: Array<{ requirements: { wasMember: boolean; isMember: boolean; isAnonymous: boolean }; text: string }>;
	customTab?: Array<{ key: string; value: string }>;

	memberTranslate?: string;
	membersTranslate?: string;
	mainButton?: {
		label: string;
		translations: any;
	};
	userLabelMappings?: { [key: string]: string };
	showBigDescription?: boolean;

	settings?: {
		[key: string]: IGroupSetting;
	};
}
export interface IGroupSetting {
	optional: boolean;
	value: string;
	type: string; // date / number
	label: string;
	error?: boolean;
}
export interface VhostGroupCategoryTaxonomyType {
	type: string;
}

export interface VhostGroupCategoryTaxonomyTypeTaxonomy extends VhostGroupCategoryTaxonomyType {
	searchFilter?: boolean;
	slug: string;
}

export interface VhostGroupCategoryTaxonomyTypeLink extends VhostGroupCategoryTaxonomyType {
	class?: string;
}

export type VhostGroupCategoryTaxonomy = VhostGroupCategoryTaxonomyTypeTaxonomy & VhostGroupCategoryTaxonomyTypeLink;

export interface GroupDefaultFeatureInterface {
	name: string;
	icon: string;
	iconApp?: string;
	description: string;
	propertyOnGroup: string;
	available?: boolean;
	defaultSelected: boolean;
	pageCategory?: boolean;
	id?: string;
	namePlural?: string;
	// allowedForUser?: (vhost: ModelVhostInterface, user: ModelUserInterface) => boolean;
}

export interface GroupDefaultMenuInterface {
	name: string;
	href: string;
	available?: boolean;
	sort?: number;
	translations?: ITranslations;
}
