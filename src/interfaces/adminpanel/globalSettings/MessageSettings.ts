import { Checkbox } from '../annotations/Checkbox';

export class MessagesSettings {
	messageDigest: IMessageDigestSettings;
}

export class IMessageDigestSettings {
	@Checkbox({ label: 'Can attach messages' })
	canAttachMessages?: boolean;
	@Checkbox({ label: 'Can attach agenda' })
	canAttachAgenda?: boolean;
}
