import { IsBoolean, IsString } from '@nestjs/class-validator';
import { Input } from '../annotations/Input';
import { Switch } from '../annotations/Switch';

export class FaqSettings {
	@Switch({ label: 'Enable frequently asked questions on vhost' })
	@IsBoolean()
	status: boolean;

	@Input({ label: 'Set url for frequently asked questions page' })
	@IsString()
	href: string;
}
