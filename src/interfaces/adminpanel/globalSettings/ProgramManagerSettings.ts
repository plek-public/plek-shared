import { IsOptional, IsBoolean } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';

export class ProgramManagerSettings {
	@Checkbox({ label: 'Enable subgroup manager within groups' })
	@IsOptional()
	@IsBoolean()
	status?: boolean;
}
