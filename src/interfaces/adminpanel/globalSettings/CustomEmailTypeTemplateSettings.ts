import { IsBoolean, IsOptional, IsString } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';
import { Input } from '../annotations/Input';

export class CustomEmailTypeTemplateSettings {
	@Switch({ label: 'Status' })
	@IsBoolean()
	status: boolean;

	@Input({ label: 'Defined custom email template type' })
	@IsString()
	customType: string;
}
