import { IsBoolean } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';
import { Switch } from '../annotations/Switch';

class ModuleStatusSetting {
	@IsBoolean()
	status?: boolean;
}

export class AdminpanelSettings {
	channels?: ModuleStatusSetting;
	statistics?: StatisticsSettings;
	taxonomies?: ModuleStatusSetting;
	bin?: ModuleStatusSetting;
	widgets?: ModuleStatusSetting;
	mailstatus?: ModuleStatusSetting;
	email?: ModuleStatusSetting;
	users?: UsersSettings;
	taxonomy?: ModuleStatusSetting;
	@Switch({ label: 'Toggle react version within adminpanel' })
	reactVersion: boolean;
}

class StatisticsSettings {
	@Checkbox({ label: 'Hide individual statistics within adminpanel' })
	@IsBoolean()
	hideIndividualStatistics?: boolean;
}

class UsersSettings {
	@Checkbox({ label: "Force the 'create new user' button to be shown even though the host is SamlOnly/OAuthOnly" })
	forceAllowCreateNewUser?: boolean;
	@Checkbox({ label: 'Hide role selection possibility when creating a new user' })
	hideUserRoles?: boolean;
	@Checkbox({ label: "Hide 'do not send notification email' functionality" })
	hideEmailNotify?: boolean;
	@Checkbox({ label: 'Allow admin to remove multiple users' })
	allowAdminToRemoveMultipleUsers?: boolean;
}
