import { IsArray, IsBoolean, IsOptional, IsEnum } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';
import { PlekUserRoles } from '../PlekUserRoles';

export class UserImport {
	@Switch({ label: 'Enable user import for ADMINS', value: true })
	@IsOptional()
	@IsBoolean()
	status?: boolean;
	/*
    @deprecated Only 'admin' and 'super-admin' should have this permission
    @IsOptional()
    @IsArray()
    @IsEnum(PlekUserRoles, {each: true})
    roles?: PlekUserRoles[];
    */
}
