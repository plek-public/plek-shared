import { Checkbox } from '../annotations/Checkbox';

export class DigestSettings {
	@Checkbox({ label: 'Allow user to configure digest' })
	userCanSetDigest?: boolean;
	@Checkbox({ label: 'Enforce featured messages to be put on top' })
	featuredOnTop?: boolean;
	@Checkbox({ label: 'Set default opt in/out of digest for users on the platform' })
	mode?: boolean;
}
