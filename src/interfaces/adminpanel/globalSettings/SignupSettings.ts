import { Type } from '@nestjs/class-transformer';
import { IsBoolean, IsOptional, ValidateNested } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';

export class SignupSettings {
	@Type(() => PasswordResetSetting)
	@IsOptional()
	@ValidateNested()
	duplicateUserError?: PasswordResetSetting;
}

class PasswordResetSetting {
	@Checkbox({ label: 'Do not send reset password email' })
	@IsOptional()
	@IsBoolean()
	noResetPassword?: boolean;
}
