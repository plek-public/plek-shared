export interface SchemaInterface {
	name: string;
	description: string;
	version: number;
	elasticSearchAggregations: SchemaElasticSearchAggregationInterface[];
	regions: SchemaRegionInterface[];
}
export interface SchemaElasticSearchAggregationInterface {
	key: string; // inside of ES
	aggregationType: string;
	label: string;
	path?: string; // path on the entity object
}

export type SchemaRegionPropertyFieldType =
	| 'tag'
	| 'tag_and_add'
	| 'dropdown'
	| 'textfield'
	| 'textarea'
	| 'label'
	| 'display'
	| 'email'
	| 'date'
	| 'phone'
	| 'linked_in'
	| 'radiobutton'
	| 'checkbox'
	| 'group'
	| 'file'
	| 'boolean';

export interface SchemaRegionPropertyInterface {
	id: string;
	name: string;
	region: string;
	field_type: SchemaRegionPropertyFieldType;
	enabled: boolean;
	mandatory: boolean;
	deletable: boolean;
	weight: number;
	display_options: {
		menu: boolean;
		search: boolean;
		sort: boolean;
		page: boolean;
		edit: boolean;
		editable: boolean;
		userexport?: boolean;
		connection?: boolean;
		numericalSearchable?: boolean;
	};
	defaultValue: any;
}

export interface SchemaRegionInterface {
	name: string;
	properties: SchemaRegionPropertyInterface[];
}
