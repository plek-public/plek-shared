import { Switch } from '../annotations/Switch';
import { Input } from '../annotations/Input';

// TODO: Properties 'disabled' and 'email' exist on schema but are never used, 'text' is not defined on schema but is used in ejs files
export class FooterInfoSettings {
	@Switch({ label: 'Status' })
	status: boolean;
	@Input({ label: 'Explanatory text for footer info' })
	text?: string;
	@Input({ label: 'HTML source for footer info' })
	source?: string;
}
