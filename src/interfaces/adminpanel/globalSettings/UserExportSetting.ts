import { IsBoolean, IsOptional } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';

export class UserExportSetting {
	@Checkbox({ label: 'Export dates without time' })
	@IsOptional()
	@IsBoolean()
	dateWithoutTime?: boolean;
}
