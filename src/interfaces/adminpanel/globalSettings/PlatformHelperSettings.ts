import { IsBoolean } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';

export class PlatformHelperSettings {
	@Switch({ label: 'Enable helper functionalities for users within the platform' })
	@IsBoolean()
	status: boolean;
}
