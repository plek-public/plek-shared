import { Type } from '@nestjs/class-transformer';
import { IsNumber, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';
import { ITranslations } from './Translations';

export class IAppMenu {
	@IsNumber()
	index: number;

	@IsString()
	icon: string;

	@IsString()
	title: string;

	@IsString()
	type: string;

	@Type(() => ITranslations)
	@IsOptional()
	@ValidateNested()
	translations?: ITranslations;
}
