import { IsBoolean } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';

export class GoogleDriveIntegrationSettings {
	@Switch({ label: 'Enable Google Drive integration throughout the platform' })
	@IsBoolean()
	status: boolean;
}
