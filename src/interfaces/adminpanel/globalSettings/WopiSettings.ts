import { IsBoolean, IsOptional, IsString } from '@nestjs/class-validator';
import { Input } from '../annotations/Input';
import { Switch } from '../annotations/Switch';
import { Checkbox } from '../annotations/Checkbox';

export class WopiSettings {
	@Switch({ label: 'Allow users to edit Plek documents on Microsoft Office Online' })
	@IsBoolean()
	status: boolean;

	@Checkbox({ label: 'Validate users Microsoft Office Online license (Enable for business use)', value: true })
	@IsBoolean()
	business: boolean;

	@Input({ label: 'On premise WOPI instance url (leave empty for cloud version)', placeholder: 'Type here...' })
	@IsOptional()
	@IsString()
	discoveryUrl?: string;
}
