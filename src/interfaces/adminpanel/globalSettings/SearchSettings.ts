import { Input } from '../annotations/Input';

export class SearchSettings {
	aggregation?: AggregationSetting;
	group?: {
		aggregation?: GroupSearchAggregationSetting;
	};
	user?: {
		aggregation?: UserSearchAggregationSetting;
	};
}

class AggregationSetting {
	@Input({ label: 'Set search aggregation time in milliseconds' })
	limit: number;
}

class GroupSearchAggregationSetting {
	@Input({ label: 'Make Channels module visible in adminpanel' })
	limit: number;
}

class UserSearchAggregationSetting {
	@Input({ label: 'Number of filter options to maximum show in search sidebar (default: 100)' })
	limit: number;
}
