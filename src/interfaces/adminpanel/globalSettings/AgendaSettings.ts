import { Checkbox } from '../annotations/Checkbox';

export class AgendaSettings {
	@Checkbox({ label: 'Allow group agendas only on the platform' })
	groupAgendasOnly: boolean;
}
