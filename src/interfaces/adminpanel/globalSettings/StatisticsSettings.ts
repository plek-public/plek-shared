import { IsBoolean } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';

export class StatisticsSettings {
	@Checkbox({ label: 'Enable statistics export on the platform' })
	@IsBoolean()
	exports: boolean;
}
