import { Input } from '../annotations/Input';
import { Checkbox } from '../annotations/Checkbox';

export class Dashboard {
	@Checkbox({ label: 'Include channels' })
	showChannels: boolean;
	@Checkbox({ label: 'Include groups where user is member of' })
	showGroups: boolean;
	@Checkbox({ label: 'Include other open groups' })
	showOtherGroups: boolean;
	@Checkbox({ label: 'Only include featured posts' })
	featuredOnly: boolean;
	@Input({ label: 'Channel ids to include' })
	showSpecificChannels: string[];
	@Input({ label: 'Only show groups of this categories' })
	showSpecificGroupCategories: string[];
	@Input({ label: 'Number of days a post can be important' })
	dayRange: number;

	backdrop?: {
		textColor: string;
		backgroundColor: string;
	};
}
