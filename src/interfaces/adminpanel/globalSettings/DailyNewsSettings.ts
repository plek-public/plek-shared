import { IsBoolean } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';

export class DailyNewsSettings {
	@Switch({ label: 'Enable daily news for users on platform' })
	@IsBoolean()
	status: boolean;
}
