import { Type } from '@nestjs/class-transformer';
import { IsBoolean, ValidateNested } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';
import { Checkbox } from '../annotations/Checkbox';

export class MicrosoftOfficeIntegrationSync {
	@IsBoolean()
	syncUsers: boolean;

	@IsBoolean()
	syncDocuments: boolean;

	@IsBoolean()
	syncGroups: boolean;

	@IsBoolean()
	syncMembersOfGroups: boolean;
}

export class MicrosoftOfficeIntegration {
	@Switch({ label: 'Status' })
	@IsBoolean()
	status: boolean;

	@Type(() => MicrosoftOfficeIntegrationSync)
	@ValidateNested()
	sync: MicrosoftOfficeIntegrationSync;

	@Checkbox({ label: 'Allow users to start call from profile' })
	@IsBoolean()
	skypeCall: boolean;

	@Checkbox({ label: 'Allow users to start call from group chat' })
	@IsBoolean()
	teamsCall: boolean;

	@Checkbox({ label: 'Allow users to link M365 document in Plek pages' })
	@IsBoolean()
	searchOffice365DocumentsInPages: boolean;

	@Checkbox({ label: 'Allow users to link M365 Teams chat with Plek Group Chat' })
	@IsBoolean()
	linkMicrosoftTeamsChat: boolean;

	@Checkbox({ label: 'Send selected Plek push notifications via Teams' })
	@IsBoolean()
	pushNotificationViaTeams: boolean;
}
