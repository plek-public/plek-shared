import { IsOptional, IsString } from '@nestjs/class-validator';
import { Input } from '../annotations/Input';

export class MultiPlekSidebarSettings {
	@Input({ label: "This link will be used for 'New plek team' button in Multiplek sidebar (no button will appear if left empty)" })
	@IsOptional()
	@IsString()
	newPlekTeamUrl: string;
}
