import { Type } from '@nestjs/class-transformer';
import { IsBase32, IsBoolean, IsOptional, IsPositive, ValidateNested } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';
import { Input } from '../annotations/Input';

export class TimelineSettings {
	@Type(() => CommentSetting)
	@IsOptional()
	@ValidateNested()
	onComment?: CommentSetting;
}

class CommentSetting {
	@Checkbox({ label: 'Push new comments to top' })
	@IsBoolean()
	pushToTop: boolean;
	@Input({ label: 'Amount of comments that are displayed before collapsing the entire comment thread' })
	@IsPositive()
	numberOfComments: number;
}
