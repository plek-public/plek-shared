import { IsBoolean } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';

export class ReleaseNotesSettings {
	@Switch({ label: 'Status' })
	@IsBoolean()
	status: boolean;
}
