import { IsBoolean, IsString } from '@nestjs/class-validator';
import { Input } from '../annotations/Input';
import { Switch } from '../annotations/Switch';

export class LandingpageSettings {
	@Switch({ label: 'Enable setting custom landingpage within the platform' })
	@IsBoolean()
	status: boolean;

	@Input({ label: 'Set route as default landingpage', placeholder: 'https://custom-url' })
	@IsString()
	href: string;
}
