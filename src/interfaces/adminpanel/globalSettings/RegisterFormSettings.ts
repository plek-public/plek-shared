import { IsArray, IsEnum } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';
import { Textarea } from '../annotations/Textarea';
import { Input } from '../annotations/Input';

export class FormSettingsLink {
	@Input({ label: 'Link text' })
	linkText: string;
	@Input({ label: 'Href' })
	linkHref: string;
}

export class RegisterFormSettings {
	@Switch({ label: 'Status' })
	status: boolean;
	@Textarea({ label: 'HTML source for form' })
	html?: string;
	@IsArray()
	customLinks?: Array<FormSettingsLink>;
}
