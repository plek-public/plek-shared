import { IsBoolean } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';

export class EmployeeEngagementSettings {
	@Switch({ label: 'Status' })
	@IsBoolean()
	status: boolean;
}
