import { IsOptional, IsBoolean } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';

export class UpsellSettings {
	@Checkbox({ label: 'Hide upsell menu item in user dropdown' })
	@IsOptional()
	@IsBoolean()
	hideUpsellMenuItem?: boolean;
}
