import { PlekUserRoles } from '../PlekUserRoles';
import { Input } from '../annotations/Input';
import { Switch } from '../annotations/Switch';
import { IsArray, IsBoolean, IsEnum, IsNumber, IsObject, IsOptional, IsPositive, IsString, ValidateNested } from '@nestjs/class-validator';
import { Type } from '@nestjs/class-transformer';

export class ContentPages {
	@Type(() => IContentPageCategories)
	@IsArray()
	@ValidateNested({ each: true })
	categories: IContentPageCategories[];
}

export class TaxonomyValue {
	@Input({ label: 'Taxonomy type' })
	type: string;
	@Input({ label: 'Taxonomy slug' })
	slug: string;
}

export class IContentPageCategories {
	@Switch({ label: 'Enabled' })
	@IsBoolean()
	enabled: boolean;
	@Input({ label: "Id of the content page, can't be changed later" })
	@IsString()
	id: string;

	@Input({ label: "Category (like id?) of the content page, can't be changed later" })
	@IsOptional()
	@IsString()
	category?: string;

	@Input({ label: 'Displayed name of content page category' })
	@IsString()
	name: string;

	@Input({ label: 'Displayed plural name of content page category' })
	@IsOptional()
	@IsString()
	namePlural?: string;

	@IsOptional()
	@IsString()
	description?: string;

	@IsString()
	icon: string;

	@IsObject()
	taxonomy?: {
		[key: string]: TaxonomyValue;
	};

	@IsArray()
	@IsEnum(PlekUserRoles, { each: true })
	permissions: PlekUserRoles[];

	@IsOptional()
	@IsNumber()
	@IsPositive()
	monthsSinceLastReviewAlert?: number;

	extraOptions: {
		hasRating: boolean;
		hasComments: boolean;
		canShare: boolean;
		hasBookmarks: boolean;
		labels: {
			items: string;
			newItem: string;
			myItems: string;
		};
	};
}
