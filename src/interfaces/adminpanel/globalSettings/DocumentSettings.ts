import { PlekUserRoles } from '../PlekUserRoles';
import { Switch } from '../annotations/Switch';
import { Checkbox } from '../annotations/Checkbox';

export class DocumentSettings {
	@Checkbox({ label: 'Enable folders in documents' })
	folders?: boolean;
	@Checkbox({ label: 'Allow admins to upload HTML documents and render them in the document preview' })
	allowHTML?: boolean;
	@Checkbox({ label: "Don't allow users to download documents (Preview only)" })
	restrictionsDocumentDownload?: boolean;
	@Checkbox({ label: 'Only enable group documents (no public documents)' })
	groupDocumentsOnly?: boolean;
	@Checkbox({ label: 'Disable preview within document overlay' })
	previewDisabled?: boolean;
	// TODO: Should replace towards permissions screen
	permissions?: PlekUserRoles[];
}
