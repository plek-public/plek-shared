import { IsArray, IsOptional, IsString } from '@nestjs/class-validator';
import { Input } from '../annotations/Input';

export class IframeDashboardWidgetSettings {
	@Input({ label: 'Select channels to be shown' })
	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	// Somehow on playground.plek.co this is of type string
	// TODO: Should add validation to only accept unique id's?
	channels?: string[];

	@Input({ label: 'Set editorial channel ID' })
	@IsOptional()
	@IsString()
	editorial?: string;

	@Input({ label: 'Whitelist' })
	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	whitelist?: string[];
}
