import { Input } from '../annotations/Input';
import { VhostGroupCategoryInterface } from './VhostGroupCategory';
import { Checkbox } from '../annotations/Checkbox';
import { IsArray, ValidateNested } from '@nestjs/class-validator';
import { Type } from '@nestjs/class-transformer';
import { Textarea } from '../annotations/Textarea';

export class GroupSettings {
	@Checkbox({ label: 'Enable pages in groups' })
	pages?: boolean;
	@Checkbox({ label: 'Enable chat in groups' })
	chat?: boolean;
	@Textarea({ label: 'Default text in emails when admin invites a user to a group', placeholder: 'Type here...' })
	welcomeEmail: string;
	@Type(() => VhostGroupCategoryInterface)
	@IsArray()
	@ValidateNested({ each: true })
	categories: VhostGroupCategoryInterface[];
}
