import { IsBoolean, IsOptional } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';

class EmailExtensionWhiteListItem {
	@IsBoolean()
	status: boolean;

	items: string[];
}

export class PermissionsSettings {
	@Checkbox({ label: 'Enable group creation on platform' })
	@IsOptional()
	@IsBoolean()
	canCreateGroups?: boolean;

	@Checkbox({ label: "Enable user creation by roletype 'user'" })
	@IsOptional()
	@IsBoolean()
	usersAllowedToCreateUsers?: boolean;

	@Checkbox({ label: "Set role type to 'external' for users that were created by of users of role  'user'" })
	@IsOptional()
	@IsBoolean()
	userCreatedUserHasRoleExternalAccess?: boolean;

	@IsOptional()
	emailExtensionWhiteList?: EmailExtensionWhiteListItem;
}
