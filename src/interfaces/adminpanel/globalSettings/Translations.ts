import { IsString } from '@nestjs/class-validator';
import { Input } from '../annotations/Input';

export class TranslatedKeyToString {
	@Input({ label: 'Dutch' })
	['nl_NL']?: string;
	@Input({ label: 'English' })
	['en_US']?: string;
}

export class TranslatedKeyToStringArray {
	@Input({ label: 'Dutch' })
	['nl_NL']?: string[];
	@Input({ label: 'English' })
	['en_US']?: string[];
}

class TranslationValue {
	@Input({ label: 'Set custom title translation' })
	@IsString()
	title: string;
}

export class ITranslations {
	// TODO: Add `ValidateNested` when defining the keys properly
	[language: string]: TranslationValue;
}
