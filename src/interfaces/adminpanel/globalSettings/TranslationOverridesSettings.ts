import { Input } from '../annotations/Input';
import { TranslatedKeyToString } from './Translations';

export class Translation {
	[id: string]: TranslatedKeyToString;
}

export class TranslationOverridesSettings {
	@Input({ label: 'Describe label that should be overwritten' })
	id?: Translation;
	@Input({ label: 'Label to be overwritten' })
	key?: Translation;
}
