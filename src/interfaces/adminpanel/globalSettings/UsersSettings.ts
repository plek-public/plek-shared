import { Checkbox } from '../annotations/Checkbox';
import { PlekUserRoles } from '../PlekUserRoles';
import { Input } from '../annotations/Input';

export class UsersSettings {
	@Input({ label: 'Set user roles that are allowed on this vhost' })
	roles?: PlekUserRoles[];
	@Checkbox({ label: 'Only show activated users throughout the platform' })
	showOnlyActivated: boolean;
}
