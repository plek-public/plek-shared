import { Type } from '@nestjs/class-transformer';
import { IsArray, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';

export class IModelVhostGlobalSettingRuleBasedOnboarding {
	role: string[] | string;
	notRole: string[] | string;

	@IsOptional()
	@IsArray()
	@ValidateNested({ each: true })
	@Type(() => IOnBoardingStepTemplateOLD)
	steps?: IOnBoardingStepTemplateOLD[];
}

class OnboardingStepTemplateImage {
	@IsString()
	dashboard: string;

	@IsString()
	timeline: string;
}

export class IOnBoardingStepTemplateOLD {
	@IsString()
	id: string;

	@IsOptional()
	@IsString()
	condition?: string;

	@IsString()
	title: string;

	@IsString()
	body: string;

	@IsString()
	type: string;

	@Type(() => OnboardingStepTemplateImage)
	@IsOptional()
	@IsArray()
	@ValidateNested({ each: true })
	images?: OnboardingStepTemplateImage[];
}
