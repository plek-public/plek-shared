import { Type } from '@nestjs/class-transformer';
import { IsBoolean, IsOptional, ValidateNested } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';

export class TranslationSettings {
	@Type(() => ContentPageSetting)
	@IsOptional()
	@ValidateNested()
	CONTENT_PAGE?: ContentPageSetting;

	@Type(() => MessageSetting)
	@IsOptional()
	@ValidateNested()
	MESSAGES?: MessageSetting;
}

class ContentPageSetting {
	@Switch({ label: 'Enable Contentpage translation possibilities (this includes all contentpage types)' })
	@IsBoolean()
	available: boolean;
}

class MessageSetting {
	@Switch({ label: 'Enable Messages translation possibilities' })
	@IsBoolean()
	available: boolean;
}
