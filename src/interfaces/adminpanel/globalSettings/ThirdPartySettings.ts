import { IsBoolean, IsOptional } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';

export class ThirdPartySettings {
	@Checkbox({ label: 'Allow user search through a thirdParty source' })
	@IsOptional()
	@IsBoolean()
	userSearch?: boolean;

	@Checkbox({ label: 'Allow user profile search through a thirdParty source' })
	@IsOptional()
	@IsBoolean()
	userProfile?: boolean;

	@Checkbox({ label: 'Allow user profile to be public (opt in/out possibility for user in User create/edit screen)' })
	@IsOptional()
	@IsBoolean()
	userCanMakeProfilePublic?: boolean;
}
