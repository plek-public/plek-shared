import { IsBoolean, IsOptional } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';
import { Switch } from '../annotations/Switch';

export class SubgroupManagerSettings {
	@Switch({ label: 'Enable subgroup manager within groups' })
	@IsOptional()
	@IsBoolean()
	status?: boolean;
}
