export class MediaSettings {
	strictMediaPermissions?: boolean;
	video?: {
		status: boolean;
		adminOnly?: boolean;
	};
}
