import { Type } from '@nestjs/class-transformer';
import { IsNumber, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';

class ThemeBackground {
	@IsString()
	path: string;

	@IsOptional()
	@IsString()
	backgroundColor?: string;
}

class VhostBackground {
	@Type(() => ThemeBackground)
	@IsOptional()
	@ValidateNested()
	dashboard?: ThemeBackground;

	@Type(() => ThemeBackground)
	@IsOptional()
	@ValidateNested()
	login?: ThemeBackground;
}

class AppColor {
	@IsOptional()
	@IsString()
	primaryHighlight?: string;
}

class AppLogo {
	@IsOptional()
	@IsString()
	url?: string;

	@IsOptional()
	@IsNumber()
	width?: number;

	@IsOptional()
	@IsNumber()
	height?: number;
}

class VhostApp {
	@Type(() => AppColor)
	@IsOptional()
	@ValidateNested()
	color?: AppColor;

	@Type(() => AppLogo)
	@IsOptional()
	@ValidateNested()
	logo?: AppLogo;
}

export class IVhostTheme {
	/**
	 * @deprecated please use 'themecolor' instead of 'color'
	 * @TODO: check where backend still uses this property and remove support for it
	 */
	@IsOptional()
	@IsString()
	color?: string;

	@IsOptional()
	@IsString()
	logo?: string;

	@IsOptional()
	@IsString()
	logoLarge?: string;

	@Type(() => VhostBackground)
	@IsOptional()
	@ValidateNested()
	background?: VhostBackground;

	@Type(() => VhostApp)
	@IsOptional()
	@ValidateNested()
	app?: VhostApp;
}
