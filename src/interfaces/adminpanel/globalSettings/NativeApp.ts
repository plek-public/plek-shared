import { IsArray, IsBoolean, IsMobilePhone, IsNumber, IsOptional, IsString } from '@nestjs/class-validator';
import { Input } from '../annotations/Input';
import { Checkbox } from '../annotations/Checkbox';
import { Switch } from '../annotations/Switch';

export class NativeApp {
	@Switch({ label: 'Status' })
	@IsBoolean()
	status: boolean;

	@Checkbox({ label: 'Allow to periodically ask the user to rate the App' })
	@IsOptional()
	@IsBoolean()
	askForRating?: boolean;

	@Checkbox({ label: 'Enforce login via biometric authentication for users of the App' })
	@IsOptional()
	@IsBoolean()
	forceBiometricAuth?: boolean;

	@Input({ label: 'Itunes connect id of app' })
	@IsNumber()
	// @Matches()
	id: number;

	@Checkbox({ label: 'App is only available via b2b store (Generate and import tokens)' })
	@IsOptional()
	@IsBoolean()
	b2b?: boolean;

	@Input({ label: 'If b2b is in multiple countries, specify list here' })
	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	countries?: string[];
}
