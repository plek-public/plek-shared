import { IsArray, IsBoolean, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';
import { Type } from '@nestjs/class-transformer';
import { Input } from '../annotations/Input';

enum NotificationCategories {
	EMAIL = 'EMAIL',
	NOTIFICATIONICON = 'NOTIFICATIONICON',
	PUSHDESKTOP = 'PUSHDESKTOP',
	PUSHMOBILE = 'PUSHMOBILE',
}

type NotificationLabels = 'MESSAGE_COMMENT' | 'MESSAGE_LIKE' | 'NEW_CHANNEL_MESSAGE' | 'NEW_GROUP_MESSAGE';

class NotificationSettingsValue {
	@IsArray()
	@IsString({ each: true })
	fields: NotificationCategories[];

	@Checkbox({ label: 'Opt in email notifications for users' })
	@IsBoolean()
	mailOptIn: boolean;

	@IsArray()
	@IsString({ each: true })
	notifications: string[];

	@Checkbox({ label: 'Opt in push notifications for users' })
	@IsBoolean()
	pushOptIn: boolean;

	@IsBoolean()
	@Checkbox({ label: 'Enable platform wide user access to notifications' })
	userAccess: boolean;

	@IsBoolean()
	@Checkbox({ label: 'Enable user access per group' })
	userAccessPerGroup: boolean;

	@IsBoolean()
	externalUserConfiguration?: boolean;
}

export class NotificationSettings {
	// TOOD: Add the `@ValidateNested` once the keys are defined
	@Input({ label: 'Message comment' })
	@Type(() => NotificationSettingsValue)
	@IsOptional()
	@ValidateNested()
	['MESSAGE_COMMENT']: NotificationSettingsValue;
	@Input({ label: 'Message like' })
	@Type(() => NotificationSettingsValue)
	@IsOptional()
	@ValidateNested()
	['MESSAGE_LIKE']: NotificationSettingsValue;
	@Input({ label: 'New channel message' })
	@Type(() => NotificationSettingsValue)
	@IsOptional()
	@ValidateNested()
	['NEW_CHANNEL_MESSAGE']: NotificationSettingsValue;
	@Input({ label: 'New group message' })
	@Type(() => NotificationSettingsValue)
	@IsOptional()
	@ValidateNested()
	['NEW_GROUP_MESSAGE']: NotificationSettingsValue;
}
