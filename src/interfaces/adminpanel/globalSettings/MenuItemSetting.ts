import { ITranslations } from './Translations';
import { Switch } from '../annotations/Switch';
import { Input } from '../annotations/Input';
import { Checkbox } from '../annotations/Checkbox';

class MenuItem {
	// @Switch({ label: 'Enable menu item'})
	status?: boolean;
	@Input({ label: 'Set nav icon' })
	navIcon?: string;
	@Checkbox({ label: 'Hide title on desktop' })
	hideTitleOnDesktopApp?: boolean;
	@Input({ label: 'Set custom translations' })
	translations?: ITranslations;
	@Checkbox({ label: 'Enable page description' })
	isShowingDescription?: boolean;
	@Input({ label: 'Page description header' })
	pageDescriptionHeader?: string;
	@Input({ label: 'Page description description' })
	pageDescription?: string;
	@Input({ label: 'Path' })
	path: string;
	@Input({ label: 'Search url' })
	search: string;
	@Input({ label: 'Menu item title' })
	title: string;
	@Input({ label: 'Menu item icon' })
	icon: string;
}

export class MenuItemSetting {
	['users']: MenuItem;
	['contentpages']: MenuItem;
	['messages']: MenuItem;
	['groups']: MenuItem;
	['agenda']: MenuItem;
	['tasks']: MenuItem;
	['projects']: MenuItem;
	['organizations']: MenuItem;
	['lessons']: MenuItem;
	['knowledgebase']: MenuItem;
	['applications']: MenuItem;
	['documents']: MenuItem;
	// TODO: On should exclude the other within the platfrom, both cannot be enabled at the same time
	['documentsM365']?: MenuItem;
	['documentsGdrive']?: MenuItem;
}
