import { TimezoneEnum } from '../TimezoneEnum';
import { Checkbox } from '../annotations/Checkbox';
import { Type } from '@nestjs/class-transformer';
import { IsBoolean, IsEnum, ValidateNested } from '@nestjs/class-validator';
import { ModuleDescription } from '../annotations/ModuleDescription';

export class LocalizationSettings {
	@Type(() => TimeZoneSetting)
	@ValidateNested()
	timezone: TimeZoneSetting;
}

class TimeZoneSetting {
	/*
	 * This is a tryout with use of extra explanatory information
	 * */
	@ModuleDescription(
		'Timezone settings allows the platform to correctly display date and time, according to the timezone where the user is currently at'
	)
	@IsEnum(TimezoneEnum)
	vhostTimezone: TimezoneEnum;

	@Checkbox({ label: 'Allow users to select timezone for themselves on platform' })
	@IsBoolean()
	isUserSelectionAllowed: boolean;
}
