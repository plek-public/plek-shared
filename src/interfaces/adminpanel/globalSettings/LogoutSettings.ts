import { PlekUserRoles } from '../PlekUserRoles';
import { Input } from '../annotations/Input';
import { IsArray, IsEnum } from '@nestjs/class-validator';

// TODO: Possible options in old screen were '"admin", "super-admin", "user", "demo"', check if this should still be the case
export class LogoutSettings {
	@IsArray()
	@IsEnum(PlekUserRoles, { each: true })
	roles: PlekUserRoles[];
}
