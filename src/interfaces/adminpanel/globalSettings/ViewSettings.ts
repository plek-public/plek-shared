import { IsBoolean, IsOptional } from '@nestjs/class-validator';
import { Checkbox } from '../annotations/Checkbox';
import { Switch } from '../annotations/Switch';

export class ViewSettings {
	@Switch({ label: 'Enable user recovery possibility within User edit screen' })
	@IsOptional()
	@IsBoolean()
	showRecoveryButton?: boolean;
}
