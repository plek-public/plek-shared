import { IsBoolean } from '@nestjs/class-validator';
import { Switch } from '../annotations/Switch';

export class HiddenGroupsSettings {
	@Switch({ label: 'Enable configuring hidden groups within the platform ' })
	@IsBoolean()
	status: boolean;
}
