import 'reflect-metadata';

import { ChatModule } from './modules/ChatModule';
import { ConnectionModule } from './modules/ConnectionModule';
import { ActivationModule } from './modules/ActivationModule';
import { SubscriptionModule } from './modules/SubscriptionModule';
import { BirthdayModule } from './modules/BirthdayModule';
import { BookingModule } from './modules/BookingModule';
import { BookingToolModule } from './modules/BookingToolModule';
import { TasksModule } from './modules/TasksModule';
import { PlekOnboardingModule } from './modules/PlekOnboardingModule';
import { SentensorModule } from './modules/SentensorModule';
import { DashboardModule } from './modules/DashboardModule';
import { NotificationsModule } from './modules/NotificationsModule';
import { ConfettiModule } from './modules/ConfettiModule';
import { HeaderBarModule } from './modules/HeaderBarModule';
import { EmployeeEngagementModule } from './modules/EmployeeEngagementModule';

export enum PlekLanguageEnum {
	EN_US = 'en_US',
	NL_NL = 'nl_NL',
	DE_DE = 'de_DE',
	CZ_CZ = 'cz_CZ',
	PL_PL = 'pl_PL',
	PT_PT = 'pt_PT',
	FR_FR = 'fr_FR',
	HU_HU = 'hu_HU',
	ES_ES = 'es_ES',
}

interface IDisabledModule {
	status: false;
}

const Title =
	(value: string): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('title', value, target, propertyKey);
	};

const Description =
	(value: string): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('description', value, target, propertyKey);
	};

export class IModules {
	@Title('Smart Conversations')
	@Description(
		'Enrich the employee experience with listening at scale, measure employee satisfaction and get team insights. Also powerful for change management.'
	)
	sentensor?: SentensorModule;

	@Title('Booking')
	@Description('Let users book rooms, devices or cars')
	bookingTool?: BookingToolModule;

	@Title('Chat')
	@Description('Enables direct messaging in Plek Groups and 1on1')
	chat?: ChatModule;

	@Title('Connection')
	@Description('Let users make connection with each other, build a network of connected users')
	connection?: ConnectionModule;

	@Title('Account Activation Reminders')
	@Description('(Old activation engine) Send e-mail reminders to users to join/complete profile etc.')
	activationEngine?: ActivationModule;

	@Title('OLD IWA Membership')
	@Description("Don't use")
	subscription?: SubscriptionModule;

	@Title('Startpage')
	@Description('Configure the important news')
	dashboard?: DashboardModule;

	@Title('Birthday')
	@Description('Show list of users with birthday (+ startpage widget)')
	birthday?: BirthdayModule;

	@Title('Tasks')
	@Description('Allow users to create and assign tasks, complete them and give deadlines.')
	tasks?: TasksModule;

	@Title('Activation Manager')
	@Description('Powerful system for platform / employee onboarding, compliance management and other automated work processes')
	plekOnboarding?: PlekOnboardingModule;

	// @Title('Employee Engagement')
	// @Description('This is employee engagement')
	employeeEngagement?: EmployeeEngagementModule;

	@Title('Notification')
	@Description('This is notifcation')
	notifications?: NotificationsModule;

	@Title('Confetti')
	confetti?: ConfettiModule;

	@Title('Header bar')
	@Description('Add html for additional top header, like government logo')
	headerBar?: HeaderBarModule;

	@Title('OLD Booking')
	@Description("Don't use")
	booking?: BookingModule;

	/**
	 * @deprecated this is only for back-compat with IWA
	 */
	memberAccess?: {
		status?: boolean;
	};
	/**
	 * @deprecated New app homepage will replace this
	 */
	dailyNews?: {
		status: boolean; // one third is false, two third is true
	};
}
