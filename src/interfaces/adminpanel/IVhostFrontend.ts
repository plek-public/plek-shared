import { IGlobalSettings } from './IGlobalSettings';
import { IVhost } from './IVhost';

export class IVhostFrontend extends IVhost {
	settings: IGlobalSettings;
	gitVersion?: string;
	plekIsUpdatingMessage?: string;
	parentMultiPlekVhosts?: string[];
	lastReleaseDate?: Date;
	twoFactor: {
		enabled: boolean;
		appName: string;
	};
	normalLoginAllowed?: boolean;
	saml?: {
		status?: boolean;
		samlOnly?: boolean;
	};
	inception: {
		enabled?: boolean;
	};
	communities: {
		enabled: boolean;
	};
	autoTranslate: {
		comments: boolean;
	};
}
