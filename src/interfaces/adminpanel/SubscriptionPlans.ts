export enum SubscriptionPlans {
	CONNECT = 'connect',
	ENGAGE = 'engage',
	EMPOWER = 'empower',
}
