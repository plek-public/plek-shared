import 'reflect-metadata';
import { APN } from './serverSettings/APN';
import { Swagger } from './serverSettings/Swagger';
import { IVhostEmailConfig } from './serverSettings/EmailConfig';
import { ITwoFactorConfig } from './serverSettings/TwoFactorConfig';
import { IModelVhostServerSettingsMicrosoftGraph } from './serverSettings/MicrosoftGraph';
import { IModelVhostServerSettingsGoogleAuth } from './serverSettings/GoogleAuth';
import { AuthenticationSettings } from './serverSettings/AuthenticationSettings';
import { SAMLSettings } from './serverSettings/SamlSettings';
import { MultiplekSettings } from './serverSettings/MultiplekSettings';
import { vhostSCIMSettings } from './serverSettings/SCIMSettings';
import { ILdapConfig } from './serverSettings/ILdapConfig';
import { SamlIDPConfig } from './serverSettings/SamlIDPConfig';
import { OAuthSettings } from './serverSettings/IOauthClientSettings';
import { IOpenIDConnectSettings } from './serverSettings/IOpenIDConnectSettings';
import { Type } from '@nestjs/class-transformer';
import { IsArray, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';
import { PowerBISettings } from './serverSettings/PowerBI';
import { WeeklyDigestSettings } from './serverSettings/WeeklyDigest';
import { UserThemes } from './serverSettings/UserThemes';
import { TaskAssignRules } from './serverSettings/TaskAssignRules';
import { SessionConfig } from './serverSettings/Session';
import { IprovaSettings } from './serverSettings/IprovaSettings';
import { ThirdPartyApiSettings } from './serverSettings/ThirdPartyApi';
import { GenericLoginConfig } from './serverSettings/GenericLogin';
import { ElasticConfig } from './serverSettings/Elastic';
import { AutoTranslateConfig } from './serverSettings/AutoTranslate';
import { ChatbotConfig } from './serverSettings/Chatbot';
import { SignUpConfig } from './serverSettings/SignUp';
import { EmailDomain } from './serverSettings/EmailDomain';
import { DemoConfig } from './serverSettings/Demo';
import { InceptionConfig } from './serverSettings/Inception';

const Title =
	(value: string): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('title', value, target, propertyKey);
	};

const Description =
	(value: string): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('description', value, target, propertyKey);
	};

export class IServerSettings {
	@Title('Apple Push Notification settings')
	@Description('Settings for iPhone push notifications when using custom app')
	@Type(() => APN)
	@IsOptional()
	@ValidateNested()
	apn?: APN;

	@Title('Plek API (Swagger) settings')
	@Description('Enable thirdparty api')
	@Type(() => Swagger)
	@IsOptional()
	@ValidateNested()
	swagger?: Swagger;

	@Title('Post settings')
	@Description('')
	messages?: {
		hideUniqueViewsIndication: boolean;
	};

	@Title('Authentication Settings')
	@Description('Configure password settings')
	@Type(() => AuthenticationSettings)
	@IsOptional()
	@ValidateNested()
	authentication?: AuthenticationSettings;

	@Title('Two factor Authentication')
	@Description('Setup 2-FA')
	@Type(() => ITwoFactorConfig)
	@IsOptional()
	@ValidateNested()
	twoFactorAuthentication?: ITwoFactorConfig;

	@Title('Android notifications')
	@IsOptional()
	@IsString()
	gcm?: string; // In all 0.1% cases the value is 'fake'

	@Title('Playstore')
	@Description('')
	@IsOptional()
	@IsString()
	playstoreId?: string;

	@Title('Email settings')
	@Description('Disable email sending, DKIM or set custom email sending servers')
	@Type(() => IVhostEmailConfig)
	@IsOptional()
	@ValidateNested()
	email?: IVhostEmailConfig;

	@Title('Microsoft 365 integration settings')
	@Description('Sync documents to sharepoint, etc')
	@Type(() => IModelVhostServerSettingsMicrosoftGraph)
	@IsOptional()
	@ValidateNested()
	microsoftGraph?: IModelVhostServerSettingsMicrosoftGraph;

	@Title('Google Apps integration settings')
	@Description('Sync documents to Google Drive, etc')
	@Type(() => IModelVhostServerSettingsGoogleAuth)
	@IsOptional()
	@ValidateNested()
	googleAuth?: IModelVhostServerSettingsGoogleAuth;

	@Title('SAML Settings')
	@Description('Login in Plek via SAML')
	@Type(() => SAMLSettings)
	@IsOptional()
	@ValidateNested()
	saml?: SAMLSettings;

	@Title('OAuth2 / OpenID Settings')
	@Description('Login in Plek via OAuth2 / OpenID')
	@Type(() => OAuthSettings)
	@IsOptional()
	@ValidateNested()
	OAuth?: OAuthSettings;

	@Title('SAML Identity provider')
	@Description('Let other applications use Plek login')
	@Type(() => SamlIDPConfig)
	@IsOptional()
	@ValidateNested()
	samlIdp?: SamlIDPConfig;

	@Title('OAuth2/OpenID Identity provider')
	@Description('Let other applications use Plek login')
	@Type(() => IOpenIDConnectSettings)
	@IsOptional()
	@ValidateNested()
	OpenIDConnect?: IOpenIDConnectSettings;

	@Title('LDAP User provisioning')
	@Description('(Please use SCIM)')
	@Type(() => ILdapConfig)
	@IsOptional()
	@ValidateNested()
	ldap?: ILdapConfig;

	@Title('SCIM User provisioning')
	@Description('Automatic create/update/delete users')
	@Type(() => vhostSCIMSettings)
	@IsOptional()
	@ValidateNested()
	SCIMClient?: vhostSCIMSettings;

	@Title('Organizations with members')
	@Description('Members can invite other members within same organization')
	communities?: {
		enabled: boolean;
	};

	@Title('PowerBI Integration')
	@Description('Load statistics into PowerBI')
	@Type(() => PowerBISettings)
	@IsOptional()
	@ValidateNested()
	powerBI?: PowerBISettings;

	@Title('Weekly digest settings')
	@Description('Weekly email containing content of Plek')
	@Type(() => WeeklyDigestSettings)
	@IsOptional()
	@ValidateNested()
	digest?: WeeklyDigestSettings;

	@Title('Custom Plek theme based on users profile field')
	@Description('')
	@Type(() => UserThemes)
	@IsOptional()
	@ValidateNested()
	userThemes?: UserThemes;

	@Title('Task Assign Rules')
	@Description('Limit who can assign tasks to who')
	@Type(() => TaskAssignRules)
	@IsOptional()
	@ValidateNested()
	taskAssignRules?: TaskAssignRules;

	@Title('Multipleks settings')
	@Description('Configure parent/child vhosts')
	@Type(() => MultiplekSettings)
	@IsOptional()
	@ValidateNested()
	multiplek?: MultiplekSettings;

	@Title('Session settings')
	@Description('Configure session length and rules')
	@Type(() => SessionConfig)
	@IsOptional()
	@ValidateNested()
	session?: SessionConfig;

	@Title('iProva settings')
	@Description('')
	@Type(() => IprovaSettings)
	@IsOptional()
	@ValidateNested()
	iProva?: IprovaSettings;

	@Title('Thirdparty API settings')
	@Description('')
	@Type(() => ThirdPartyApiSettings)
	@IsOptional()
	@ValidateNested()
	thirdParty?: ThirdPartyApiSettings;

	@Title('Customize login screen')
	@Description('')
	@Type(() => GenericLoginConfig)
	@IsOptional()
	@ValidateNested()
	genericLogin?: GenericLoginConfig;

	@Title('Elasticsearch settings')
	@Description('')
	@Type(() => ElasticConfig)
	@IsOptional()
	@ValidateNested()
	elasticsearch?: ElasticConfig;

	// WIP adding more settings
	jitsi?: {
		token: string;
		iss: string;
	};
	@Title('Automatically translation')
	@Description('Translate comments via Deepl')
	@Type(() => AutoTranslateConfig)
	@IsOptional()
	@ValidateNested()
	autoTranslate?: AutoTranslateConfig;

	@Title('Show chat bot')
	@Description('Custom pipedrive chatbot integration')
	@Type(() => ChatbotConfig)
	@IsOptional()
	@ValidateNested()
	chatbot?: ChatbotConfig;

	@Title('Signup')
	@Description('Let users create accounts themselves')
	@Type(() => SignUpConfig)
	@IsOptional()
	@ValidateNested()
	signup?: SignUpConfig;

	@Title('Signup email whitelist')
	@Description('Limit user registration to specific emaildomains')
	@Type(() => EmailDomain)
	@IsOptional()
	@IsArray()
	@ValidateNested({ each: true })
	whitelistEmailDomain?: EmailDomain[];

	@Title('Demo environment')
	@Description('Make this vhost a demo environment')
	@Type(() => DemoConfig)
	@IsOptional()
	@ValidateNested()
	demo?: DemoConfig;

	@Title('Inception integration')
	@Description('')
	@Type(() => InceptionConfig)
	@IsOptional()
	@ValidateNested()
	inception?: InceptionConfig;

	@Title('Subscriptions (OLD IWA)')
	@Description("Don't use")
	subscription?: {
		status?: boolean;
		required?: boolean;
	};

	// Found in 5.8% of all vhosts
	permissions?: {
		minRoles: {
			overrides: {
				[key: string]: {
					any?: {
						create?: {
							role: string;
						};
						delete?: {
							role: string;
						};
						read?: {
							role: string;
						};
						update?: {
							role: string;
						};
					};
					own?: {
						create?: {
							role: string;
						};
						delete?: {
							role: string;
						};
						read?: {
							role: string;
						};
						update?: {
							role: string;
						};
					};
				};
			};
		};
	};
	security?: {
		onlyLogCSRF: boolean;
	};
}
