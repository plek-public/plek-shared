/**
 * Additional information that is not part of the system,
 * but it's used by other departments to identify clients or generate reports.
 */
export class IMetadata {
	/**
	 * The client's ID from CS systems.
	 */
	clientId?: string;
}
