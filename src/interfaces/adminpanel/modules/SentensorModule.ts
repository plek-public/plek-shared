import { ReportTypeEnum } from '../../smartConversations/reports/ReportTypeEnum';
import { MappedType } from '../MappedType';

export class Persona {
	id: number;
	name: string;
	avatar: number;
}

export class SentensorModule {
	customerId: number;
	staging: boolean;
	sendResults: boolean;
	shared?: MappedType<'iterationId', string, ReportTypeEnum[]>;
	personas?: Persona[];
	/**
	 * @deprecated Toggle between new implementation that loads our own data and old implementation that indirectly loads data from Sentensor
	 */
	newVersion?: boolean;
}
