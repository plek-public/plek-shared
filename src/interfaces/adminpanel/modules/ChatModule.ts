export class Jitsi {
	status: boolean;
	domain: string;
	jwt: boolean;
}

export class ChatModule {
	status?: boolean;
	app?: string;
	privateChat?: boolean;
	sidebar?: boolean;
	hasNewChat?: boolean;
	hasZoomIntegration?: boolean;
	hasGoogleMeetIntegration?: boolean;
	jitsi?: Jitsi;
}
