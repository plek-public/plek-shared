import { Input } from '../annotations/Input';

export class HeaderBarModule {
	status: boolean;
	styling?: string;

	@Input({ label: 'TEMPLATE', placeholder: 'Type here...' })
	template?: string;

	links?: string[];
}
