import { Input } from '../annotations/Input';
import { Switch } from '../annotations/Switch';

export class ConnectionModule {
	@Switch({ label: 'Enable' })
	status: boolean;

	@Input({ label: 'Allow groupadmins to invite non-connections' })
	allowGroupInviteConnection: boolean;

	@Input({ label: 'Message users see when they receive a connection request', placeholder: 'Type here...' })
	message?: string;
}
