import { Permission } from 'accesscontrol';

export interface IMinimumRoleDefinition {
	role: IPermissionServiceRoles;
	attributes: string[];
}

export interface IMinimumRoleSettingEntity {
	own: {
		create: IMinimumRoleDefinition;
		read: IMinimumRoleDefinition;
		update: IMinimumRoleDefinition;
		delete: IMinimumRoleDefinition;
	};
	any: {
		create: IMinimumRoleDefinition;
		read: IMinimumRoleDefinition;
		update: IMinimumRoleDefinition;
		delete: IMinimumRoleDefinition;
	};
}

export interface IMinimumRoleSettings {
	PLATFORM_AGENDAITEM: IMinimumRoleSettingEntity;
	PLATFORM_DOCUMENT: IMinimumRoleSettingEntity;
	PLATFORM_CONTENTPAGE: IMinimumRoleSettingEntity;
	PLATFORM_LESSON: IMinimumRoleSettingEntity;
	PLATFORM_MESSAGE: IMinimumRoleSettingEntity;
}

export type IPermissionServiceResult = Permission;

export enum IPermissionServiceRoles {
	EVERYONE = 'EVERYONE',
	READONLY = 'READONLY',
	USER_LIGHT = 'USER_LIGHT',
	USER = 'USER',
	USER_PREBOARDING = 'USER_PREBOARDING',
	USER_PLUS = 'USERPLUS',
	USER_PREBOARDING_PLUS = 'USER_PREBOARDING_PLUS',
	CONTENT_MANAGER = 'CONTENT_MANAGER',
	ADMIN = 'ADMIN',
	SUPER_ADMIN = 'SUPER_ADMIN',
}

export enum IPermissionServiceTypes {
	ANY = 'any',
	OWN = 'own',
}

export enum IPermissionServiceActions {
	CREATE = 'create',
	READ = 'read',
	UPDATE = 'update',
	DELETE = 'delete',
}

export enum IPermissionServiceBaseEntitites {
	PLATFORM_AGENDAITEM = 'PLATFORM_AGENDAITEM',
	PLATFORM_DOCUMENT = 'PLATFORM_DOCUMENT',
	PLATFORM_CONTENTPAGE = 'PLATFORM_CONTENTPAGE',
	PLATFORM_GROUP = 'PLATFORM_GROUP',
	PLATFORM_MESSAGE = 'PLATFORM_MESSAGE',
	PLATFORM_APPLICATION = 'PLATFORM_APPLICATION',
	PLATFORM_USER = 'PLATFORM_USER',
}

export const PermissionServiceTypeArray = [IPermissionServiceTypes.ANY, IPermissionServiceTypes.OWN];
export const PermissionServiceActionArray = [
	IPermissionServiceActions.CREATE,
	IPermissionServiceActions.READ,
	IPermissionServiceActions.UPDATE,
	IPermissionServiceActions.DELETE,
];
export const PermissionServiceRoleArray: string[] = [
	IPermissionServiceRoles.EVERYONE,
	IPermissionServiceRoles.READONLY,
	IPermissionServiceRoles.USER_PREBOARDING,
	IPermissionServiceRoles.USER,
	IPermissionServiceRoles.USER_PLUS,
	IPermissionServiceRoles.CONTENT_MANAGER,
	IPermissionServiceRoles.ADMIN,
	IPermissionServiceRoles.SUPER_ADMIN,
];
export const PermissionServiceBaseEntityArray = [
	IPermissionServiceBaseEntitites.PLATFORM_AGENDAITEM,
	IPermissionServiceBaseEntitites.PLATFORM_DOCUMENT,
	IPermissionServiceBaseEntitites.PLATFORM_CONTENTPAGE,
	IPermissionServiceBaseEntitites.PLATFORM_USER,
	IPermissionServiceBaseEntitites.PLATFORM_APPLICATION,
	// IPermissionServiceBaseEntitites.PLATFORM_MESSAGE,
	IPermissionServiceBaseEntitites.PLATFORM_GROUP,
];

export enum PlekUserRoles {
	DEMO = 'demo',
	USER_LIGHT = 'user-light',
	USER_PREBOARDING = 'user-preboarding',
	USER = 'user',
	CONTENT_MANAGER = 'content-manager',
	ADMIN = 'admin',
	SUPER_ADMIN = 'super-admin',
	PLATFORM_EXTERNAL_ACCESS = 'external-access', // Access to the own groups on platfrom
	PLATFORM_EXTERNAL_ACCESS_LIGHT = 'external-access-light', // Same as platform-external-access without chat
	GROUP_EXTERNAL_ACCESS = 'group-external-access', // Same as platform-external-access but visible only to `members of own group
	MEMBER = 'member',
	USER_PLUS = 'user-plus', // Excludes preboarding user
	USER_PREBOARDING_PLUS = 'user-preboarding-plus', // Includes preboarding user
}
