import { PlekUserRoles } from '../PlekUserRoles';
import { Checkbox } from '../annotations/Checkbox';
import { IsBoolean, IsOptional } from '@nestjs/class-validator';

export class PlekOnboardingModule {
	status: boolean;
	option_label?: string;
	createRoles?: PlekUserRoles[]; // 0.1% of vhosts has value '' instead of an array or undefined
	hasNewProgramResultsInterface: boolean; // 0.1% of vhosts has value 'true'
	demoUserCanCompleteOnboarding?: boolean; // 0.1% of vhosts has this set (in all cases true)
	notifications?: boolean;
	newVersion?: boolean;
	appMenuTitle?: boolean; // 2.1% of vhosts have this set
	allowFullscreenOption?: boolean;
	allowForcedView?: boolean;
	@Checkbox({ label: 'Enable new design: employee journey fullscreen' })
	@IsOptional()
	@IsBoolean()
	employeeJourneyFullscreen?: boolean;
}
