import { Input } from '../annotations/Input';
import { Switch } from '../annotations/Switch';

export class BookingToolModule {
	@Switch({ label: 'Status', value: true })
	status: boolean;

	@Input({ label: 'Default group', placeholder: 'Type here...' })
	defaultGroup?: string;

	app: string;
	initTime?: string;
}
