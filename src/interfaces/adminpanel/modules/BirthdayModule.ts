import { Input } from '../annotations/Input';

export class BirthdayModule {
	status: boolean;
	// TODO: The one who did this should order food for the team

	@Input({ label: 'TITLE', placeholder: 'Type here...' })
	agenda_title?: string;

	@Input({ label: 'SUBTITLE', placeholder: 'Type here...' })
	agenda_subtitle?: string;

	@Input({ label: 'TITLE', placeholder: 'Type here...' })
	agenda_message?: string;
}
