import { IsBoolean, IsOptional } from '@nestjs/class-validator';
import { MappedType } from '../MappedType';

// TODO: Move to GlobalSettings
export class DashboardModule {
	@IsOptional()
	@IsBoolean()
	status?: boolean;

	@IsBoolean()
	isNewsSectionShown: boolean;

	@IsOptional()
	@IsBoolean()
	isReservedRightColumnEnabled?: boolean;
	// TODO add multilanguage annotation
	titleTranslations?: MappedType<'lang', string, string>;
}
