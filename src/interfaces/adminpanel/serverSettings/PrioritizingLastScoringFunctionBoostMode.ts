export type PrioritizingLastScoringFunctionBoostMode = 'multiply' | 'sum' | 'replace' | 'min' | 'max' | 'avg' | undefined;
