import { IsArray, IsBoolean, IsNumber, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';
import { PrioritizingLastScoringFunctionBoostMode } from './PrioritizingLastScoringFunctionBoostMode';
import { Type } from '@nestjs/class-transformer';

class QuerySettings {
	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	fields?: string[];
}

class QuerySettingsWithBoost extends QuerySettings {
	@IsString()
	boostMode: PrioritizingLastScoringFunctionBoostMode;
}

class SearchConfig {
	@Type(() => QuerySettings)
	@IsOptional()
	@ValidateNested()
	querySettings?: QuerySettings;
}

class SearchWithBoostConfig extends SearchConfig {
	@Type(() => QuerySettingsWithBoost)
	@IsOptional()
	@ValidateNested()
	querySettings?: QuerySettingsWithBoost;
}

export class ElasticConfig {
	@IsOptional()
	@IsNumber()
	bulkLimit?: number;

	@IsOptional()
	@IsNumber()
	asyncModelLimit?: number;

	@IsOptional()
	@IsNumber()
	parallelVhost?: number;

	@IsBoolean()
	new: boolean; // Found on all vhosts to be set to true, never false or undefined

	@Type(() => SearchConfig)
	@IsOptional()
	@ValidateNested()
	userSearch?: SearchConfig;

	@Type(() => SearchWithBoostConfig)
	@IsOptional()
	@ValidateNested()
	messageSearch?: SearchWithBoostConfig;

	@Type(() => SearchWithBoostConfig)
	@IsOptional()
	@ValidateNested()
	contentPageSaerch?: SearchWithBoostConfig;

	@Type(() => SearchWithBoostConfig)
	@IsOptional()
	@ValidateNested()
	documentSearch?: SearchWithBoostConfig;

	@Type(() => SearchConfig)
	@IsOptional()
	@ValidateNested()
	groupSearch?: SearchConfig;

	@Type(() => SearchConfig)
	@IsOptional()
	@ValidateNested()
	agendaItemSearch?: SearchConfig;
}
