import { IsBoolean, IsOptional, IsString } from '@nestjs/class-validator';

export class ChatbotConfig {
	@IsBoolean()
	chatbotEnabled: boolean;

	@IsOptional()
	@IsString() // Could make this a UUID
	pipedrivePlaybookUuid?: string;
}
