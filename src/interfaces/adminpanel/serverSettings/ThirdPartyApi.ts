import { IsArray, IsBoolean, IsEnum, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';
import { availableModifications } from './ProfilePictureModifications';
import { Type } from '@nestjs/class-transformer';

export class UserFetchConditions {
	@IsBoolean()
	all: boolean;

	@IsOptional()
	@IsString()
	userAllowedByProfileField?: string;
}

class UserExportProfilePic {
	@IsBoolean()
	enable: true;

	@IsString()
	size: string;

	@IsEnum(availableModifications)
	mode: availableModifications; // Not found on any vhost

	@IsString()
	fieldName: string; // Not found on any vhost
}

class GetUsersConfig {
	@Type(() => UserFetchConditions)
	@ValidateNested()
	fetchCondition: UserFetchConditions;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	profileFieldsToRender?: string[];

	@Type(() => UserExportProfilePic)
	@IsOptional()
	@ValidateNested()
	exportProfilePic?: UserExportProfilePic;
}

class UpdateUserConfig {
	@IsBoolean()
	updateAll: false;
}

export class ThirdPartyApiSettings {
	@Type(() => GetUsersConfig)
	@IsOptional()
	@ValidateNested()
	getUsers?: GetUsersConfig;

	@IsOptional()
	@IsBoolean()
	registration?: boolean; // Found in 0.3% of third party configs

	@IsOptional()
	@IsBoolean()
	userCanMakeProfilePublic?: boolean; // Found in 44.7% of third party configs, only 0.3% is true
	// Not found on any vhost
	@Type(() => UpdateUserConfig)
	@IsOptional()
	@ValidateNested()
	updateUser?: UpdateUserConfig;

	@IsOptional()
	@IsBoolean()
	userProfile: boolean; // only 0.3% is true, everything else is false

	@IsOptional()
	@IsBoolean()
	userSearch: boolean; // only 0.5% is true, everything else is false
}
