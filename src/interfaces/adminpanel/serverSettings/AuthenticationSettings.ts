import { Type } from '@nestjs/class-transformer';
import { IsArray, IsBoolean, IsNumber, IsOptional, IsPositive, IsString, ValidateNested } from '@nestjs/class-validator';

class IpFilterSetting {
	@IsBoolean()
	status: boolean;

	@IsArray()
	@IsString({ each: true })
	ips: string[];
}

export class AuthenticationSettings {
	@IsString()
	level: string;

	@IsNumber()
	age: number;

	@IsBoolean()
	ageStatus: boolean;

	@IsBoolean()
	storeLoginData: boolean;

	@IsOptional()
	@IsString()
	forgotPasswordMessage?: string;

	@Type(() => IpFilterSetting)
	@IsOptional()
	@ValidateNested()
	ipfilter?: IpFilterSetting;
}
