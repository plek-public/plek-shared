import { Type } from '@nestjs/class-transformer';
import { IsBoolean, IsEnum, IsOptional, IsPort, IsSemVer, IsString, ValidateNested } from '@nestjs/class-validator';

export class IVhostEmailConfig {
	@IsOptional()
	@IsBoolean()
	allowUserWithoutEmail?: boolean;

	@IsBoolean()
	blockEmailForNonSuperAdmin: boolean;

	@Type(() => IEmailServerConfig)
	@IsOptional()
	@ValidateNested()
	SMTPServerConfig?: IEmailServerConfig;

	@Type(() => IEmailDKIM)
	@IsOptional()
	@ValidateNested()
	dkim?: IEmailDKIM;
}

export enum AvailableEmailServerMethods {
	SMTP = 'SMTP',
	DIRECT = 'DIRECT',
}

class EmailServerConfigTLS {
	@IsBoolean()
	rejectUnauthorized: boolean;
}

class EmailServerConfigSettings {
	@IsString()
	host: string;

	@IsPort()
	port: number;

	@IsBoolean()
	pool: boolean;

	@Type(() => EmailServerConfigTLS)
	@ValidateNested()
	tls: EmailServerConfigTLS;
}

export class IEmailServerConfig {
	@IsEnum(AvailableEmailServerMethods)
	method: AvailableEmailServerMethods;

	@Type(() => EmailServerConfigSettings)
	@ValidateNested()
	settings: EmailServerConfigSettings;
}

export class IEmailDKIM {
	domainName: string;
	keySelector: string;
	privateKey: string;
}
