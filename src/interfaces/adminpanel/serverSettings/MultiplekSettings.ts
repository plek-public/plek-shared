import { Type } from '@nestjs/class-transformer';
import { IsArray, IsBoolean, IsEnum, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';

export enum EnumMultiplekRelationTypes {
	related = 'related',
	parent = 'parent',
	child = 'child',
}

class SyncProfileFieldOptions {
	@IsOptional()
	@IsBoolean()
	wipeOnEmpty?: boolean;
}

class SyncProfileField {
	@IsString()
	source: string;

	@IsString()
	target: string;

	@Type(() => SyncProfileFieldOptions)
	@IsOptional()
	@ValidateNested()
	options?: SyncProfileFieldOptions;
}

class AutoCreateUserConfig {
	@IsBoolean()
	onParent: boolean;

	@IsBoolean()
	onChild: boolean;
}

class VhostSetting {
	@Type(() => SyncProfileField)
	@IsArray()
	@ValidateNested({ each: true })
	syncProfileFields: SyncProfileField[];

	@Type(() => AutoCreateUserConfig)
	@ValidateNested()
	autoCreateUser: AutoCreateUserConfig;
}
class Vhost {
	@IsString()
	vhost: string;

	@IsEnum(EnumMultiplekRelationTypes)
	relationType: EnumMultiplekRelationTypes;

	@Type(() => VhostSetting)
	@ValidateNested()
	setting: VhostSetting;
}

export class MultiplekSettings {
	@Type(() => Vhost)
	@IsArray()
	@ValidateNested({ each: true })
	relatedVhost: Vhost[];
}
