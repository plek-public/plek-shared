import { IsString } from '@nestjs/class-validator';

export class GenericLoginConfig {
	@IsString()
	loginLabel: string;

	@IsString()
	displayName: string;
}
