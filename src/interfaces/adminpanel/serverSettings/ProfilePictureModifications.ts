export enum availableModifications {
	none = 'none',
	circle = 'circle',
	transparent = 'transparent',
	darkMode = 'darkMode',
}
