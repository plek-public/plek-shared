import { Type } from '@nestjs/class-transformer';
import { IsAlpha, IsArray, IsBoolean, IsNumber, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';

export class IOpenIDConnectSettings {
	@IsBoolean()
	enabled: boolean;

	@IsBoolean()
	encrypted: boolean; // This field is not found on any vhost, unused feature?

	@IsOptional()
	@IsBoolean()
	disablePKCE?: boolean;

	@Type(() => ClientMetadata)
	@IsOptional()
	@IsArray()
	@ValidateNested({ each: true })
	clients?: ClientMetadata[];

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	cookie_keys?: string[];

	@IsOptional()
	signing_keys?: {
		keys: any[]; // Analysis doesn't detail possible type
	};
}

export class AllClientMetadata {
	@IsOptional()
	@IsString()
	client_id?: string;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	redirect_uris?: string[];

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	grant_types?: string[];

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	response_types?: ResponseType[];

	@IsOptional()
	@IsString()
	application_type?: 'web' | 'native';

	@IsOptional()
	@IsNumber()
	client_id_issued_at?: number;

	@IsOptional()
	@IsString()
	client_name?: string;

	@IsOptional()
	@IsNumber()
	client_secret_expires_at?: number;

	@IsOptional()
	@IsString()
	client_secret?: string;

	@IsOptional()
	@IsString()
	client_uri?: string;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	contacts?: string[];

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	default_acr_values?: string[];

	@IsOptional()
	@IsNumber()
	default_max_age?: number;
	// id_token_signed_response_alg?: SigningAlgorithmWithNone | undefined;

	@IsOptional()
	@IsString()
	initiate_login_uri?: string;

	@IsOptional()
	@IsString()
	jwks_uri?: string;

	@IsOptional()
	jwks?: { keys: JWK[] };

	@IsOptional()
	@IsString()
	logo_uri?: string;

	@IsOptional()
	@IsString()
	policy_uri?: string;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	post_logout_redirect_uris?: string[];

	@IsOptional()
	@IsBoolean()
	require_auth_time?: boolean;

	@IsOptional()
	@IsString()
	scope?: string;

	@IsOptional()
	@IsString()
	sector_identifier_uri?: string;
	// subject_type?: SubjectTypes | undefined;
	// token_endpoint_auth_method?: ClientAuthMethod | undefined;

	@IsOptional()
	@IsString()
	tos_uri?: string;

	@IsOptional()
	@IsString()
	tls_client_auth_subject_dn?: string;

	@IsOptional()
	@IsString()
	tls_client_auth_san_dns?: string;

	@IsOptional()
	@IsString()
	tls_client_auth_san_uri?: string;

	@IsOptional()
	@IsString()
	tls_client_auth_san_ip?: string;

	@IsOptional()
	@IsString()
	tls_client_auth_san_email?: string;
	// token_endpoint_auth_signing_alg?: SigningAlgorithm | undefined;
	// userinfo_signed_response_alg?: SigningAlgorithmWithNone | undefined;
	// introspection_signed_response_alg?: SigningAlgorithmWithNone | undefined;
	// introspection_encrypted_response_alg?: EncryptionAlgValues | undefined;
	// introspection_encrypted_response_enc?: EncryptionEncValues | undefined;

	@IsOptional()
	@IsBoolean()
	backchannel_logout_session_required?: boolean;

	@IsOptional()
	@IsString()
	backchannel_logout_uri?: string;
	// request_object_signing_alg?: SigningAlgorithmWithNone | undefined;
	// request_object_encryption_alg?: EncryptionAlgValues | undefined;
	// request_object_encryption_enc?: EncryptionEncValues | undefined;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	request_uris?: string[];
	// id_token_encrypted_response_alg?: EncryptionAlgValues | undefined;
	// id_token_encrypted_response_enc?: EncryptionEncValues | undefined;
	// userinfo_encrypted_response_alg?: EncryptionAlgValues | undefined;
	// userinfo_encrypted_response_enc?: EncryptionEncValues | undefined;
	// authorization_signed_response_alg?: SigningAlgorithm | undefined;
	// authorization_encrypted_response_alg?: EncryptionAlgValues | undefined;
	// authorization_encrypted_response_enc?: EncryptionEncValues | undefined;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	web_message_uris?: string[];

	@IsOptional()
	@IsBoolean()
	tls_client_certificate_bound_access_tokens?: boolean;

	@IsOptional()
	@IsBoolean()
	require_signed_request_object?: boolean;

	@IsOptional()
	@IsBoolean()
	require_pushed_authorization_requests?: boolean;

	@IsOptional()
	@IsBoolean()
	backchannel_user_code_parameter?: boolean;

	@IsOptional()
	@IsString()
	backchannel_authentication_request_signing_alg?: string;

	@IsOptional()
	@IsString()
	backchannel_client_notification_endpoint?: string;
	// backchannel_token_delivery_mode?: CIBADeliveryMode | undefined;

	[key: string]: unknown;
}

export class ClientMetadata extends AllClientMetadata {
	@IsString()
	client_id: string;
}

export type ResponseType = 'code' | 'id_token' | 'code id_token' | 'id_token token' | 'code token' | 'code id_token token' | 'none';

export interface JWK {
	kid?: string | undefined;
	x5c?: string[] | undefined;
	alg?: string | undefined;
	crv?: string | undefined;
	d?: string | undefined;
	dp?: string | undefined;
	dq?: string | undefined;
	e?: string | undefined;
	ext?: boolean | undefined;
	k?: string | undefined;
	key_ops?: string[] | undefined;
	kty?: string | undefined;
	n?: string | undefined;
	p?: string | undefined;
	q?: string | undefined;
	qi?: string | undefined;
	use?: string | undefined;
	x?: string | undefined;
	y?: string | undefined;
}
