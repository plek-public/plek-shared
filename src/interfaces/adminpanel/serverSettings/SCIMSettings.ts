import { Type } from '@nestjs/class-transformer';
import { IsArray, IsBoolean, IsDefined, IsNumber, IsNumberString, IsObject, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';

class vhostScimCron {
	@IsNumberString()
	dayOfMonth: string;

	@IsNumberString()
	dayOfWeek: string;

	@IsNumberString()
	hour: string;

	@IsNumberString()
	minute: string;

	@IsNumberString()
	month: string;

	@IsNumberString()
	second: string;
}

class ADSetting {
	@IsObject()
	ADFields: {
		[SCIMField: string]: Array<{
			model: string;
			field: string;
			default?: boolean | string | number;
			ignoreNullOnLdap?: boolean;
			reverse?: boolean;
		}>;
	};

	@IsArray()
	@IsString({ each: true })
	OUs: string[];

	@IsDefined()
	mapKey: MappingKeyConfigType;
}

export class vhostSCIMConfig {
	@IsString()
	clientVersion: '1.0.3';

	@Type(() => vhostScimCron)
	@IsOptional()
	@ValidateNested()
	cronTab?: vhostScimCron;

	@Type(() => vhostScimCron)
	@IsOptional()
	@ValidateNested()
	cronTabCheckConfig?: vhostScimCron;

	@IsOptional()
	@IsNumber()
	serviceManagerInterval?: number;

	@Type(() => ADSetting)
	@ValidateNested()
	ADSetting: ADSetting;
}

export class ScimOptions {
	@IsBoolean()
	forceDeleteUser: boolean;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	whitelistDomains?: string[];

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	ignoreRoles?: string[];
}

export class vhostSCIMSettings {
	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	validIPs?: string[];

	@IsOptional()
	@IsString()
	token?: string;

	@Type(() => vhostSCIMConfig)
	@IsOptional()
	@ValidateNested()
	config?: vhostSCIMConfig;

	@Type(() => ScimOptions)
	@ValidateNested()
	scimOptions: ScimOptions;

	@IsOptional()
	@IsBoolean()
	enabled?: boolean;
}

export type MappingKeyConfigType =
	| string // @deprecated format (11.1% still has this while 88.9% has object format below)
	| {
			gateway: string;
			plek: string;
	  };
