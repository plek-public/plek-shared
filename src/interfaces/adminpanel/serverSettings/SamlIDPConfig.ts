import { Type } from '@nestjs/class-transformer';
import { IsArray, IsBoolean, IsDivisibleBy, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';

class SamlServiceProvider {
	@IsString()
	name: string;

	@IsOptional()
	@IsString()
	entityID?: string;

	@IsString()
	signingCert: string;

	@IsString()
	encryptCert: string;

	@IsString()
	callBackUrl: string;

	@IsOptional()
	@IsBoolean()
	wantAssertionsSigned?: boolean;

	@IsOptional()
	@IsBoolean()
	wantMessagesSigned?: boolean;
}

class LoginResponseAttribute {
	@IsString()
	name: string;

	@IsString()
	valueTag: string;

	@IsString()
	nameFormat: string;

	@IsString()
	valueXsiType: string;
}

class Template {
	@IsString()
	context: string;
}

class LoginResponseAdditional {
	@Type(() => Template)
	@ValidateNested()
	attributeStatementTemplate: Template;

	@Type(() => Template)
	@ValidateNested()
	attributeTemplate: Template;
}

class LoginResponseTemplate {
	@IsString()
	context: string;

	@Type(() => LoginResponseAttribute)
	@IsArray()
	@ValidateNested({ each: true })
	attributes: LoginResponseAttribute[];

	@Type(() => LoginResponseAdditional)
	@IsOptional()
	@ValidateNested()
	additionalTemplates?: LoginResponseAdditional;
}

// Only ServiceProviders, entityID, status and wantAuthnRequestsSigned are found on servers
export class SamlIDPConfig {
	@IsBoolean()
	status: boolean;

	@IsOptional()
	@IsString()
	entityID?: string;

	@IsString()
	signingCert: string;

	@IsString()
	privateKey: string;

	@IsString()
	encPrivateKey: string;

	@IsString()
	encryptCert: string;

	@Type(() => SamlServiceProvider)
	@IsArray()
	@ValidateNested({ each: true })
	ServiceProviders: SamlServiceProvider[];

	@IsOptional()
	@IsString()
	privateKeyPass?: string;

	@IsOptional()
	@IsString()
	encPrivateKeyPass?: string;

	@IsOptional()
	@IsBoolean()
	isAssertionEncrypted?: boolean;

	@IsOptional()
	@IsBoolean()
	wantAuthnRequestsSigned?: boolean;

	@Type(() => LoginResponseTemplate)
	@ValidateNested()
	loginResponseTemplate: LoginResponseTemplate;
}
