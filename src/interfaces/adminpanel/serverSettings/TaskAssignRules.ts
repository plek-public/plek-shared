import { Type } from '@nestjs/class-transformer';
import { IsArray, IsBoolean, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';

class AssignGroup {
	@IsString()
	from: string;

	@IsArray()
	@IsString({ each: true })
	to: string[];
}

export class TaskAssignRules {
	@IsBoolean()
	all: boolean;

	@IsString()
	type: string; // Not found on any vhost

	@Type(() => AssignGroup)
	@IsOptional()
	@ValidateNested({ each: true })
	groups?: AssignGroup[];
}
