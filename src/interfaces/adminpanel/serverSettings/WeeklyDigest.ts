import { IsBoolean, IsEnum, IsNumber, IsOptional, IsString } from '@nestjs/class-validator';

export enum WeeklyDigestPeriod {
	WEEKLY = 'weekly',
	DAILY = 'daily',
	MONTHLY = 'monthly',
}

export class WeeklyDigestSettings {
	@IsBoolean()
	status: boolean;

	@IsOptional()
	@IsString()
	subject?: string;

	@IsOptional()
	@IsString()
	lastsend?: string;

	@IsOptional()
	@IsNumber()
	chanCount?: number;

	@IsOptional()
	@IsNumber()
	day?: number;

	@IsOptional()
	@IsNumber()
	featuredCount?: number;

	@IsOptional()
	@IsNumber()
	groupCount?: number;

	@IsOptional()
	@IsNumber()
	hour?: number;

	/**
	 * Only in 0.3%
	 */
	@IsOptional()
	@IsNumber()
	openCroupCount?: number;

	@IsOptional()
	@IsNumber()
	agendaCount?: number;

	/**
	 * If set, value is 'weekly' with a very few vhosts having it (probably incorrectly) set to 'Weekly'
	 * @default WeeklyDigestPeriod.WEEKLY
	 */
	@IsEnum(WeeklyDigestPeriod)
	period: WeeklyDigestPeriod;
}
