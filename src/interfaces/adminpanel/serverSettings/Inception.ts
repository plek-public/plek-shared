import { IsBoolean, IsOptional, IsString } from '@nestjs/class-validator';

export class InceptionConfig {
	@IsBoolean()
	enabled: boolean;

	@IsOptional()
	@IsString()
	preHash?: string;

	@IsOptional()
	@IsString()
	postHash?: string;
}
