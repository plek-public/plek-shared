import { IsBoolean, IsOptional } from '@nestjs/class-validator';

export class SignUpConfig {
	@IsOptional()
	@IsBoolean()
	status?: boolean;

	@IsBoolean()
	needActivation: boolean;

	@IsOptional()
	@IsBoolean()
	needPassword?: boolean;

	@IsOptional()
	@IsBoolean()
	canNotSetPassword?: boolean;
}
