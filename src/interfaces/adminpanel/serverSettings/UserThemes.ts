import { IsArray, IsBoolean, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';
import { IVhostTheme } from '../globalSettings/Theme';
import { Type } from '@nestjs/class-transformer';

class Theme {
	@IsString()
	match: string;

	@Type(() => IVhostTheme)
	@ValidateNested()
	theme: IVhostTheme;
}

export class UserThemes {
	@IsBoolean()
	status: boolean;

	@Type(() => Theme)
	@IsOptional()
	@IsArray()
	@ValidateNested({ each: true })
	themes?: Theme[];
}
