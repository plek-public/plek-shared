import { Type } from '@nestjs/class-transformer';
import { IsBoolean, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';
import { Input } from '../annotations/Input';
import { Switch } from '../annotations/Switch';
import { VhostGroupCategoryTaxonomyTypeLink } from '../globalSettings/VhostGroupCategory';

class CustomPushToken {
	@IsString()
	key: string;

	@IsString()
	keyId: string;

	@IsString()
	teamId: string;
}

export class APN {
	@Switch({ label: 'Custom APN connection (only needed when publishing in other Appstore then ILUMY)' })
	@IsBoolean()
	status: boolean;

	@Switch({ label: 'Use production APN push connection', value: true })
	@IsBoolean()
	production: boolean;

	@Input({ label: 'Application topic (Apple app id)', placeholder: 'zzz.yyy.xxx' })
	@IsOptional()
	@IsString()
	topic?: string; // Not found in 15.4% of all APN

	@Input({ label: 'Custom push token when using other Appstore then ILUMY' })
	@Type(() => CustomPushToken)
	@IsOptional()
	@ValidateNested()
	token?: CustomPushToken;

	@IsOptional()
	@IsString()
	cert?: string; // 0.5% found in all APN

	@IsOptional()
	@IsString()
	key?: string; // 0.5% found in all APN
}
