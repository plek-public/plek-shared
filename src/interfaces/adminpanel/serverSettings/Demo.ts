import { IsBoolean, IsOptional, IsString } from '@nestjs/class-validator';

export class DemoConfig {
	@IsBoolean()
	enabled: boolean;

	@IsOptional()
	@IsString()
	demoUserId?: string;
}
