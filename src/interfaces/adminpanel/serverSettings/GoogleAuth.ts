import { Type } from '@nestjs/class-transformer';
import { IsBoolean, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';

export class IModelVhostServerSettingsGoogleAuth {
	@IsString()
	clientID: string;

	@IsOptional()
	@IsString()
	clientSecret?: string;

	@IsOptional()
	@IsString()
	clientEmail?: string; // Not found on any vhost

	@IsOptional()
	@IsString()
	privateKey?: string; // Not found on any vhost

	@Type(() => IModelVhostServerSettingsGoogleAuthSync)
	@IsOptional()
	@ValidateNested()
	sync?: IModelVhostServerSettingsGoogleAuthSync;

	@IsOptional()
	@IsBoolean()
	forceAuthentication?: boolean;
}

export class IModelVhostServerSettingsGoogleAuthSync {
	@IsBoolean()
	syncGroups: boolean;

	@IsBoolean()
	syncDocuments: boolean;

	@IsOptional()
	@IsBoolean()
	publicDocumentsOnDrive?: boolean;

	@IsOptional()
	@IsString()
	platformDefaultFolder?: string;

	@IsOptional()
	@IsString()
	platformDefaultDrive?: string;

	@IsOptional()
	@IsBoolean()
	automaticallyLinkGroupWithDrive?: boolean;
}
