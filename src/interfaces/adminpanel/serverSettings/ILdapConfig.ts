// import { BaseModelInterface } from './BaseModelInterface';

import { Type } from '@nestjs/class-transformer';
import { IsArray, IsBoolean, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';

class Mapping {
	@IsString()
	source: string;

	@IsString()
	target: string;
}

class LdapDefaults {
	@IsString()
	url: string;

	@IsString()
	username: string;

	@IsString()
	password: string;

	@Type(() => Mapping)
	@IsArray()
	@ValidateNested({ each: true })
	mapping: Mapping[];

	@IsArray()
	@IsString({ each: true })
	attributed: string[];
}

export class ILdapConfig {
	@IsOptional()
	@IsBoolean()
	status?: boolean; // Only values of false have been found, does this mean undefined -> true ?
	defaults?: {
		url: string;
		username: string;
		password: string;
		mapping: Array<{
			source: string;
			target: string;
		}>;
		attributes: string[];
		extraConnectionConfigs?: {
			tlsOptions: {
				rejectUnauthorized: boolean;
			};
		};
		chained?: string; // 40% not set, 40% string ('true') and 20% boolean (true), what typing should it be?
		dn: string;
		filter?: string;
		scope?: string;
		sync?: string;
		uniqueIdentifier?: string;
	};
	actions?: ILdapAction[];
	// 81.5% is a boolean (false), 7.4% is below object??
	ldapLogin?: {
		status: boolean;
		ldapLogin?: boolean; // Not found on any vhosts
		ldapOnly: boolean;
		createNewUsers?: boolean; // Not found on any vhosts
		scope: string;
		searchFilter: string;
		uniqueIdentifier?: string;
	};
	defaultConnectionSettings?: any; // Not found on any vhosts
	sleepingTime?: number; // Not found on any vhosts
	userType?: string; // Not found on any vhosts
	lightUpdate?: string; // Not found on any vhosts
	chained?: boolean; // Not found on any vhosts
	sync?: string;
	createNewUsers?: boolean;
	ldapOnly?: boolean;
	removeNonLdapUsers?: boolean;
}

export interface ILdapAction {
	action: string;
	url?: string;
	username?: string;
	password?: string;
	dn?: Object;
	uniqueIdentifier?: boolean;
	mapping?: Array<{
		source: string;
		target: string;
	}>;
	attributes?: string[];
	forceRemoveNonLdapUsers?: boolean;
	validateCertificate?: boolean;
	scope?: string;
	filter?: string;
	extraConnectionConfigs?: any;
	disabled?: any;
	defaultConnectionSettings?: any;
	sleepingTime?: number;
	userType?: string;
	lightUpdate?: string;
	chained?: boolean;
	syncId?: number;
}
