import { AvailableEmailServerMethods } from './EmailConfig';

export interface IEmailServerConfig {
	method: AvailableEmailServerMethods;
	settings: {
		host: string;
		port: number;
	};
}
