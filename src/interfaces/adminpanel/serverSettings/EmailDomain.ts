import { IsArray, IsString } from '@nestjs/class-validator';

export class EmailDomain {
	@IsArray()
	@IsString({ each: true })
	emails: string[];
}
