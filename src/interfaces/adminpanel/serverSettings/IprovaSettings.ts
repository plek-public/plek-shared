import { IsBoolean, IsString } from '@nestjs/class-validator';

export class IprovaSettings {
	@IsBoolean()
	status: boolean;

	@IsString()
	domain: string;

	@IsString()
	trustedApplicationID: string;
}
