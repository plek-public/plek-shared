import { IsBoolean, IsString } from '@nestjs/class-validator';

class GroupMapping {
	@IsString()
	sourcekey: string;

	@IsString()
	sourcevalue: string;

	@IsString()
	groupId: string;

	@IsBoolean()
	loadGroupWithUsersValue: boolean;

	// TODO: fix the typing here
	unsubscribeOtherMembershipsOfGroupCategory: string[] | string;
}

export class IGroupMapping {
	@IsBoolean()
	active: boolean;
	mapping: [
		{
			sourcekey: string;
			sourcevalue: string;
			groupId: string;
			loadGroupWithUsersValue: boolean; // Option to load group by identifier in user object
			unsubscribeOtherMembershipsOfGroupCategory: string[] | string; // Leave groups of this category if not send in this list anymore
		},
	];
}
