import { Type } from '@nestjs/class-transformer';
import { IsBoolean, IsDate, IsNumber, IsOptional, IsSemVer, IsString, ValidateNested } from '@nestjs/class-validator';

export class IModelVhostServerSettingsMicrosoftGraph {
	@IsString()
	clientID: string;

	@IsString()
	clientSecret: string;

	@IsOptional()
	@IsString()
	directoryID?: string;

	@IsOptional()
	@IsBoolean()
	appAccess?: boolean;

	@IsOptional()
	@IsString()
	scopes?: string;

	@IsString()
	sharepointDomain: string;

	@IsOptional()
	@IsBoolean()
	genericAppAuthentication?: boolean;

	@IsOptional()
	@IsBoolean()
	limitDocumentSearchToConnectedSites?: boolean;

	@IsOptional()
	@IsBoolean()
	forceAllUserToOauthAuthorize?: boolean;

	@IsOptional()
	@IsString()
	oauthtoken?: string;

	@Type(() => IModelVhostServerSettingsMicrosoftGraphSync)
	@IsOptional()
	@ValidateNested()
	sync?: IModelVhostServerSettingsMicrosoftGraphSync;
}

class SyncGroupsLog {
	@Type(() => Date)
	@IsDate()
	lastSync: Date;

	@IsNumber()
	synced: number;

	@IsNumber()
	available: number;

	@IsString()
	lastError: string;
}

export class IModelVhostServerSettingsMicrosoftGraphSync {
	@IsString()
	syncUserId: string;

	@IsOptional()
	@IsBoolean()
	syncGroups?: boolean;

	@IsOptional()
	syncDocuments?: boolean;

	@IsOptional()
	@IsString()
	platformDefaultGroup?: string;

	@IsOptional()
	@IsString()
	platformDefaultSite?: string;

	@IsOptional()
	@IsString()
	platformDefaultGroupSubscription?: string;

	@IsOptional()
	@IsString()
	// 20% of all vhosts have this field as undefined except at vrhaaglanden, there it's set to an url
	platformDefaultGroupUrl?: string;

	@Type(() => SyncGroupsLog)
	@ValidateNested()
	syncGroupsLog: SyncGroupsLog;

	@IsOptional()
	@IsBoolean()
	syncUsers?: boolean;

	@IsOptional()
	@IsBoolean()
	syncMembersOfGroups?: boolean;

	@IsOptional()
	@IsBoolean()
	linkGroupToSite?: boolean;

	@IsOptional()
	@IsBoolean()
	publicDocumentsOnSharepoint?: boolean;

	@IsOptional()
	@IsBoolean()
	fallbackToServerToken?: boolean;
}
