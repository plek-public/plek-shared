import { IsBoolean, IsOptional, IsString } from '@nestjs/class-validator';

export class AutoTranslateConfig {
	@IsOptional()
	@IsString()
	deeplApikey?: string;

	@IsBoolean()
	comments: boolean;
}
