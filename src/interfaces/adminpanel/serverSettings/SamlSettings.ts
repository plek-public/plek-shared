// Around 5% of configs with saml are not this object type but SAMLConfigurations object type

import { Type } from '@nestjs/class-transformer';
import { IsArray, IsBoolean, IsMobilePhone, IsOptional, IsString, IS_ALPHA, ValidateNested } from '@nestjs/class-validator';

// bzk.plek.co intranet.hooymijer.nl jplink.nl qhub.nl need to be migrated
export class SAMLSettings {
	@Type(() => SAMLConfigurations)
	@IsOptional()
	@IsArray()
	@ValidateNested({ each: true })
	samlConfigurations?: SAMLConfigurations[];

	@IsOptional()
	@IsBoolean()
	status?: boolean;

	@IsOptional()
	@IsBoolean()
	samlOnly?: boolean;

	@IsOptional()
	@IsString()
	loginLabel?: string;
}

class SAMLConfigurationMapping {
	@IsString()
	source: string;

	@IsString()
	target: string;
}

class SAMLUserFilter {
	@IsBoolean()
	status: boolean;

	@IsString()
	field: string;

	@IsString()
	regex: string;
}

export class SAMLConfigurations {
	@IsBoolean()
	status: boolean;

	@IsOptional()
	@IsString()
	index?: string;

	@IsOptional()
	@IsString()
	xmlUrl?: string;

	@IsString()
	emailExtention: string;

	@IsArray()
	@IsString({ each: true })
	certs: string[];

	@IsString()
	cert: string;

	@IsString()
	metaIssuer: string;

	@IsString()
	encryptCert: string;

	@IsString()
	name: string;

	@IsString()
	nameIDFormat?: string;

	@IsString()
	url: string;

	@IsString()
	logoutRedirectUrl: string;

	@IsString()
	forgotPasswordUrl: string;

	@IsString()
	changePasswordUrl: string;

	@IsString()
	callbackUrl: string;

	@IsString()
	role: string;

	@IsBoolean()
	redirect: boolean;

	@IsBoolean()
	appQROnly: boolean;

	@IsString()
	appQROnlyMessage: string;

	@Type(() => SAMLUserFilter)
	@ValidateNested()
	userFilter: SAMLUserFilter;

	@Type(() => SAMLConfigurationMapping)
	@IsArray()
	@ValidateNested({ each: true })
	mapping: SAMLConfigurationMapping[];

	@IsBoolean()
	createNewUsers: boolean;

	@IsBoolean()
	recoverDeletedUsers: boolean;

	@IsString()
	issuer: string;

	@IsString()
	mappingkey: string;

	@IsBoolean()
	samlOnly: boolean;

	@IsArray()
	@IsString({ each: true })
	samlOnlyRedirectAddress: [string];

	@IsString()
	sendWelcomeMail: string;

	@IsOptional()
	@IsBoolean()
	wantAuthnRequestsSigned?: boolean;

	@IsOptional()
	@IsBoolean()
	isAssertionEncrypted?: boolean;

	@IsOptional()
	@IsBoolean()
	wantMessageSigned?: boolean;

	@IsOptional()
	@IsBoolean()
	samlify?: boolean;

	@IsOptional()
	@IsString()
	logoutUrl?: string;

	@IsOptional()
	@IsString()
	privateCert?: string;

	@IsOptional()
	@IsString()
	authnContext?: string;

	@IsOptional()
	@IsBoolean()
	wantAssertionsSigned?: boolean;

	@IsOptional()
	@IsBoolean()
	disableRequestedAuthnContext?: boolean;
}
