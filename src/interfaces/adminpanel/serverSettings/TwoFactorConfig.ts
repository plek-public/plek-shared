import { Type } from '@nestjs/class-transformer';
import { IsArray, IsBoolean, IsEnum, IsNumber, IsOptional, IsPositive, IsString, ValidateNested } from '@nestjs/class-validator';

export enum ACTION_TYPE {
	// VIEW = 'VIEW',
	// CREATE = 'CREATE',
	UPDATE = 'UPDATE',
	DELETE = 'DELETE',
	DELETE_ALL = 'DELETE_ALL',
}

export enum ACTION_MODEL {
	USER = 'USER',
	GROUP = 'GROUP',
	MESSAGE = 'MESSAGE',
	DOCUMENT = 'DOCUMENT',
	VHOST = 'VHOST',
	AGENDA_ITEM = 'AGENDA_ITEM',
}

export class ITwoFactorSettings {
	@IsOptional()
	@IsString()
	sender?: string;

	@IsString()
	template: string;

	@IsPositive()
	tokenLength: number;

	@IsNumber()
	timeout: number;

	@IsOptional()
	@IsNumber()
	sessionTimeout?: number;

	@IsOptional()
	@IsString()
	authenticatorAppName?: string;

	@IsOptional()
	@IsString()
	authenticatorEnabled?: string; // Found in 28% of all settings
}

export class ITwoFactorMethodAdminActions {
	@IsBoolean()
	enabled: boolean;

	@IsEnum(ACTION_MODEL)
	model: ACTION_MODEL;

	@IsEnum(ACTION_TYPE)
	action: ACTION_TYPE;

	@IsOptional()
	@IsArray()
	@IsString({ each: true })
	roleOverride?: string[];

	@IsBoolean()
	require2FA: boolean;
}

export class ITwoFactorMethodDocumentView {
	@IsBoolean()
	enabled: boolean;

	@IsString()
	matcher: string;

	@IsBoolean()
	require2FA: boolean;
}

export class ITwoFactorMethodEndpointBased {
	@IsBoolean()
	enabled: boolean;

	@IsString()
	matcher: string;

	@IsBoolean()
	regex: boolean;

	@IsBoolean()
	allUsers: boolean;

	@IsOptional()
	@IsString()
	userMatcher?: string | null;
}

class TwoFactorMethods {
	@Type(() => ITwoFactorMethodEndpointBased)
	@IsArray()
	@ValidateNested({ each: true })
	endpointBased: ITwoFactorMethodEndpointBased[];

	@Type(() => ITwoFactorMethodAdminActions)
	@IsArray()
	@ValidateNested({ each: true })
	adminActions: ITwoFactorMethodAdminActions[];

	@Type(() => ITwoFactorMethodDocumentView)
	@IsArray()
	@ValidateNested({ each: true })
	documentView: ITwoFactorMethodDocumentView[];
}

export class ITwoFactorConfig {
	@IsBoolean()
	enabled: boolean;

	@Type(() => ITwoFactorSettings)
	@IsOptional()
	@ValidateNested()
	settings?: ITwoFactorSettings;

	@Type(() => TwoFactorMethods)
	@IsOptional()
	@ValidateNested()
	methods?: TwoFactorMethods;

	@IsOptional()
	@IsString()
	phoneNumberAtLoginRoles?: string;
}

export class ITwoFactorSessionObject {
	requestId?: string | null;
	pendingRequest: boolean;
	verified: boolean;
	validUntil?: number;
}

export enum TwoFactorStatusCode {
	Success = 'success',
	InternalError = 'internal error',
	NotAllowed = 'not allowed',
	BadRequest = 'bad request',
	NotFound = 'not found',
	InsufficientBalance = 'insufficient balance',
	PendingRequest = 'pending request',
	AlreadyVerified = 'already verified',
}

export interface ITwoFactorStatus {
	success: boolean;
	code: TwoFactorStatusCode;
}

export interface ITwoFactorResponse {
	status: ITwoFactorStatus;
	requestId: string | null;
}
