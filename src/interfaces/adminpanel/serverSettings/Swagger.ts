import { IsBoolean, IsOptional } from '@nestjs/class-validator';

export class Swagger {
	@IsBoolean()
	enabled: boolean;

	@IsOptional()
	@IsBoolean()
	thirdParty?: boolean; // Doesn't exist on any vhost

	@IsBoolean()
	internalEndpoints: boolean;
}
