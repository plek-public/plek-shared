import { IsBoolean } from '@nestjs/class-validator';

export class PowerBISettings {
	@IsBoolean()
	status: boolean;
}
