import { IsBoolean, IsNumber } from '@nestjs/class-validator';

export class SessionConfig {
	@IsBoolean()
	persistent: boolean;

	@IsNumber()
	maxAge: number;

	@IsBoolean()
	renew: boolean;

	@IsBoolean()
	optIn: boolean;

	@IsBoolean()
	nonSessionRequests: boolean;
}
