import { Type } from '@nestjs/class-transformer';
import { IsBoolean, IsOptional, IsString, ValidateNested } from '@nestjs/class-validator';
import { VhostGroupCategoryTaxonomyTypeLink } from '../globalSettings/VhostGroupCategory';
import { IGroupMapping } from './GroupMapping';

class ClientMapping {
	@IsString()
	source: string;

	@IsString()
	target: string;
}

export class IOauthClientSettings {
	@IsBoolean()
	enabled: boolean;

	@IsOptional()
	@IsBoolean()
	noSSO?: boolean;

	@IsString()
	name: string;

	@IsOptional()
	@IsString()
	issuer?: string;

	@IsString()
	displayName: string;

	@IsString()
	clientId: string;

	@IsString()
	clientSecret: string;

	@IsString()
	scope: string;

	@IsOptional()
	@IsString()
	accessType?: string;

	@IsOptional()
	@IsString()
	wellknownUrl?: string;

	@IsString()
	authUrl: string;

	@IsString()
	tokenUrl: string;

	@IsString()
	callbackUrl: string;

	@IsOptional()
	@IsString()
	logoutUrl?: string;

	@IsOptional()
	@IsString()
	jwks_uri?: string;

	@IsOptional()
	@IsString()
	mappingkey?: string;

	@IsString()
	userinfoUrl: string;

	@IsString()
	defaultRole: string;

	@IsOptional()
	@IsBoolean()
	recoverDeletedUsers?: boolean;

	@Type(() => IGroupMapping)
	@IsOptional()
	@ValidateNested()
	groupMapping?: IGroupMapping;

	@Type(() => ClientMapping)
	@IsOptional()
	@ValidateNested({ each: true })
	mapping?: ClientMapping[];

	@IsOptional()
	@IsString()
	emailExtention?: string;

	@IsOptional()
	@IsBoolean()
	debug?: boolean;

	@IsOptional()
	@IsString()
	prompt?: string;

	@IsOptional()
	@IsString()
	useState?: boolean;
}

export class OAuthSettings {
	@Type(() => IOauthClientSettings)
	@IsOptional()
	@ValidateNested()
	client?: IOauthClientSettings[];

	@IsOptional()
	@IsString()
	loginLabel?: string;

	@IsOptional()
	@IsBoolean()
	oauthOnly?: boolean;
}
