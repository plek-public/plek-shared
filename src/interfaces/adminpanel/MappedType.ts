/**
 * Type with generics as workaround to handle mapped types in typescript-rtti.
 *
 * Instead of:
 * { [id: number]: Example }
 *
 * You can write:
 * MappedType<'id', number, Example>
 *
 * Both are the same in typescript but the latter exposes the key label, key type and value type through the generics.
 */
export type MappedType<KeyLabel extends string, KeyType extends string | number | symbol, ValueType> = {
	[key in KeyType]: ValueType;
};
