export interface ICheckboxProps {
	label?: string;
	value?: boolean;
}

export const Checkbox =
	(props?: ICheckboxProps): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('checkbox', props, target, propertyKey);
	};
