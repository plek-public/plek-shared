export interface ISwitchProps {
	value?: boolean; // default should be set to 'false'
	label?: string;
}

export const Switch =
	(props?: ISwitchProps): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('switch', props, target, propertyKey);
	};
