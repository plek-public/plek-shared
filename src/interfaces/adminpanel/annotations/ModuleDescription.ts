export const ModuleDescription =
	(value: string): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('moduleDescription', value, target, propertyKey);
	};
