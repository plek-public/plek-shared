import { IInputProps } from './Input';

export const Textarea =
	(props?: IInputProps): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('textarea', props, target, propertyKey);
	};
