export interface IInputProps {
	label?: string;
	placeholder?: string;
}

export const Input =
	(props?: IInputProps): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('input', props, target, propertyKey);
	};
