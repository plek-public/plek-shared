export const HelpText =
	(props?: string): PropertyDecorator =>
	(target, propertyKey) => {
		Reflect.defineMetadata('help', props, target, propertyKey);
	};
