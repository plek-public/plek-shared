export enum PixabayTypeEnum {
	PHOTO = 'photo',
	ILLUSTRATION = 'illustration',
	VECTOR = 'vector',
}
