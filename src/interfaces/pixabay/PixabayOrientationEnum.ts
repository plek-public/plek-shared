export enum PixabayOrientationEnum {
	HORIZONTAL = 'horizontal',
	VERTICAL = 'vertical',
}
