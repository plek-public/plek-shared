import { IPixabayResult } from './IPixabayResult';

export type IPixabayResults = {
	total: number;
	totalHits: number;
	hits: Array<IPixabayResult>;
};
