import { PixabayOrderEnum } from './PixabayOrderEnum';
import { PixabayCategoryEnum } from './PixabayCategoryEnum';
import { PixabayColorEnum } from './PixabayColorEnum';
import { PixabayTypeEnum } from './PixabayTypeEnum';
import { PixabayOrientationEnum } from './PixabayOrientationEnum';

export type IPixabayParams = {
	key?: string;
	q?: string;
	page?: number;
	id?: number;
	per_page?: number;
	min_width?: number;
	min_height?: number;
	editors_choice?: boolean;
	safesearch?: boolean;
	order?: PixabayOrderEnum;
	colors?: PixabayColorEnum;
	category?: PixabayCategoryEnum;
	image_type?: PixabayTypeEnum;
	orientation?: PixabayOrientationEnum;
};
