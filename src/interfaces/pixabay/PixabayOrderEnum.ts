export enum PixabayOrderEnum {
	LATEST = 'latest',
	POPULAR = 'popular',
}
