import { PixabayTypeEnum } from './PixabayTypeEnum';

export type IPixabayResult = {
	id: number;
	type: PixabayTypeEnum;
	tags: string; // seperated by ', ';
	previewURL: string;
	previewWidth: number;
	previewHeight: number;
	webformatURL: string;
	webformatWidth: number;
	webformatHeight: number;
	largeImageURL: string;
	fullHDURL: string;
	imageURL: string;
	imageWidth: number;
	imageHeight: number;
	imageSize: number;
	views: number;
	downloads: number;
	likes: number;
	comments: number;
	user_id: string;
	user: string;
	userImageURL: string;
};
