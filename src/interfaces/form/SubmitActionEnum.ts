export enum SubmitActionEnum {
	SHOW_THANK_YOU_MESSAGE = 'showThankYoumessage',
	SHOW_SLIDE_WITH_RESULTS = 'showSlideWithResults',
	SHOW_DATA_GRAPHS_TO_USER = 'showDataGraphsToUser',
	SHOW_QUIZ_RESULTS = 'showQuizResults',
}
