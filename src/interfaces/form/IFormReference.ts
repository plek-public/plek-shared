import { FormReferenceTypeEnum } from './FormReferenceTypeEnum';

export interface IFormReference {
	_id: string;
	type?: FormReferenceTypeEnum;
	label?: string;
	condition?: string;
}
