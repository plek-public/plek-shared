import { IFormReference } from './IFormReference';
import { IFollowUpTo } from './IFollowUpTo';
import { IFormFieldOptionCategory } from './IFormFieldOptionCategory';
import { IMedia } from '../media/IMedia';

export interface IFormFieldOption {
	id: string;
	label: string;
	category?: IFormFieldOptionCategory;
	skipTo?: IFormReference;
	followUpTo?: IFollowUpTo;
	attachments?: Array<IMedia>;
	condition?: string;
}
