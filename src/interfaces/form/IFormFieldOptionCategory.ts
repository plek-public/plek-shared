export interface IFormFieldOptionCategory {
	id: string;
	label: string;
}
