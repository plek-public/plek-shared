export enum FormReferenceTypeEnum {
	SECTION = 'SECTION',
	FIELD = 'FIELD',
}
