import { SourceTypeEnum } from './SourceTypeEnum';

export type IFormSourceDetails =
	| {
			type: SourceTypeEnum.ONBOARDING;
			onboarding: {
				id: string;
				stepId: string;
				name: string;
			};
	  }
	| {
			type: SourceTypeEnum.INFO_PAGE;
			contentPage: {
				id: string;
				title: string;
			};
	  }
	| {
			type: SourceTypeEnum.GROUP_MESSAGE;
			message: {
				id: string;
				subject: string;
			};
			group: {
				id: string;
				name: string;
			};
	  }
	| {
			type: SourceTypeEnum.CHANNEL_MESSAGE;
			message: {
				id: string;
				subject: string;
			};
			channel: {
				id: string;
				name: string;
			};
	  }
	| {
			type: SourceTypeEnum.AGENDA_MESSAGE;
			message: {
				id: string;
				subject: string;
			};
			agenda: {
				id: string;
				title: string;
			};
	  }
	| {
			type: SourceTypeEnum.AGENDA_EVENT;
			agenda: {
				id: string;
				title: string;
			};
	  };
