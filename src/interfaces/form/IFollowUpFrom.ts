import { IOptionAnswer } from './IFormFieldAnswerValue';

export interface IFollowUpFrom {
	fieldId: string;
	answer?: IOptionAnswer;
}
