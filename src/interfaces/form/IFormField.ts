import { FormFieldTypeEnum } from './FormFieldTypeEnum';
import { ValidationTypeEnum } from './ValidationTypeEnum';
import { IFormFieldOption } from './IFormFieldOption';
import { RatingTypeEnum } from './RatingTypeEnum';
import { MessageTypeEnum } from './MessageTypeEnum';

export interface IFormFieldGeneric<T extends FormFieldTypeEnum = FormFieldTypeEnum> {
	_id: string;
	_sectionId: string;
	type: T;
	mandatory: boolean;
	label: string;
	maxAttemptsAllowed?: number;
	helptext?: string;
	bubbles?: Array<{ label: string; condition?: string }>;
	condition?: string;
	prefillWith?: string;
	correctMessage?: string;
	incorrectMessage?: string;
}

interface IShareToGroup {
	isShareable: true;
}

interface IFormFieldText extends IFormFieldGeneric<FormFieldTypeEnum.TEXT> {
	maxLength?: number;
	shareToGroup?: IShareToGroup;
	/**
	 * @deprecated Required by backend, not used in frontend
	 */
	valueType: 'string';
}

interface IFormFieldTextarea extends IFormFieldGeneric<FormFieldTypeEnum.TEXTAREA> {
	shareToGroup?: IShareToGroup;
	/**
	 * @deprecated Required by backend, not used in frontend
	 */
	valueType: 'string';
}

interface IFormFieldCheckbox extends IFormFieldGeneric<FormFieldTypeEnum.CHECKBOX> {
	options: IFormFieldOption[];
	isOtherAnswerAllowed?: boolean;
	minOptionCount?: number;
	maxOptionCount?: number;
	correctOptions?: string[];
	/**
	 * @deprecated Required by backend, not used in frontend
	 */
	valueType: 'array';
}

interface IFormFieldRadiobox extends IFormFieldGeneric<FormFieldTypeEnum.RADIOBOX> {
	options: IFormFieldOption[];
	isOtherAnswerAllowed?: boolean;
	correctOptions?: string[];
	/**
	 * @deprecated Required by backend, not used in frontend
	 */
	valueType: 'array';
	/**
	 * @deprecated Required by backend, not used in frontend
	 */
	maxItem?: number;
}

interface IFormFieldAttachment extends IFormFieldGeneric<FormFieldTypeEnum.ATTACHMENT> {
	/**
	 * @deprecated Required by backend, not used in frontend
	 */
	valueType: 'attachment';
}

interface IFormFieldScale extends IFormFieldGeneric<FormFieldTypeEnum.SCALE> {
	minLabel: string;
	maxLabel: string;
	max: number;
	min: number;
	options?: IFormFieldOption[];
}

interface IFormFieldDate extends IFormFieldGeneric<FormFieldTypeEnum.DATE> {}

interface IFormFieldIntroduction extends IFormFieldGeneric<FormFieldTypeEnum.INTRODUCTION> {
	description: string;
	messageType?: MessageTypeEnum;
}

interface IFormFieldRating extends IFormFieldGeneric<FormFieldTypeEnum.RATING> {
	ratingType: RatingTypeEnum;
	ratingAmount: number;
	options?: IFormFieldOption[];
}

type IFormFieldNumber = IFormFieldGeneric<FormFieldTypeEnum.NUMBER> &
	(
		| {
				validationType: ValidationTypeEnum.WHOLE_NUMBER_BETWEEN;
				min: string;
				max: string;
		  }
		| {
				validationType: Exclude<ValidationTypeEnum, ValidationTypeEnum.WHOLE_NUMBER_BETWEEN>;
		  }
	);

export type IFormField<T extends FormFieldTypeEnum = FormFieldTypeEnum> = T extends FormFieldTypeEnum.TEXT
	? IFormFieldText
	: T extends FormFieldTypeEnum.TEXTAREA
		? IFormFieldTextarea
		: T extends FormFieldTypeEnum.CHECKBOX
			? IFormFieldCheckbox
			: T extends FormFieldTypeEnum.RADIOBOX
				? IFormFieldRadiobox
				: T extends FormFieldTypeEnum.ATTACHMENT
					? IFormFieldAttachment
					: T extends FormFieldTypeEnum.SCALE
						? IFormFieldScale
						: T extends FormFieldTypeEnum.DATE
							? IFormFieldDate
							: T extends FormFieldTypeEnum.INTRODUCTION
								? IFormFieldIntroduction
								: T extends FormFieldTypeEnum.RATING
									? IFormFieldRating
									: T extends FormFieldTypeEnum.NUMBER
										? IFormFieldNumber
										:
												| IFormFieldText
												| IFormFieldTextarea
												| IFormFieldCheckbox
												| IFormFieldRadiobox
												| IFormFieldAttachment
												| IFormFieldScale
												| IFormFieldDate
												| IFormFieldIntroduction
												| IFormFieldRating
												| IFormFieldNumber;
