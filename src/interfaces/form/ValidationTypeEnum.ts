export enum ValidationTypeEnum {
	WHOLE_NUMBER = 'WHOLE_NUMBER',
	DECIMAL_NUMBER = 'DECIMAL_NUMBER',
	WHOLE_NUMBER_BETWEEN = 'WHOLE_NUMBER_BETWEEN',
}

export const validation = {
	[ValidationTypeEnum.WHOLE_NUMBER]: {
		type: ValidationTypeEnum.WHOLE_NUMBER,
		label: 'WHOLE_NUMBER',
	},
	[ValidationTypeEnum.DECIMAL_NUMBER]: {
		type: ValidationTypeEnum.DECIMAL_NUMBER,
		label: 'DECIMAL_NUMBER',
	},
	[ValidationTypeEnum.WHOLE_NUMBER_BETWEEN]: {
		type: ValidationTypeEnum.WHOLE_NUMBER_BETWEEN,
		label: 'WHOLE_NUMBER_BETWEEN',
	},
};
