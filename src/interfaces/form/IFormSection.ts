import { IFormReference } from './IFormReference';

export interface IFormSection {
	_id: string;
	title: string;
	// Backend sometimes returns null instead of undefined...
	skipTo?: IFormReference | IFormReference[] | null;
	isFollowUp?: boolean;
}
