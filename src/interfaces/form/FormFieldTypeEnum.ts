export enum FormFieldTypeEnum {
	TEXT = 'text',
	TEXTAREA = 'textarea',
	CHECKBOX = 'checkbox',
	RADIOBOX = 'radiobox',
	ATTACHMENT = 'attachment',
	SCALE = 'scale',
	DATE = 'date',
	INTRODUCTION = 'introduction',
	RATING = 'rating',
	NUMBER = 'number',
}
