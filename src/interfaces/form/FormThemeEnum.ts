/**
 * describe when a new value added to the enum, please update the FormController SWAGGER decorator and Form.ts Schema as well.
 */
export enum FormThemeEnum {
	SENTENSOR = 'sentensor',
}
