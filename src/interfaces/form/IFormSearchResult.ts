import { IForm } from './IForm';
import { IFormSourceDetails } from './IFormSourceDetails';

export interface IFormSearchResult extends Pick<IForm, 'id' | 'title' | 'user' | 'createdAt' | 'isArchived'> {
	source?: IFormSourceDetails;
}

export type IFormSearchResults = Array<IFormSearchResult[]>;
