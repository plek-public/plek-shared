import { FormErrorTypeEnum } from './FormErrorTypeEnum';

export type IFormError = {
	fieldId?: string;
	sectionId?: string;
} & (
	| {
			type: FormErrorTypeEnum.SECTION_TITLE_REQUIRED;
			fieldId?: never;
			sectionId: string;
	  }
	| {
			type:
				| FormErrorTypeEnum.FIELD_LABEL_REQUIRED
				| FormErrorTypeEnum.FIELD_DESCRIPTION_REQUIRED
				| FormErrorTypeEnum.MAX_OPTION_COUNT_MUST_BE_LARGER_THAN_2
				| FormErrorTypeEnum.CORRECT_OPTIONS_REQUIRED
				| FormErrorTypeEnum.NUMBER_MIN_REQUIRED
				| FormErrorTypeEnum.NUMBER_MAX_REQUIRED
				| FormErrorTypeEnum.NUMBER_MAX_SHOULD_BE_LARGER_THAN_MIN;
			fieldId: string;
			sectionId: string;
	  }
	| {
			type: FormErrorTypeEnum.OPTION_TITLE_REQUIRED | FormErrorTypeEnum.OPTION_IMAGE_REQUIRED;
			fieldId: string;
			sectionId: string;
			optionId: string;
	  }
	| {
			type: FormErrorTypeEnum.OPTION_CATEGORY_TITLE_REQUIRED;
			fieldId: string;
			sectionId: string;
			categoryId: string;
	  }
);
