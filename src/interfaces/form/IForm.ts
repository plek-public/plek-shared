import { IFormSection } from './IFormSection';
import { IFormField } from './IFormField';
import { FormStatusEnum } from './FormStatusEnum';
import { FormTypeEnum } from './FormTypeEnum';
import { IUserMicro } from '../user/IUserMicro';
import { SubmitActionEnum } from './SubmitActionEnum';
import { FormThemeEnum } from './FormThemeEnum';
import { IFormSource } from './IFormSource';
import { IFormFieldAnswer } from './IFormFieldAnswer';
import { ShowCorrectAnswersEnum } from './ShowCorrectAnswersEnum';

export interface IForm {
	id: string;
	type: FormTypeEnum;
	title: string;
	fields: IFormField[];
	sections: IFormSection[];
	shuffleOptions?: boolean;
	isAnonymous?: boolean;
	submitAction?: SubmitActionEnum;
	multipleEntriesAllowed?: boolean;
	isInsertingSubmittedAnswers?: boolean;
	usersToNotify?: IUserMicro[];
	confirmationMessage?: string;
	status?: FormStatusEnum;
	canEdit?: boolean;
	shareToGroup?: { groupIds: string[] };
	sharedTo?: string;
	user?: IUserMicro;
	label?: string;
	copyFromFormId?: string;
	source?: IFormSource;
	theme?: FormThemeEnum;
	vhost: string;
	variableMapping?: {
		[variableName: string]: Pick<IFormFieldAnswer, 'fieldId' | 'followUpFrom'>;
	};
	series?: {
		/**
		 * - formSeriesIdentifier (based on sentensor dialogId)
		 * - formSeriesNr (based on cycleId or just check last form with same Identifier)
		 */
		dialogId?: number;
		cycleId?: number;
		id: string;
		index: number;
	};
	createdAt: string;
	isArchived?: boolean;
	isDeleted?: boolean;
	showCorrectAnswers?: ShowCorrectAnswersEnum;
}
