export enum RatingTypeEnum {
	STARS = 'STARS',
	SMILEYS = 'SMILEYS',
}

export const rating = {
	[RatingTypeEnum.STARS]: {
		type: RatingTypeEnum.STARS,
		icon: 'star',
		name: 'STARS',
	},
	[RatingTypeEnum.SMILEYS]: {
		type: RatingTypeEnum.SMILEYS,
		icon: 'smiley',
		name: 'SMILEYS',
	},
};
