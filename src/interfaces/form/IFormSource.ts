import { SourceTypeEnum } from './SourceTypeEnum';

export type IFormSource =
	| {
			type: SourceTypeEnum.ONBOARDING;
			onBoardingId: string;
			stepId: string;
	  }
	| {
			type: SourceTypeEnum.INFO_PAGE;
			contentPageId: string;
	  }
	| {
			type: SourceTypeEnum.GROUP_MESSAGE;
			messageId: string;
			groupId: string;
	  }
	| {
			type: SourceTypeEnum.CHANNEL_MESSAGE;
			messageId: string;
			channelId: string;
	  }
	| {
			type: SourceTypeEnum.AGENDA_MESSAGE;
			messageId: string;
			agendaId: string;
	  }
	| {
			type: SourceTypeEnum.AGENDA_EVENT;
			agendaId: string;
	  }
	| {
			type: SourceTypeEnum.WIDGET;
	  };
