import { FormFieldTypeEnum } from './FormFieldTypeEnum';
import { RatingValue } from './RatingValue';
import { IMedia, IMediaAudio, IMediaImage, IMediaVideo } from '../media/IMedia';

export interface IOptionAnswer {
	id: string;
	label: string;
}

export type IFormFieldAnswerAttachments = {
	attachments: {
		images?: IMediaImage[];
		video?: IMediaVideo[];
		audio?: IMediaAudio[];
		documents?: IMedia[];
	};
};

/**
 * @deprecated Use FormFieldAnswerValueTypeMap instead
 */
export type IFormFieldAnswerValue<T extends FormFieldTypeEnum = FormFieldTypeEnum> = FormFieldAnswerValueTypeMap[T];

export type FormFieldAnswerValueTypeMap = {
	[FormFieldTypeEnum.INTRODUCTION]: true | undefined;
	[FormFieldTypeEnum.TEXTAREA]: string | undefined;
	[FormFieldTypeEnum.SCALE]: string | undefined;
	[FormFieldTypeEnum.DATE]: string | undefined;
	[FormFieldTypeEnum.NUMBER]: string | undefined;
	[FormFieldTypeEnum.TEXT]: string | undefined;
	[FormFieldTypeEnum.RATING]: RatingValue | undefined;
	[FormFieldTypeEnum.CHECKBOX]: IOptionAnswer[] | undefined;
	[FormFieldTypeEnum.RADIOBOX]: IOptionAnswer[] | undefined;
	[FormFieldTypeEnum.ATTACHMENT]: IFormFieldAnswerAttachments | undefined;
};

/**
 * This only exists to make sure FormFieldAnswerValueTypeMap has all the keys of FormFieldTypeEnum. Do not use it.
 */
const typeCheck: Record<FormFieldTypeEnum, unknown> = {} as FormFieldAnswerValueTypeMap;
