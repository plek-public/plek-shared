import { IFormField } from './IFormField';
import { FormStatusEnum } from './FormStatusEnum';
import { IFormFieldAnswer } from './IFormFieldAnswer';

export interface IFormSequence {
	items: Array<{
		id: string;
		field: IFormField;
		answer: IFormFieldAnswer;
		title?: string;
		progress?: number;
		editable?: boolean;
	}>;
	current: number;
	isCompleted?: boolean;
	title: string;
	status: FormStatusEnum;
}
