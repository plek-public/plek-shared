import { IFormField } from './IFormField';
import { IUserMicro } from '../user/IUserMicro';
import { IFormFieldAnswer } from './IFormFieldAnswer';

export type FormResponse = {
	answer: IFormFieldAnswer;
	/** Optional since form could have been anonymous */
	user?: IUserMicro;
};

export type FormResultsQuestion = {
	/**
	 * Since the same fields can appear more than once in the results,
	 * we cannot rely on the field id to be unique. But the combination
	 * of field id and answer follow up from id results in a unique id.
	 *
	 * This id can be used to uniquely identify each returned questions
	 * within the results e.g. for the required keys in a React render.
	 */
	id: string;
	field: IFormField;
	/** All responses received for this question, this can differ per question due to e.g. skipping and routing */
	responses: FormResponse[];
};

export type FormResultsResponse = {
	/**
	 * All possible questions for a form with their responses,
	 * e.g. a multi select question with 5 options to choose
	 * from and 3 follow-up questions repeated for each option
	 * results in 16 questions that should be in the results.
	 */
	questions: FormResultsQuestion[];
	/**
	 * Total number of received responses, this number can
	 * differ from the number of responses in each question
	 * due to e.g. skipping and routing based on answers.
	 */
	receivedResponses: number;
};
