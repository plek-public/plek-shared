export type IAnswerSetting = {
	message: string;
	skipTo?: any;
	score: number;
};
