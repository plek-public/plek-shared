export enum FormTypeEnum {
	ADVANCED = 'advanced',
	QUICK = 'quick',
	POLL = 'poll',
	QUIZ = 'quiz',
}
