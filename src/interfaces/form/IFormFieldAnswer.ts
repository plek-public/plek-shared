import { FormFieldTypeEnum } from './FormFieldTypeEnum';
import { FormFieldAnswerValueTypeMap } from './IFormFieldAnswerValue';
import { IFollowUpFrom } from './IFollowUpFrom';

export interface IFormFieldAnswer<T extends FormFieldTypeEnum = FormFieldTypeEnum> {
	fieldId: string;
	answer: FormFieldAnswerValueTypeMap[T];
	followUpFrom?: IFollowUpFrom;
}
