/**
 * Quartile is a mathematical description of
 * a dataset focusing on the distribution of
 * the data.
 */
export type Quartile = {
	min: number;
	max: number;
	q1: number;
	q2: number;
	q3: number;
	/**
	 * Normalized value to represent the
	 * minimum while removing outliers. The
	 * value is calculated as `Q(1) - 1.5x(Q(3) - Q(1))`
	 */
	lower: number;
	/**
	 * Normalized value to represent the
	 * maximum while removing outliers. The
	 * value is calculated as `Q(3) + 1.5x(Q(3) - Q(1))`
	 */
	upper: number;
};

export type QuartileRank = 0 | 1 | 2 | 3 | 4;
