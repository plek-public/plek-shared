// TODO: Add colors for dark theme
export const filenameToColor = (filename: string, isDark?: boolean): string | undefined => {
	switch (/\.[^.]+$/.exec(filename.toLowerCase())?.[0]) {
		case '.pdf':
			return '#b50707';
		case '.doc':
		case '.docx':
		case '.jpg':
		case '.png':
		case '.gif':
		case '.jpeg':
			return '#233d69';
		case '.ppt':
		case '.pptx':
		case '.psd':
		case '.ai':
		case '.idd':
		case '.indd':
			return '#d14424';
		case '.xls':
		case '.xlsx':
			return '#2b7152';
		case '.mp4':
		case '.avi':
		case '.mov':
		case '.wav':
		case '.wma':
		case '.mp3':
			return '#b95285';
		case '.txt':
		case '.html':
		case '.css':
		case '.php':
		case '.js':
		case '.scss':
		case '.json':
			return '#808080';
		case '.zip':
			return '#eaa805';
	}
};

export const filenameToIcon = (filename: string, asUnicode?: boolean): string => {
	switch (/\.[^.]+$/.exec(filename.toLowerCase())?.[0]) {
		case '.pdf':
			return asUnicode ? '\uf138' : 'document-pdf';
		case '.doc':
		case '.docx':
			return asUnicode ? '\uf13b' : 'document-word';
		case '.jpg':
		case '.png':
		case '.gif':
		case '.jpeg':
			return asUnicode ? '\uf168' : 'photo';
		case '.ppt':
		case '.pptx':
			return asUnicode ? '\uf139' : 'document-powerpoint';
		case '.psd':
		case '.ai':
		case '.idd':
		case '.indd':
			return asUnicode ? '\uf174' : 'project';
		case '.xls':
		case '.xlsx':
			return asUnicode ? '\uf137' : 'document-excel';
		case '.mp4':
		case '.avi':
		case '.mov':
			return asUnicode ? '\uf164' : 'online-video';
		case '.wav':
		case '.wma':
		case '.mp3':
			return asUnicode ? '\uf136' : 'document-audio';
		case '.txt':
			return asUnicode ? '\uf135' : 'document';
		case '.html':
		case '.css':
		case '.php':
		case '.js':
		case '.scss':
		case '.json':
			return asUnicode ? '\uf134' : 'development';
		case '.zip':
			return asUnicode ? '\uf146' : 'folder';
		default:
			return asUnicode ? '\uf12d' : 'copy';
	}
};
