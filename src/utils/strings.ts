/**
 * Replaces text in a string asynchronously, using a regular expression or search string.
 * @param value The source string.
 * @param searchValue A string to search for.
 * @param replacer An asynchronous function that returns the replacement text.
 */
export const replaceAsync = async (value: string, searchValue: string | RegExp, replacer: (substring: string, ...args: any[]) => Promise<string>) => {
	// 1. Run pass of `replace`, collect promises from `replacer` calls
	// 2. Resolve them with `Promise.all`
	// 3. Run `replace` with values from resolved promises
	const promises = [] as any[];
	value.replace(searchValue, (...args) => {
		promises.push(replacer(...args));
		return '';
	});
	const values = await Promise.all(promises);
	return value.replace(searchValue, () => values.shift());
};
