export const groupBy = (array: any[], predicate: Function) =>
	array?.reduce((newObject, item, index) => {
		const key = predicate(item, index, array);
		if (!(key in newObject)) {
			newObject[key] = {};
		}
		newObject[key].push(item);
		return newObject;
	}, {});

export const groupAndSubgroupBy = (array: any[], predicate: Function, subPredicate: Function) =>
	array?.reduce((newObject, item, index) => {
		const key = predicate(item, index, array);
		const subKey = subPredicate(item);
		if (!(key in newObject)) {
			newObject[key] = {};
		}
		if (!(subKey in newObject[key])) {
			newObject[key][subKey] = [];
		}
		newObject[key][subKey].push(item);
		return newObject;
	}, {});
