/**
 * Find a value in an array with an asynchronous predicate
 * @param array
 * @param predicate
 */
export const findAsync = async <T>(array: T[], predicate: (t: T) => Promise<boolean>): Promise<T | undefined> => {
	for (const item of array) {
		if (await predicate(item)) {
			return item;
		}
	}
	return undefined;
};
