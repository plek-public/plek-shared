import { FormFieldTypeEnum } from '../interfaces/form/FormFieldTypeEnum';
import { FileTypeEnum } from '../interfaces/media/FileTypeEnum';
import { IMedia } from '../interfaces/media/IMedia';
import { IFormField, IFormFieldGeneric } from '../interfaces/form/IFormField';
import { IForm } from '../interfaces/form/IForm';
import { IFormReference } from '../interfaces/form/IFormReference';
import { FormReferenceTypeEnum } from '../interfaces/form/FormReferenceTypeEnum';
import { ValidationTypeEnum } from '../interfaces/form/ValidationTypeEnum';
import { FormFieldAnswerValueTypeMap } from '../interfaces/form/IFormFieldAnswerValue';
import { IFormFieldAnswer } from '../interfaces/form/IFormFieldAnswer';
import { IFormSection } from '../interfaces/form/IFormSection';
import { RatingTypeEnum } from '../interfaces/form/RatingTypeEnum';
import { IFormFieldOption } from '../interfaces/form/IFormFieldOption';
import { IFormFieldOptionCategory } from '../interfaces/form/IFormFieldOptionCategory';
import { IFollowUpTo } from '../interfaces/form/IFollowUpTo';
import { IFormError } from '../interfaces/form/IFormError';
import { FormErrorTypeEnum } from '../interfaces/form/FormErrorTypeEnum';
import { FormTypeEnum } from '../interfaces/form/FormTypeEnum';
import { FormStatusEnum } from '../interfaces/form/FormStatusEnum';
import { SourceTypeEnum } from '../interfaces/form/SourceTypeEnum';
import { IFormSource } from '../interfaces/form/IFormSource';
import { FormThemeEnum } from '../interfaces/form/FormThemeEnum';
import { SubmitActionEnum } from '../interfaces/form/SubmitActionEnum';
import { IFollowUpFrom } from '../interfaces/form/IFollowUpFrom';
import { isMedia } from './media';
import { v4 as uuid } from 'uuid';
import { intersectionBy, isNaN, uniq, uniqBy, xor } from 'lodash';
import { format, isValid } from 'date-fns';
import { ShowCorrectAnswersEnum } from '../interfaces/form/ShowCorrectAnswersEnum';

// Below code should not be platform (frontend, backend, or mobile) specific

/**
 * Parse form field answer, the answer value is not parsed and returned 'as is',
 * this should always be parsed with `parseFormFieldAnswerValue` afterwards!
 * @param value The form field answer that should be parsed
 */
export const parseFormFieldAnswer = (value: any): IFormFieldAnswer | undefined => {
	if (!value || typeof value !== 'object' || typeof value.fieldId !== 'string') {
		return undefined;
	}
	const answer: IFormFieldAnswer = {
		fieldId: value.fieldId,
		answer: value.answer,
	};
	const followUpFrom = parseFollowUpFrom(value.followUpFrom);
	if (followUpFrom) {
		answer.followUpFrom = followUpFrom;
	}
	return answer;
};

/**
 * Parse follow up from
 * @param value The follow up from that should be parsed
 */
export const parseFollowUpFrom = (value: any): IFollowUpFrom | undefined => {
	if (!value || typeof value !== 'object' || typeof value.fieldId !== 'string') {
		return undefined;
	}
	const followUpFrom: IFollowUpFrom = {
		fieldId: value.fieldId,
	};
	if (value.answer && typeof value.answer === 'object' && typeof value.answer.id === 'string' && typeof value.answer.label === 'string') {
		followUpFrom.answer = {
			id: value.answer.id,
			label: value.answer.label,
		};
	}
	return followUpFrom;
};

/**
 * Parse form field answer value
 * @param field The form field that is answered
 * @param value The answer value that should be parsed
 */
export const parseFormFieldAnswerValue = <T extends FormFieldTypeEnum>(field: IFormField<T>, value: any): FormFieldAnswerValueTypeMap[T] => {
	switch (field.type) {
		case FormFieldTypeEnum.INTRODUCTION: {
			return true as FormFieldAnswerValueTypeMap[T];
		}
		case FormFieldTypeEnum.TEXT: {
			return (
				typeof value === 'string' && (!field.maxLength || value.length <= field.maxLength) ? value : undefined
			) as FormFieldAnswerValueTypeMap[T];
		}
		case FormFieldTypeEnum.TEXTAREA: {
			return (typeof value === 'string' ? value : undefined) as FormFieldAnswerValueTypeMap[T];
		}
		case FormFieldTypeEnum.SCALE: {
			if (typeof value !== 'string') {
				return undefined as FormFieldAnswerValueTypeMap[T];
			}
			const number = parseInt(value, 10);
			if (isNaN(number)) {
				return undefined as FormFieldAnswerValueTypeMap[T];
			}
			if (number > field.max || number < field.min) {
				return undefined as FormFieldAnswerValueTypeMap[T];
			}
			return `${number}` as FormFieldAnswerValueTypeMap[T];
		}
		case FormFieldTypeEnum.DATE: {
			return (
				typeof value === 'string' && value && isValid(new Date(value)) ? new Date(value).toISOString() : undefined
			) as FormFieldAnswerValueTypeMap[T];
		}
		case FormFieldTypeEnum.CHECKBOX: {
			const sortedValue = intersectionBy(
				field.options,
				Array.isArray(value) ? value.filter((v) => typeof v === 'object' && typeof v.id === 'string' && typeof v.label === 'string') : [],
				'id'
			);
			return (sortedValue.length ? sortedValue : undefined) as FormFieldAnswerValueTypeMap[T];
		}
		case FormFieldTypeEnum.RADIOBOX: {
			const sortedValue = intersectionBy(
				field.options,
				Array.isArray(value) ? value.filter((v) => typeof v === 'object' && typeof v.id === 'string' && typeof v.label === 'string') : [],
				'id'
			);
			return (sortedValue.length === 1 ? sortedValue : undefined) as FormFieldAnswerValueTypeMap[T];
		}
		case FormFieldTypeEnum.ATTACHMENT: {
			if (!value || typeof value !== 'object' || typeof value.attachments !== 'object') {
				return undefined as FormFieldAnswerValueTypeMap[T];
			}
			const images = Array.isArray(value.attachments.images)
				? value.attachments.images.filter(isMedia).filter(({ fileType }: any) => fileType === FileTypeEnum.image)
				: [];
			const video = Array.isArray(value.attachments.video)
				? value.attachments.video.filter(isMedia).filter(({ fileType }: any) => fileType === FileTypeEnum.video)
				: [];
			const audio = Array.isArray(value.attachments.audio)
				? value.attachments.audio.filter(isMedia).filter(({ fileType }: any) => fileType === FileTypeEnum.audio)
				: [];
			const documents = Array.isArray(value.attachments.documents) ? value.attachments.documents.filter(isMedia) : [];
			const media = images?.[0] ?? video?.[0] ?? audio?.[0] ?? documents?.[0];
			if (!media) {
				return undefined as FormFieldAnswerValueTypeMap[T];
			}
			return mediaToFormFieldAnswer(media) as FormFieldAnswerValueTypeMap[T];
		}
		case FormFieldTypeEnum.RATING: {
			return (typeof value !== 'number' || value > 5 || value < 1 ? undefined : value) as FormFieldAnswerValueTypeMap[T];
		}
		case FormFieldTypeEnum.NUMBER: {
			if (typeof value !== 'string') {
				return undefined as FormFieldAnswerValueTypeMap[T];
			}
			const number = field.validationType === ValidationTypeEnum.DECIMAL_NUMBER ? parseFloat(value) : parseInt(value, 10);
			if (
				isNaN(number) ||
				(field.validationType === ValidationTypeEnum.WHOLE_NUMBER_BETWEEN && (number < parseInt(field.min, 10) || number > parseInt(field.max, 10)))
			) {
				return undefined as FormFieldAnswerValueTypeMap[T];
			}
			return `${number}` as FormFieldAnswerValueTypeMap[T];
		}
		default:
			return undefined as FormFieldAnswerValueTypeMap[T];
	}
};

/**
 * Check if form field answer values are "answered", it's used to check if a field is in filled in or empty state, this is used to decide when to show the skip button and in combination with the mandatory option of the field.
 */
export const isFormFieldAnswered = <T extends FormFieldTypeEnum>(field: IFormField<T>, answer: FormFieldAnswerValueTypeMap[T]): boolean => {
	switch (field.type) {
		case FormFieldTypeEnum.INTRODUCTION:
		case FormFieldTypeEnum.SCALE:
		case FormFieldTypeEnum.DATE:
		case FormFieldTypeEnum.RATING:
			return answer !== undefined;
		case FormFieldTypeEnum.CHECKBOX:
		case FormFieldTypeEnum.RADIOBOX:
			const optionsAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX];
			return (optionsAnswer?.length ?? 0) > 0;
		case FormFieldTypeEnum.TEXT:
		case FormFieldTypeEnum.TEXTAREA:
			const textAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.TEXT | FormFieldTypeEnum.TEXTAREA];
			return (textAnswer?.length ?? 0) > 0;
		case FormFieldTypeEnum.ATTACHMENT:
			const attachmentAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.ATTACHMENT];
			const media = formFieldAnswerToMedia(attachmentAnswer);
			return media !== undefined;
		case FormFieldTypeEnum.NUMBER:
			const numberAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.NUMBER];
			return ![undefined, '', '-'].includes(numberAnswer);
		default:
			return false;
	}
};

/**
 * Check if form field answer values are valid, contrary to the above its only used during editing of answersand not submission to or from backend. If an answer is invalid, the UI will be able to show an error (optional)and block navigation to the next field until the answer is corrected by the user.
 */
export const isFormFieldAnswerValid = <T extends FormFieldTypeEnum>(field: IFormField<T>, answer: FormFieldAnswerValueTypeMap[T]): boolean => {
	switch (field.type) {
		case FormFieldTypeEnum.TEXT:
			const textAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.TEXT];
			return field.maxLength ? (textAnswer ?? '').length <= field.maxLength : true;
		case FormFieldTypeEnum.NUMBER:
			const numberAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.NUMBER];
			// Empty answers are considered valid
			if ([undefined, ''].includes(numberAnswer)) {
				return true;
			}
			const number = field.validationType === ValidationTypeEnum.DECIMAL_NUMBER ? parseFloat(numberAnswer ?? '') : parseInt(numberAnswer ?? '', 10);
			return !(
				isNaN(number) ||
				(field.validationType === ValidationTypeEnum.WHOLE_NUMBER_BETWEEN && (number < parseInt(field.min, 10) || number > parseInt(field.max, 10)))
			);
		case FormFieldTypeEnum.ATTACHMENT:
			const attachmentAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.ATTACHMENT];
			const media = formFieldAnswerToMedia(attachmentAnswer);
			// Temp attachments (file is still uploading) aren't valid
			return media === undefined || !media.filelink.startsWith('blob:');
		case FormFieldTypeEnum.CHECKBOX:
			const checkboxAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.CHECKBOX];
			return (
				checkboxAnswer === undefined ||
				!(
					(field.minOptionCount && checkboxAnswer.length < field.minOptionCount) ||
					(field.maxOptionCount && checkboxAnswer.length > field.maxOptionCount)
				)
			);
		default:
			return true;
	}
};

export const mediaToFormFieldAnswer = (media: IMedia): FormFieldAnswerValueTypeMap[FormFieldTypeEnum.ATTACHMENT] => {
	let answer: FormFieldAnswerValueTypeMap[FormFieldTypeEnum.ATTACHMENT];
	switch (media.fileType) {
		case FileTypeEnum.image:
			answer = { attachments: { images: [media] } };
			break;
		case FileTypeEnum.video:
			answer = { attachments: { video: [media] } };
			break;
		case FileTypeEnum.audio:
			answer = { attachments: { audio: [media] } };
			break;
		default:
			answer = { attachments: { documents: [media] } };
	}
	return answer;
};

export const formFieldAnswerToMedia = (answer: FormFieldAnswerValueTypeMap[FormFieldTypeEnum.ATTACHMENT]) =>
	answer?.attachments.images?.[0] ?? answer?.attachments.video?.[0] ?? answer?.attachments.audio?.[0] ?? answer?.attachments.documents?.[0];

export const getFormFieldIdFromSkipTo = (form: IForm, skipTo: IFormReference) => {
	switch (skipTo?.type) {
		case FormReferenceTypeEnum.FIELD:
			if (skipTo?._id) {
				return skipTo?._id;
			}
			break;
		default:
		case FormReferenceTypeEnum.SECTION:
			const skipToSectionId = skipTo?._id;
			const firstFieldIdInSection = skipToSectionId && form?.fields.find(({ _sectionId }) => _sectionId === skipToSectionId)?._id;
			if (firstFieldIdInSection) {
				return firstFieldIdInSection;
			}
			break;
	}
};

export const createAnswerForFormField = (field: IFormField): IFormFieldAnswer => ({
	fieldId: field._id,
	answer: parseFormFieldAnswerValue(field, undefined),
});

/**
 * Same "thing" since answers to the same field could be different follow up answers, but we want those to be the same too, only the actual answer value itself is allowed to differ between answers.
 */
export const isAnswerToSameThing = (a: Pick<IFormFieldAnswer, 'fieldId' | 'followUpFrom'>, b: Pick<IFormFieldAnswer, 'fieldId' | 'followUpFrom'>) =>
	a.fieldId === b.fieldId && a.followUpFrom?.fieldId === b.followUpFrom?.fieldId && a.followUpFrom?.answer?.id === b.followUpFrom?.answer?.id;

export const getOrderedFormFieldAnswers = (form: IForm) => {
	let orderedAnswers: IFormFieldAnswer[] = [];
	for (const field of form.fields) {
		const section = form.sections.find(({ _id }) => _id === field._sectionId);
		if (section?.isFollowUp) continue;

		// include the field in the result array
		orderedAnswers.push(createAnswerForFormField(field));

		switch (field.type) {
			case FormFieldTypeEnum.CHECKBOX:
				const followUpToOptions = field.options.filter((o) => !!o.followUpTo?.sectionId);
				if (followUpToOptions.length === 0) break;

				// if a checkbox field has followUpTo options, include these in the result array too
				for (const option of followUpToOptions) {
					const followUpSectionFields = form.fields.filter(({ _sectionId }) => _sectionId === option.followUpTo?.sectionId);

					orderedAnswers.push(
						...followUpSectionFields.map((f) => ({
							...createAnswerForFormField(f),
							followUpFrom: { fieldId: field._id, answer: option },
						}))
					);
				}
		}
	}

	return orderedAnswers;
};

export const createNextFormFieldAnswer = (
	form: IForm,
	answers: IFormFieldAnswer[],
	answer?: IFormFieldAnswer,
	followUpToIsFilledIn?: boolean
): IFormFieldAnswer | undefined => {
	// Return first field in form if there are no answers yet
	if (!answers.length || !answer) {
		return createAnswerForFormField(form.fields[0]);
	}

	const fieldIndex = form?.fields.findIndex(({ _id }) => _id === answer.fieldId);
	if (fieldIndex === -1) {
		throw Error('Field not found for answer');
	}
	const currentField = form.fields[fieldIndex];

	// Handle answered field with follow up to
	if (
		!followUpToIsFilledIn &&
		isFormFieldAnswered(currentField, answer.answer) &&
		currentField.type === FormFieldTypeEnum.CHECKBOX &&
		currentField.options.find(({ followUpTo }) => !!followUpTo?.sectionId)
	) {
		const optionsAnswer = answer as IFormFieldAnswer<FormFieldTypeEnum.CHECKBOX>;
		const sortedAnswers = intersectionBy(currentField.options, optionsAnswer.answer ?? [], 'id').map(({ id, label }) => ({
			id,
			label,
		}));
		const optionForAnswer = currentField.options.find(({ id }) => id === sortedAnswers[0]?.id);
		const followUpToField =
			optionForAnswer?.followUpTo?.sectionId && form?.fields.find(({ _sectionId }) => _sectionId === optionForAnswer.followUpTo?.sectionId);
		if (followUpToField) {
			return {
				...createAnswerForFormField(followUpToField),
				followUpFrom: { fieldId: currentField._id, answer: sortedAnswers[0] },
			};
		}
	}

	// Handle answered scale or rating field with skip to
	if (
		isFormFieldAnswered(currentField, answer.answer) &&
		(currentField.type === FormFieldTypeEnum.RATING || currentField.type === FormFieldTypeEnum.SCALE)
	) {
		const optionsAnswer = answer as IFormFieldAnswer<FormFieldTypeEnum.RATING | FormFieldTypeEnum.SCALE>;
		const optionForAnswer = (currentField.options ?? []).find(({ label }) => parseInt(`${label}`, 10) === parseInt(`${optionsAnswer.answer}`, 10));
		const skipToFieldId = optionForAnswer?.skipTo && getFormFieldIdFromSkipTo(form, optionForAnswer?.skipTo);
		const skipToField = form.fields.find(({ _id }) => _id === skipToFieldId);
		if (skipToField) {
			return createAnswerForFormField(skipToField);
		}
	}

	// Handle answered radio field with skip to
	if (isFormFieldAnswered(currentField, answer.answer) && currentField.type === FormFieldTypeEnum.RADIOBOX) {
		const optionsAnswer = answer as IFormFieldAnswer<FormFieldTypeEnum.RADIOBOX>;
		const optionForAnswer = currentField.options.find(({ id }) => id === optionsAnswer.answer?.[0].id);
		const skipToFieldId = optionForAnswer?.skipTo && getFormFieldIdFromSkipTo(form, optionForAnswer.skipTo);
		const skipToField = form.fields.find(({ _id }) => _id === skipToFieldId);
		if (skipToField) {
			return createAnswerForFormField(skipToField);
		}
	}

	// Handle last field in section with skip to
	const section = form.sections.find(({ _id }) => _id === currentField._sectionId);
	if (
		section?.skipTo &&
		!Array.isArray(section.skipTo) &&
		form.fields.filter(({ _sectionId }) => _sectionId === section._id).slice(-1)[0]?._id === answer.fieldId
	) {
		const skipToFieldId = getFormFieldIdFromSkipTo(form, section.skipTo);
		const skipToField = form.fields.find(({ _id }) => _id === skipToFieldId);
		if (skipToField) {
			return createAnswerForFormField(skipToField);
		}
	}

	// Handle field in follow up section
	if (answer.followUpFrom?.fieldId) {
		const followUpFromField = form.fields.find(({ _id }) => _id === answer.followUpFrom?.fieldId);
		const followUpFromAnswer = followUpFromField && answers.find(({ fieldId }) => fieldId === followUpFromField._id);
		const fieldsInFollowUpSection = form.fields.filter(({ _sectionId }) => _sectionId === currentField._sectionId);
		if (!followUpFromAnswer || !followUpFromField || !fieldsInFollowUpSection?.length) {
			throw Error('Follow up from answer, field or fields in follow up section not found');
		}

		const isLastFieldInFollowUpSection = fieldsInFollowUpSection.slice(-1)[0]?._id === answer.fieldId;
		if (isLastFieldInFollowUpSection) {
			// Check if follow up section needs to be repeated
			if (
				answer.followUpFrom.answer &&
				isFormFieldAnswered(followUpFromField, followUpFromAnswer.answer) &&
				followUpFromField.type === FormFieldTypeEnum.CHECKBOX
			) {
				const followUpFromOptionsAnswer = followUpFromAnswer as IFormFieldAnswer<FormFieldTypeEnum.CHECKBOX>;
				const sortedAnswers = intersectionBy(followUpFromField.options, followUpFromOptionsAnswer.answer ?? [], 'id').map(({ id, label }) => ({
					id,
					label,
				}));
				const index = sortedAnswers.findIndex(({ id }) => id === answer.followUpFrom?.answer?.id);
				if (index === -1) {
					throw Error('Answer not found in follow up from field');
				}

				// Repeat follow up section for next response
				if (index + 1 < sortedAnswers.length) {
					return {
						...createAnswerForFormField(fieldsInFollowUpSection[0]),
						followUpFrom: {
							fieldId: answer.followUpFrom.fieldId,
							answer: sortedAnswers[index + 1],
						},
					};
				}
			}

			// Continue beyond follow up section
			return createNextFormFieldAnswer(form, answers, followUpFromAnswer, true);
		} else {
			// Continue in follow up section
			const nextFieldInFollowUpSection = fieldsInFollowUpSection[fieldsInFollowUpSection.findIndex(({ _id }) => _id === currentField._id) + 1];
			if (nextFieldInFollowUpSection) {
				return {
					...createAnswerForFormField(nextFieldInFollowUpSection),
					followUpFrom: answer.followUpFrom,
				};
			}
		}
	}

	// Else find next field that is not in a follow up section
	const nextField = form.fields.slice(fieldIndex + 1).find((field) => !form.sections.find(({ _id }) => _id === field._sectionId)?.isFollowUp);
	return nextField && createAnswerForFormField(nextField);
};

// Get progress (between 0 and 1)
export const getFormProgress = (form: IForm, answers: IFormFieldAnswer[], answer: IFormFieldAnswer): number => {
	if (answers.length === 1) {
		return 0;
	}

	// Get next answers
	const answersAndNextAnswers = [...answers];
	let currentAnswer = answersAndNextAnswers.pop();
	while (currentAnswer) {
		answersAndNextAnswers.push(currentAnswer);
		currentAnswer = createNextFormFieldAnswer(form, answersAndNextAnswers, currentAnswer, true);
	}

	const primaryAnswers = answersAndNextAnswers.filter((a) => !a.followUpFrom?.fieldId);
	const progressPerAnswer = 1 / primaryAnswers.length;
	let progress = primaryAnswers.findIndex((a) => a.fieldId === (answer.followUpFrom?.fieldId ?? answer.fieldId)) * progressPerAnswer;
	const followUpFromFormField = answer.followUpFrom?.fieldId && form.fields.find((f) => f._id === answer.followUpFrom?.fieldId);
	if (followUpFromFormField && followUpFromFormField.type === FormFieldTypeEnum.CHECKBOX) {
		const createdAnswer = createAnswerForFormField(followUpFromFormField);
		const answerToFollowUpFromFormField = answersAndNextAnswers.find((a) =>
			isAnswerToSameThing(a, createdAnswer)
		) as IFormFieldAnswer<FormFieldTypeEnum.CHECKBOX>;
		const sortedAnswers = intersectionBy(followUpFromFormField.options, answerToFollowUpFromFormField?.answer ?? [], 'id').map(({ id, label }) => ({
			id,
			label,
		}));
		if (sortedAnswers.length === 0) {
			return progress;
		}
		const fieldForAnswer = form.fields.find((f) => f._id === answer.fieldId);
		if (!fieldForAnswer) {
			return progress;
		}
		const followUpFields = form.fields.filter((f) => f._sectionId === fieldForAnswer._sectionId);
		const progressPerFollowUpAnswer = progressPerAnswer / (sortedAnswers.length * followUpFields.length + 1);
		progress +=
			(Math.max(
				sortedAnswers.findIndex(({ id }) => id === answer.followUpFrom?.answer?.id),
				0
			) *
				followUpFields.length +
				Math.max(
					followUpFields.findIndex((f) => f._id === answer.fieldId),
					0
				) +
				1) *
			progressPerFollowUpAnswer;
	}
	return progress;
};

export const getFormFieldAnswerNumbering = (form: IForm, answer: IFormFieldAnswer): string | undefined => {
	let index = 0;
	let value: string | undefined;
	const field = form.fields.find(({ _id }) => _id === answer.fieldId);
	const section = form.sections.find(({ _id }) => _id === field?._sectionId);
	if (!field || !section) {
		return;
	}
	form.sections.some((s) => {
		if (s.isFollowUp) {
			if (s._id === section._id) {
				index = 0;
			} else {
				return;
			}
		}
		const fields = form.fields.filter(({ _sectionId }) => _sectionId === s._id);
		return fields.some(({ _id }) => {
			index++;
			if (_id === answer.fieldId) {
				if (s.isFollowUp && answer.followUpFrom?.fieldId) {
					const fieldPointingToFollowUp = form.fields.find((f) => f._id === answer.followUpFrom?.fieldId);
					if (fieldPointingToFollowUp) {
						if (fieldPointingToFollowUp.type === FormFieldTypeEnum.CHECKBOX) {
							const followUpIndex = fieldPointingToFollowUp.options.findIndex((o) => o.id === answer.followUpFrom?.answer?.id);
							if (followUpIndex !== -1) {
								value = `${getFormFieldNumbering(form, fieldPointingToFollowUp._id)}.${index + followUpIndex * fields.length}`;
							}
						} else {
							value = `${getFormFieldNumbering(form, fieldPointingToFollowUp._id)}.${index}`;
						}
					}
				} else {
					value = `${index}`;
				}
				return true;
			}
		});
	});
	return value;
};

export const getAnswerLabel = (form: IForm, answer: IFormFieldAnswer): string | undefined => {
	const field = form.fields.find((f) => f._id === answer.fieldId);
	return (
		field &&
		field.label
			// If sentence starts with {{answer}}, capitalize the answer's first letter
			.replace(/^{{answer}}/, () => [answer.followUpFrom?.answer?.label ?? ''].map((str) => str[0].toUpperCase() + str.slice(1))[0])
			// If {{answer}} is found somewhere later in the sentence, de-capitalize the answer's first
			// letter (unless the whole answer is in upper case e.g. abbreviations).
			.replace(
				/{{answer}}/g,
				() => [answer.followUpFrom?.answer?.label ?? ''].map((str) => (str === str.toUpperCase() ? str : str[0].toLowerCase() + str.slice(1)))[0]
			)
	);
};

export const createFormSection = (): IFormSection => ({
	_id: uuid(),
	title: '',
});

export const updateFormSection = (form: IForm, section: IFormSection): IForm => {
	const index = form.sections.findIndex(({ _id }) => _id === section._id);
	if (index === -1) {
		return form;
	}
	const sections = [...form.sections];
	sections[index] = section;
	if (index === 0) {
		form.title = section.title;
	}
	return { ...form, sections };
};

export const getFormSectionNumbering = (form: IForm, sectionId: string): string | undefined => {
	let index = 0;
	const currentSection = form.sections.find(({ _id }) => _id === sectionId);
	if (!currentSection || currentSection.isFollowUp) {
		return;
	}
	form.sections.some((section) => {
		if (!section.isFollowUp) {
			index++;
		}
		return section._id === sectionId;
	});
	return `${index}`;
};

export const createFormField = <T extends FormFieldTypeEnum>(fieldType: T, translate: (key: string, variables?: object) => string): IFormField<T> => {
	const field: IFormFieldGeneric = {
		_id: uuid(),
		_sectionId: uuid(),
		type: fieldType,
		label: '',
		mandatory: false,
	};

	switch (field.type) {
		case FormFieldTypeEnum.TEXT:
		case FormFieldTypeEnum.TEXTAREA:
			const textField: IFormField<FormFieldTypeEnum.TEXT | FormFieldTypeEnum.TEXTAREA> = {
				...field,
				type: field.type,
				valueType: 'string',
			};
			return textField as IFormField<T>;
		case FormFieldTypeEnum.ATTACHMENT:
			const attachmentField: IFormField<FormFieldTypeEnum.ATTACHMENT> = {
				...field,
				type: field.type,
				valueType: 'attachment',
			};
			return attachmentField as IFormField<T>;
		case FormFieldTypeEnum.CHECKBOX:
		case FormFieldTypeEnum.RADIOBOX:
			const optionsField: IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX> = {
				...field,
				type: field.type,
				options: [createFormFieldOption(), createFormFieldOption()],
				valueType: 'array',
			};

			return optionsField as IFormField<T>;
		case FormFieldTypeEnum.SCALE:
			const scaleField: IFormField<FormFieldTypeEnum.SCALE> = {
				...field,
				type: field.type,
				min: 1,
				max: 5,
				minLabel: translate('LOW'),
				maxLabel: translate('HIGH'),
			};
			return scaleField as IFormField<T>;
		case FormFieldTypeEnum.INTRODUCTION:
			const introductionField: IFormField<FormFieldTypeEnum.INTRODUCTION> = {
				...field,
				type: field.type,
				description: '',
			};
			return introductionField as IFormField<T>;
		case FormFieldTypeEnum.RATING:
			const ratingField: IFormField<FormFieldTypeEnum.RATING> = {
				...field,
				type: field.type,
				ratingType: RatingTypeEnum.STARS,
				ratingAmount: 5,
			};
			return ratingField as IFormField<T>;
		case FormFieldTypeEnum.NUMBER:
			const numberField: IFormField<FormFieldTypeEnum.NUMBER> = {
				...field,
				type: field.type,
				validationType: ValidationTypeEnum.WHOLE_NUMBER,
			};
			return numberField as IFormField<T>;
		default:
			return field as IFormField<T>;
	}
};

export const insertFormField = (form: IForm, field: IFormField, sectionId?: string): IForm => {
	const sections = [...form.sections];
	const fields = [...form.fields];
	if (!sectionId) {
		const section = createFormSection();
		sectionId = section._id;
		sections.push(section);
	}
	field._sectionId = sectionId;
	fields.push(field);
	form = { ...form, fields, sections };

	return sortForm(form);
};

export const updateFormField = (form: IForm, field: IFormField): IForm => {
	const index = form.fields.findIndex(({ _id }) => _id === field._id);
	if (index === -1) {
		return form;
	}

	form = syncFollowUp(form, form.fields[index], field);

	const fields = [...form.fields];
	fields[index] = field;
	form = { ...form, fields };

	return sortForm(form);
};

export const removeFormField = (form: IForm, field: IFormField): IForm => {
	const section = field && form.sections.find(({ _id }) => _id === field._sectionId);
	if (!section) {
		return form;
	}

	form = syncFollowUp(form, field, undefined);

	const fields = form.fields.filter(({ _id }) => _id !== field._id);
	const sections = fields.find(({ _sectionId }) => _sectionId === field._sectionId)
		? form.sections
		: form.sections.filter(({ _id }) => _id !== section._id);
	form = { ...form, fields, sections };

	// Cleanup section references
	form.sections.forEach(({ _id }) => cleanupSectionReference(form, _id));

	// Cleanup field references
	form.fields.forEach(({ _id }) => cleanupFormFieldReferences(form, _id));

	return sortForm(form);
};

export const changeFormFieldType = (
	form: IForm,
	fieldId: string,
	type: FormFieldTypeEnum,
	translate: (key: string, variables?: object) => string
): IForm => {
	const field = form.fields.find(({ _id }) => _id === fieldId);
	if (!field) {
		return form;
	}

	const newField = createFormField(type, translate);
	newField._id = field._id;
	newField._sectionId = field._sectionId;
	newField.label = field.label;
	newField.mandatory = type === FormFieldTypeEnum.INTRODUCTION ? false : form.type === FormTypeEnum.POLL ? true : field.mandatory;
	return updateFormField(form, newField);
};

export const getFormFieldPointToFollowUpSection = (form: IForm, sectionId: string): IFormField =>
	form.fields.find((f) => f.type === FormFieldTypeEnum.CHECKBOX && f.options.find((o) => o.followUpTo?.sectionId === sectionId))!;

export const getFormFieldNumbering = (form: IForm, fieldId: string): string | undefined => {
	let index = 0;
	let value: string | undefined;
	const field = form.fields.find(({ _id }) => _id === fieldId);
	const section = form.sections.find(({ _id }) => _id === field?._sectionId);
	if (!field || !section) {
		return;
	}
	form.sections.some((s) => {
		if (s.isFollowUp) {
			if (s._id === section._id) {
				index = 0;
			} else {
				return;
			}
		}
		return form.fields
			.filter(({ _sectionId }) => _sectionId === s._id)
			.some(({ _id }) => {
				index++;
				if (_id === fieldId) {
					if (s.isFollowUp) {
						const fieldPointingToFollowUp = getFormFieldPointToFollowUpSection(form, section._id);
						if (fieldPointingToFollowUp) {
							value = `${getFormFieldNumbering(form, fieldPointingToFollowUp._id)}.${index}`;
						}
					} else {
						value = `${index}`;
					}
					return true;
				}
			});
	});
	return value;
};

export const createFormFieldOption = (): IFormFieldOption => ({
	id: uuid(),
	label: '',
});

export const insertFormFieldOption = (
	field: IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX>,
	option: IFormFieldOption
): IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX> => {
	const options = [...field.options];
	const categories = getFormFieldCategories(field);
	if (field.type === FormFieldTypeEnum.CHECKBOX && options.every(({ followUpTo }) => followUpTo?.sectionId === options[0]?.followUpTo?.sectionId)) {
		// Add follow up to from other fields if all of them have the same follow up to
		option.followUpTo = options[0]?.followUpTo;
	}
	if (option.category && !categories.find(({ id }) => id === option.category?.id)) {
		// If category does not exist yet, add it
		categories.push(option.category);
	}
	options.push(option);
	return {
		...field,
		options: categories.length
			? categories.map((category) => options.filter((f) => category.id === f.category?.id)).flat()
			: [...field.options, option],
	};
};

export const updateFormFieldOption = (
	field: IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX | FormFieldTypeEnum.RATING | FormFieldTypeEnum.SCALE>,
	option: IFormFieldOption
): IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX | FormFieldTypeEnum.RATING | FormFieldTypeEnum.SCALE> => {
	field.options = field.options ?? [];
	const index = field.options.findIndex((o) => o.id === option.id);
	if (index === -1) {
		return field;
	}
	const options = [...field.options];
	options[index] = option;
	return { ...field, options };
};

export const removeFormFieldOption = (
	field: IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX>,
	option: IFormFieldOption
): IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX> => {
	const options = field.options.filter((o) => o.id !== option.id);
	// Make sure that there are always 2 options, can't have a multiple choice/response without it!
	if (options.length === 1) {
		options.push({ ...createFormFieldOption(), category: options[0].category });
	}
	switch (field.type) {
		case FormFieldTypeEnum.CHECKBOX:
			const maxOptionCount = (field.maxOptionCount ?? -1) > options.length ? options.length : field.maxOptionCount;
			return { ...field, options, maxOptionCount };
		case FormFieldTypeEnum.RADIOBOX:
			return { ...field, options };
	}
};

export const createFormFieldOptionCategory = (): IFormFieldOptionCategory => ({
	id: uuid(),
	label: '',
});

export const getFormFieldCategories = (field: IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX>): IFormFieldOptionCategory[] =>
	uniqBy(
		field.options.map(({ category }) => category).filter((category): category is IFormFieldOptionCategory => !!category),
		(o) => o.id
	);

export const insertFormFieldOptionCategory = (
	field: IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX>,
	category: IFormFieldOptionCategory
): IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX> => {
	// If there are options but no categories, assign all existing options to category
	if (field.options.length && !getFormFieldCategories(field).length) {
		return {
			...field,
			options: field.options.map((option) => ({ ...option, category })),
		};
	}
	// Else add option with new category
	return insertFormFieldOption(field, {
		...createFormFieldOption(),
		category,
	});
};

export const updateFormFieldOptionCategory = (
	field: IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX>,
	category: IFormFieldOptionCategory
): IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX> => ({
	...field,
	options: field.options.map((option) => ({
		...option,
		category: option.category?.id === category.id ? category : option.category,
	})),
});

export const removeFormFieldOptionCategory = (
	field: IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX>,
	category: IFormFieldOptionCategory
): IFormField<FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX> => {
	// If there are no more categories, remove categories from all remaining options
	if (!getFormFieldCategories(field).filter(({ id }) => id !== category.id).length) {
		return {
			...field,
			options: field.options.map(({ category: c, ...option }) => option),
		};
	}
	// Else remove category and all options assigned to it
	return field.options.filter((option) => option.category?.id === category.id).reduce(removeFormFieldOption, field);
};

export const getFollowUpFromField = (field: IFormField<FormFieldTypeEnum.CHECKBOX>): IFollowUpTo | undefined =>
	field.options[0]?.followUpTo && field.options.every((option) => option.followUpTo?.sectionId === field.options[0]?.followUpTo?.sectionId)
		? field.options[0]?.followUpTo
		: undefined;

export const addFollowUpToField = (
	field: IFormField<FormFieldTypeEnum.CHECKBOX>,
	followUpTo: IFollowUpTo
): IFormField<FormFieldTypeEnum.CHECKBOX> => ({
	...field,
	options: field.options.map((option) => ({ ...option, followUpTo })),
});

export const removeFollowUpFromField = (field: IFormField<FormFieldTypeEnum.CHECKBOX>): IFormField<FormFieldTypeEnum.CHECKBOX> => ({
	...field,
	options: field.options.map(({ followUpTo, ...option }) => option),
});

/**
 * Sync is follow up in sections with follow to in options
 */
export const syncFollowUp = (form: IForm, previousField: IFormField, field?: IFormField): IForm => {
	const previousFollowUpToSectionIds =
		previousField.type === FormFieldTypeEnum.CHECKBOX
			? uniq(previousField.options.map(({ followUpTo }) => followUpTo?.sectionId).filter((sectionId): sectionId is string => !!sectionId))
			: [];
	const followUpToSectionIds =
		field && field.type === FormFieldTypeEnum.CHECKBOX
			? uniq(field.options.map(({ followUpTo }) => followUpTo?.sectionId).filter((sectionId): sectionId is string => !!sectionId))
			: [];
	const difference = xor(previousFollowUpToSectionIds, followUpToSectionIds);
	if (difference.length) {
		// Keep isFollowUp in sync
		form = {
			...form,
			sections: form.sections.map((section) =>
				difference.includes(section._id)
					? {
							...section,
							isFollowUp: followUpToSectionIds.includes(section._id),
						}
					: section
			),
		};
	}

	// Keep followUpTo in sync
	if (!field) {
		const section = form.sections.find(({ _id }) => _id === previousField._sectionId);
		const sectionWillBeRemoved =
			section && form.fields.find(({ _id, _sectionId }) => _id !== previousField._id && _sectionId === previousField._sectionId);

		// We need to clear all followUpTo pointing to the section that will be removed
		if (!sectionWillBeRemoved) {
			form = {
				...form,
				fields: form.fields.map((f) =>
					f.type === FormFieldTypeEnum.CHECKBOX
						? {
								...f,
								options: f.options.map((option) =>
									option.followUpTo?.sectionId === section?._id
										? {
												...option,
												followUpTo: { sectionId: '' },
											}
										: option
								),
							}
						: f
				),
			};
		}
	}

	return form;
};

export const getSkipToOptions = (field: IFormField<FormFieldTypeEnum.SCALE | FormFieldTypeEnum.RATING>) => {
	// generate a list of options based on SCALE & RATING field properties
	const { min = 1, max = (field as IFormField<FormFieldTypeEnum.RATING>).ratingAmount } = field as IFormField<FormFieldTypeEnum.SCALE>;
	return Array.from(Array(max + 1 - min).keys()).map((o, i) => ({
		id: `${min + i}`,
		label: `${min + i}`,
	}));
};

export const sortForm = (form: IForm): IForm => {
	// Sort sections based on follow up order
	const sections = form.sections
		.filter(({ isFollowUp }) => !isFollowUp)
		.map((section) => [
			section,
			...form.fields
				.filter(({ _sectionId }) => _sectionId === section._id)
				.map((field) =>
					field.type === FormFieldTypeEnum.CHECKBOX
						? uniq(field.options.map(({ followUpTo }) => followUpTo?.sectionId).filter((sectionId): sectionId is string => !!sectionId))
						: []
				)
				.flat()
				.map((sectionId) => form.sections.find(({ _id }) => _id === sectionId))
				.filter((section): section is IFormSection => !!section),
		])
		.flat();

	// Sort fields based on sections order
	const fields = sections.map((section) => form.fields.filter((field) => field._sectionId === section._id)).flat();

	return { ...form, title: sections[0]?.title ?? '', sections, fields };
};

/**
 * Remove skip to and follow up that are no longer valid
 */
export const cleanupFormFieldReferences = (form: IForm, fieldId: string): void => {
	const field = form.fields.find(({ _id }) => _id === fieldId);
	const section = form.sections.find(({ _id }) => _id === field?._sectionId);
	if (!field || !section) {
		return;
	}

	if (field.type === FormFieldTypeEnum.CHECKBOX) {
		field.options.forEach((o) => {
			const followUpSection = form.sections.find((s2) => s2._id === o.followUpTo?.sectionId);
			if (!followUpSection) {
				return;
			}
			const followUpIsPointingToItself = followUpSection._id === section._id;
			if (section.isFollowUp || followUpIsPointingToItself) {
				followUpSection.isFollowUp = false;
				delete o.followUpTo;
			}
		});
	}

	if (field.type === FormFieldTypeEnum.RADIOBOX || field.type === FormFieldTypeEnum.SCALE || field.type === FormFieldTypeEnum.RATING) {
		const sectionIndex = form.sections.findIndex(({ _id }) => _id === section._id);
		const availableSectionIds = form.sections
			.slice(sectionIndex + 2)
			.filter(({ isFollowUp }) => !isFollowUp)
			.map((s2) => s2._id);

		field.options?.forEach((o) => {
			const skipToIsPointingToUnavailableSection = o.skipTo?._id && !availableSectionIds.includes(o.skipTo._id);
			if (section.isFollowUp || skipToIsPointingToUnavailableSection) {
				delete o.skipTo;
			}
		});
	}
};

// Remove skip to that is no longer valid
export const cleanupSectionReference = (form: IForm, sectionId: string) => {
	const section = form.sections.find(({ _id }) => _id === sectionId);
	if (!section) {
		return;
	}

	if (section.skipTo) {
		if (section.isFollowUp) {
			section.skipTo = undefined;
			return;
		}
		const sectionIndex = form.sections.findIndex(({ _id }) => _id === section._id);
		const availableSectionIds = form.sections
			.slice(sectionIndex + 2)
			.filter(({ isFollowUp }) => !isFollowUp)
			.map(({ _id }) => _id);
		if (Array.isArray(section.skipTo)) {
			section.skipTo = section.skipTo.filter((skipTo) => availableSectionIds.includes(skipTo._id));
		}
		if (Array.isArray(section.skipTo) ? !section.skipTo.length : !availableSectionIds.includes(section.skipTo._id)) {
			section.skipTo = undefined;
		}
	}
};

export const moveFormField = (form: IForm, fieldId: string, destination: { sectionId: string; index: number }): IForm => {
	const field = form.fields.find(({ _id }) => _id === fieldId);
	if (!field) {
		return form;
	}

	// Remove field
	let fields = form.fields.filter((f) => f._id !== field._id);

	// Insert field in destination section at destination index
	fields = form.sections
		.map(({ _id }) => {
			const sectionFields = fields.filter(({ _sectionId }) => _sectionId === _id);
			if (_id === destination.sectionId) {
				sectionFields.splice(destination.index, 0, {
					...field,
					_sectionId: destination.sectionId,
				});
			}
			return sectionFields;
		})
		.flat();

	// Remove empty sections
	const sections = form.sections.filter((s) => fields.find((f) => f._sectionId === s._id));
	form = { ...form, fields, sections };

	// Cleanup section references
	form.sections.forEach(({ _id }) => cleanupSectionReference(form, _id));

	// Cleanup field references
	form.fields.forEach(({ _id }) => cleanupFormFieldReferences(form, _id));

	return sortForm(form);
};

export const moveFormSection = (form: IForm, sectionId: string, destination: number): IForm => {
	const section = form.sections.find(({ _id }) => _id === sectionId);
	if (!section) {
		return form;
	}

	// Move section to new destination
	const sections = form.sections.filter(({ _id }) => _id !== sectionId);
	sections.splice(destination, 0, section);
	form = { ...form, sections, title: sections[0]?.title };

	// Cleanup section references
	form.sections.forEach(({ _id }) => cleanupSectionReference(form, _id));

	// Cleanup field references
	form.fields.forEach(({ _id }) => cleanupFormFieldReferences(form, _id));

	return sortForm(form);
};

export const validateForm = (form: IForm): IFormError[] => {
	const errors: IFormError[] = [];
	errors.push(...form.sections.map((section) => validateFormSection(form, section)).flat());
	return errors;
};

export const validateFormSection = (form: IForm, section: IFormSection): IFormError[] => {
	const errors: IFormError[] = [];
	if (!section.title.trim() && section !== form.sections[0]) {
		errors.push({
			type: FormErrorTypeEnum.SECTION_TITLE_REQUIRED,
			sectionId: section._id,
		});
	}
	errors.push(...form.fields.map((field) => validateFormField(form, field)).flat());
	return errors;
};

export const validateFormField = (form: IForm, field: IFormField): IFormError[] => {
	const errors: IFormError[] = [];
	if (!field.label.trim()) {
		errors.push({
			type: FormErrorTypeEnum.FIELD_LABEL_REQUIRED,
			fieldId: field._id,
			sectionId: field._sectionId,
		});
	}
	switch (field.type) {
		case FormFieldTypeEnum.CHECKBOX:
		case FormFieldTypeEnum.RADIOBOX:
			const hasMedia = field.options.filter((o) => !!o.attachments?.length).length > 0;
			getFormFieldCategories(field).forEach((category) => {
				if (!category.label.trim()) {
					errors.push({
						type: FormErrorTypeEnum.OPTION_CATEGORY_TITLE_REQUIRED,
						fieldId: field._id,
						sectionId: field._sectionId,
						categoryId: category.id,
					});
				}
			});
			field.options.map((option) => {
				if (hasMedia && (!option.attachments || !option.attachments[0])) {
					errors.push({
						type: FormErrorTypeEnum.OPTION_IMAGE_REQUIRED,
						fieldId: field._id,
						sectionId: field._sectionId,
						optionId: option.id,
					});
				} else if (!option.label.trim()) {
					errors.push({
						type: FormErrorTypeEnum.OPTION_TITLE_REQUIRED,
						fieldId: field._id,
						sectionId: field._sectionId,
						optionId: option.id,
					});
				}
			});
			if (field.type === FormFieldTypeEnum.CHECKBOX && field.maxOptionCount !== undefined && field.maxOptionCount < 2) {
				errors.push({
					type: FormErrorTypeEnum.MAX_OPTION_COUNT_MUST_BE_LARGER_THAN_2,
					fieldId: field._id,
					sectionId: field._sectionId,
				});
			}
			if (field.correctOptions && field.correctOptions.length < 1) {
				errors.push({
					type: FormErrorTypeEnum.CORRECT_OPTIONS_REQUIRED,
					fieldId: field._id,
					sectionId: field._sectionId,
				});
			}
			break;
		case FormFieldTypeEnum.NUMBER:
			if (field.validationType === ValidationTypeEnum.WHOLE_NUMBER_BETWEEN) {
				const min = parseInt(field.min, 10);
				const max = parseInt(field.max, 10);
				if (!field.min || isNaN(min)) {
					errors.push({
						type: FormErrorTypeEnum.NUMBER_MIN_REQUIRED,
						fieldId: field._id,
						sectionId: field._sectionId,
					});
				}
				if (!field.max || isNaN(max)) {
					errors.push({
						type: FormErrorTypeEnum.NUMBER_MAX_REQUIRED,
						fieldId: field._id,
						sectionId: field._sectionId,
					});
				}
			}
			break;
		case FormFieldTypeEnum.INTRODUCTION:
			if (!field.description.trim()) {
				errors.push({
					type: FormErrorTypeEnum.FIELD_DESCRIPTION_REQUIRED,
					fieldId: field._id,
					sectionId: field._sectionId,
				});
			}
	}
	return errors;
};

export const isSameFormError = (a: IFormError, b: IFormError) =>
	a.type === b.type &&
	a.fieldId === b.fieldId &&
	a.sectionId === b.sectionId &&
	(a.type === FormErrorTypeEnum.OPTION_CATEGORY_TITLE_REQUIRED && b.type === FormErrorTypeEnum.OPTION_CATEGORY_TITLE_REQUIRED
		? a.categoryId === b.categoryId
		: a.type === FormErrorTypeEnum.OPTION_TITLE_REQUIRED && b.type === FormErrorTypeEnum.OPTION_TITLE_REQUIRED
			? a.optionId === b.optionId
			: true);

export const parseFormReference = (reference: any, form: any): IFormReference | undefined => {
	if (typeof reference !== 'object' || !reference?._id) {
		return;
	}

	const { _id, type = FormReferenceTypeEnum.SECTION, label } = reference;

	const { fields = [], sections = [] } = form;
	if ((type === FormReferenceTypeEnum.SECTION ? sections : fields).filter(({ _id: id }: { _id: any }) => id === _id).length === 0) {
		return;
	}

	return {
		_id,
		type,
		label: `${label ?? ``}`,
	};
};

const parseFormFieldOptions = (
	options: any = [],
	form: any
): IFormField<FormFieldTypeEnum.RADIOBOX | FormFieldTypeEnum.CHECKBOX>['options'] | undefined => {
	if (!Array.isArray(options) || options.length === 0) {
		return;
	}

	const { sections = [] } = form;

	options = options
		.map((o) => {
			const skipTo = parseFormReference(o.skipTo, form);
			const followUpTo =
				o.followUpTo?.sectionId && sections.filter(({ _id }: any) => _id === o.followUpTo?.sectionId).length !== 0
					? {
							sectionId: o.followUpTo.sectionId,
						}
					: undefined;

			return {
				id: o.id,
				attachments: o.attachments,
				label: `${o.label ?? ``}`,
				...(skipTo ? { skipTo } : {}),
				...(followUpTo ? { followUpTo } : {}),
				...(o.category
					? {
							category: {
								id: o.category.label ?? uuid(),
								label: `${o.category.label ?? ``}`,
							},
						}
					: {}),
			};
		})
		.filter(<T>(value: T): value is Exclude<T, undefined> => !!value);

	if (options.length === 0) {
		return;
	}

	return options;
};

export const parseFormField = (field: any, form: any, translate: (key: string, variables?: object) => string): IFormField | undefined => {
	if (typeof field !== 'object') {
		return;
	}

	if (!field._sectionId || !field._id || !field.type || (field.label ?? '').length === 0) {
		return;
	}

	const { _id, _sectionId, type, label, mandatory = false, helptext, bubbles } = field ?? {};
	const shared: Pick<IFormField, '_id' | '_sectionId' | 'type' | 'label' | 'mandatory' | 'helptext' | 'bubbles'> = {
		_id,
		_sectionId,
		type,
		label,
		mandatory,
		helptext: helptext ? `${helptext}` : undefined,
		bubbles: Array.isArray(bubbles) ? bubbles.filter((bubble) => typeof bubble === 'string') : undefined,
	};

	switch (field.type) {
		case FormFieldTypeEnum.ATTACHMENT: {
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.ATTACHMENT> = {
				...shared,
				type: field.type,
				valueType: 'attachment',
			};
			return value;
		}
		case FormFieldTypeEnum.CHECKBOX: {
			if (!Array.isArray(field.options)) {
				return;
			}
			const fieldOptions = parseFormFieldOptions(field.options, form);
			if (!fieldOptions) {
				return;
			}
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.CHECKBOX> = {
				...shared,
				type: field.type,
				valueType: 'array',
				options: fieldOptions,
				isOtherAnswerAllowed: !!field.isOtherAnswerAllowed,
				maxOptionCount:
					!field.maxOptionCount || isNaN(field.maxOptionCount)
						? undefined
						: field.maxOptionCount > fieldOptions.length
							? fieldOptions.length
							: field.maxOptionCount,
				correctOptions: Array.isArray(field.correctOptions)
					? field.correctOptions.filter((option: any) => fieldOptions.find((fieldOption) => fieldOption.id === option))
					: undefined,
				correctMessage: typeof field.correctMessage === 'string' && field.correctMessage.trim() ? field.correctMessage : undefined,
				incorrectMessage: typeof field.incorrectMessage === 'string' && field.incorrectMessage.trim() ? field.incorrectMessage : undefined,
			};
			return value;
		}
		case FormFieldTypeEnum.RADIOBOX: {
			if (!Array.isArray(field.options)) {
				return;
			}
			const fieldOptions = parseFormFieldOptions(field.options, form);
			if (!fieldOptions) {
				return;
			}
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.RADIOBOX> = {
				...shared,
				type: field.type,
				valueType: 'array',
				options: fieldOptions,
				isOtherAnswerAllowed: !!field.isOtherAnswerAllowed,
				maxItem: isNaN(field.maxItem) ? undefined : field.maxItem,
				correctOptions: Array.isArray(field.correctOptions)
					? field.correctOptions.filter((option: any) => fieldOptions.find((fieldOption) => fieldOption.id === option))
					: undefined,
				correctMessage: typeof field.correctMessage === 'string' && field.correctMessage.trim() ? field.correctMessage : undefined,
				incorrectMessage: typeof field.incorrectMessage === 'string' && field.incorrectMessage.trim() ? field.incorrectMessage : undefined,
			};
			return value;
		}
		case FormFieldTypeEnum.DATE: {
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.DATE> = {
				...shared,
				type: field.type,
			};
			return value;
		}
		case FormFieldTypeEnum.INTRODUCTION: {
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.INTRODUCTION> = {
				...shared,
				type: field.type,
				description: `${field.description ?? ''}`,
			};
			return value;
		}
		case FormFieldTypeEnum.NUMBER: {
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.NUMBER> = {
				...shared,
				type: field.type,
				validationType: Object.values(ValidationTypeEnum).includes(field.validationType) ? field.validationType : ValidationTypeEnum.WHOLE_NUMBER,
				...(field.validationType === ValidationTypeEnum.WHOLE_NUMBER_BETWEEN
					? {
							min: isNaN(field.min) ? 0 : parseInt(field.min, 10),
							max: isNaN(field.max) ? 999 : parseInt(field.max, 10),
						}
					: {}),
			};
			return value;
		}
		case FormFieldTypeEnum.RATING: {
			const fieldOptions = parseFormFieldOptions(field.options, form);
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.RATING> = {
				...shared,
				type: field.type,
				ratingType: field.ratingType ?? RatingTypeEnum.STARS,
				ratingAmount: isNaN(field.ratingAmount) ? 5 : parseInt(field.ratingAmount, 10),
				options: fieldOptions,
			};
			return value;
		}
		case FormFieldTypeEnum.SCALE: {
			const fieldOptions = parseFormFieldOptions(field.options, form);
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.SCALE> = {
				...shared,
				type: field.type,
				minLabel: `${field.minLabel ?? translate('LOW')}`,
				maxLabel: `${field.maxLabel ?? translate('HIGH')}`,
				min: isNaN(field.min) ? 1 : parseInt(field.min, 10),
				max: isNaN(field.max) ? 5 : parseInt(field.max, 10),
				options: fieldOptions,
			};
			return value;
		}
		case FormFieldTypeEnum.TEXT: {
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.TEXT> = {
				...shared,
				type: field.type,
				valueType: 'string',
				maxLength: !field.maxLength || isNaN(field.maxLength) ? 140 : parseInt(field.maxLength, 10),
				shareToGroup: field.shareToGroup?.isShareable
					? {
							isShareable: true,
						}
					: undefined,
			};
			return value;
		}
		case FormFieldTypeEnum.TEXTAREA: {
			// noinspection UnnecessaryLocalVariableJS
			const value: IFormField<FormFieldTypeEnum.TEXT> = {
				...shared,
				type: field.type,
				valueType: 'string',
				shareToGroup: field.shareToGroup?.isShareable
					? {
							isShareable: true,
						}
					: undefined,
			};
			return value;
		}
	}

	return field;
};

export const parseFormSection = (section: any = {}, form: any): IFormSection | undefined => {
	if (typeof section !== 'object' || !section._id) {
		return;
	}

	const { fields = [] } = form;
	if (fields.filter(({ _sectionId }: any) => _sectionId === section._id).length === 0) {
		return;
	}

	// Backend sometimes returns null instead of undefined...
	const skipTo = section.skipTo === null ? undefined : parseFormReference(section.skipTo, form);

	return {
		title: `${section.title ?? ''}`,
		_id: section._id,
		isFollowUp: !!section.isFollowUp,
		...(skipTo || skipTo === null ? { skipTo } : {}),
	};
};

/**
 * Parsing form and all of it's properties mainly for preventing errors when duplicating an older form, where data can not be trusted.
 */
export const parseForm = (form: any = {}, translate: (key: string, variables?: object) => string): IForm | undefined => {
	if (typeof form !== 'object') {
		return;
	}

	if (!Array.isArray(form.fields) || !Array.isArray(form.sections)) {
		return;
	}

	const sections: IFormSection[] = form.sections
		.map((section: any) => parseFormSection(section, form))
		.filter((section: any): section is IFormSection => !!section);
	const fields: IFormField[] = form.fields
		.map((field: any) => parseFormField(field, form, translate))
		.filter((field: any): field is IFormField => !!field);

	const shareToGroup =
		form.shareToGroup?.groupIds?.length > 0
			? {
					groupIds: form.shareToGroup.groupIds,
				}
			: undefined;

	const usersToNotify = (Array.isArray(form.usersToNotify) ? form.usersToNotify : [form.usersToNotify]).filter((o: any) => o && o.id && o.fullname);

	return {
		id: form.id,
		type: Object.values(FormTypeEnum).includes(form.type) ? (form.type as FormTypeEnum) : FormTypeEnum.ADVANCED,
		fields,
		sections,
		title: form.title ?? fields[0].label,
		shuffleOptions: !!form.shuffleOptions,
		isAnonymous: !!form.isAnonymous,
		submitAction: Object.values(SubmitActionEnum).includes(form.submitAction)
			? (form.submitAction as SubmitActionEnum)
			: SubmitActionEnum.SHOW_THANK_YOU_MESSAGE,
		multipleEntriesAllowed: !!form.multipleEntriesAllowed,
		isInsertingSubmittedAnswers: !!form.multipleEntriesAllowed && !!form.isInsertingSubmittedAnswers,
		confirmationMessage: `${form.confirmationMessage ?? ''}`,
		status: Object.values(FormStatusEnum).includes(form.status) ? (form.status as FormStatusEnum) : FormStatusEnum.DRAFT,
		canEdit: !!form.canEdit,
		...(shareToGroup ? { shareToGroup } : {}),
		...(usersToNotify ? { usersToNotify } : {}),
		sharedTo: `${form.sharedTo ?? 'message'}`,
		user: form.user,
		copyFromFormId: form.copyFromFormId,
		theme: Object.values(FormThemeEnum).includes(form.theme) ? form.theme : undefined,
		source: form.source,
		vhost: form.vhost,
		createdAt: form.createdAt,
		isArchived: form.isArchived,
		isDeleted: form.isDeleted,
		showCorrectAnswers: Object.values(ShowCorrectAnswersEnum).includes(form.showCorrectAnswers) ? form.showCorrectAnswers : undefined,
	};
};

export const getFormSourceURL = (source?: IFormSource) => {
	if (!source) return;

	switch (source.type) {
		case SourceTypeEnum.AGENDA_EVENT:
			return `/agendaItem/${source.agendaId}`;
		case SourceTypeEnum.AGENDA_MESSAGE:
		case SourceTypeEnum.CHANNEL_MESSAGE:
		case SourceTypeEnum.GROUP_MESSAGE:
			return `/message/${source.messageId}`;
		case SourceTypeEnum.INFO_PAGE:
			return `/page/${source.contentPageId}`;
		case SourceTypeEnum.ONBOARDING:
			return `/programmanager/${source.onBoardingId}`;
	}
};

export const getFormResultsURL = (formId: string, source?: IFormSource) => {
	switch (source?.type) {
		case SourceTypeEnum.ONBOARDING:
			return `/program/results?program=${source.onBoardingId}&stepId=${source.stepId}`;
		default:
			return `/forms/${formId}/results`;
	}
};

export const formFieldAnswerValueToString = (field: IFormField, answer: FormFieldAnswerValueTypeMap[FormFieldTypeEnum]): string => {
	switch (field.type) {
		case FormFieldTypeEnum.TEXT:
			return (answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.TEXT]) ?? '';
		case FormFieldTypeEnum.TEXTAREA:
			return (answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.TEXTAREA]) ?? '';
		case FormFieldTypeEnum.CHECKBOX:
		case FormFieldTypeEnum.RADIOBOX: {
			const optionsAnswer = (answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.CHECKBOX | FormFieldTypeEnum.RADIOBOX]) ?? '';
			if (!optionsAnswer) {
				return '';
			}
			return optionsAnswer.map((option) => option.label).join(', ');
		}
		case FormFieldTypeEnum.SCALE:
			return (answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.SCALE]) ?? '';
		case FormFieldTypeEnum.DATE:
			const dateAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.DATE];
			const date = new Date(dateAnswer!);
			return isValid(date) ? format(date, 'dd-MM-yyyy') : '';
		case FormFieldTypeEnum.RATING:
			const ratingAnswer = answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.RATING];
			switch (field.ratingType) {
				case RatingTypeEnum.SMILEYS:
					switch (ratingAnswer) {
						case 1:
							return '😢';
						case 2:
							return '🙁';
						case 3:
							return '😐';
						case 4:
							return '🙂';
						case 5:
							return '😀';
					}
					break;
				case RatingTypeEnum.STARS:
					return `${ratingAnswer}`;
			}
			break;
		case FormFieldTypeEnum.NUMBER:
			return (answer as unknown as FormFieldAnswerValueTypeMap[FormFieldTypeEnum.NUMBER]) ?? '';
	}
	return '';
};
