import { FileTypeEnum } from '../interfaces/media/FileTypeEnum';
import { IMedia } from '../interfaces/media/IMedia';

export const isMedia = (value: any): value is IMedia => {
	const isGenericMedia =
		Object.values(FileTypeEnum).indexOf(value.fileType) !== -1 &&
		typeof value.filelink === 'string' &&
		typeof value.filename === 'string' &&
		typeof value.id === 'string';

	if (value.fileType !== FileTypeEnum.image || value.imagemeta === undefined) {
		return isGenericMedia;
	}

	return (
		isGenericMedia &&
		(!value.imagemeta ||
			(typeof value.imagemeta?.width === 'number' &&
				typeof value.imagemeta?.height === 'number' &&
				['landscape', 'portrait'].includes(value.imagemeta?.orientation)))
	);
};
